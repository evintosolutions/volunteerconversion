------------------------------------------------------------
--HEARING VOLUNTEER INPUT
--Dependencies: 
---------------------------------------------------------------------------------

 
insert into [dbo].HearingVolunteerInput
        ( HearingID ,
          Excused ,
          ReportRequired ,
          RecommendsAccepted ,
          RecommendsRejected ,
          RecommendsNegotiated ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          VolunteerPartyID ,
          programID
        )
select 
  hh.HearingID
, hv.fVolExcused
, hv.fVolReport
, hv.nVolRecAccept
, hv.nVolRecReject
, hv.nVolRecNeg
, getdate()
, -1
, getdate()
, -1
, cp.PartyID
, dbo.fn_ProgramID()
from [CometSource].[dbo].tblHearingVolunteer hv
join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = hv.nHearingID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = hv.nPersonID
