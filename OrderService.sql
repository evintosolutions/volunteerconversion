---------------------------------------------------------------------------------
--ORDERSERVICETYPE
---------------------------------------------------------------------------------


with t as (
select distinct
sService
from [CometSource].[dbo].tblCOService
where len(sService) > 0

union

select distinct
sServiceDesc sService
from [CometSource].[dbo].[_tblService]
where len(sServiceDesc) > 0

)

insert into [dbo].OrderServiceType
        ( OrderServiceTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sService
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sService not in (select OrderServiceTypeDesc from dbo.OrderServiceType where programid = dbo.fn_programid())
order by sService




-------------------------------------------------------------------------------
--ORDERSERVICE
--Dependencies: Hearing, CaseAffiliatedParty, OrderServiceType
---------------------------------------------------------------------------------
 
insert into [dbo].OrderService
        ( HearingID ,
          CaseAffiliatedPartyID ,
          OrderServiceTypeID ,
          SpecifiedCompletionDate ,
          ActualCompletionDate ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          CaseChildID ,
          CaseFamilyMemberID ,
          programID
        )
select hh.HearingID
,null
,ost.OrderServiceTypeID
,case when isdate(os.dCompletion) = 1 then os.dCompletion end
,case when isdate(os.dCompletion) = 1 and fCompleted in('1','Y') then os.dCompletion end
,getdate()
,-1
,getdate()
,-1
,cc.CaseChildID
,cfm.CaseFamilyMemberID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblCOService os
join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = os.nHearingID
join [dbo].OrderServiceType ost on ost.OrderServiceTypeDesc = os.sService and ost.programID = dbo.fn_ProgramID()
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = os.nPersonID
left join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.programID = dbo.fn_ProgramID()
left join [dbo].CaseFamilyMember cfm on cfm.FamilyPartyID = cp.PartyID and cfm.programID = dbo.fn_ProgramID()
