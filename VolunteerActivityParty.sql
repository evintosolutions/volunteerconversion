
---------------------------------------------------------------------------------
--VOLUNTEERACTIVITYPARTY
--Dependencies: VolunteerActivity, Party
---------------------------------------------------------------------------------

 
insert into [dbo].VolunteerActivityParty
        ( VolunteerActivityID ,
          PartyID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModeUserID ,
          ProgramID ,
          isSelected
        )
select distinct
   c.VolunteerActivityID
  ,cp.PartyID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
  ,1  
from [CometSource].[dbo].tblRelationshipShipVolunteerActivity va
join [CometSource].[dbo].ConversionVolunteerActivity c on c.nVolunteerActivityID = va.nVolunteerActivityID
and c.Source = 'RelationshipVolActivity'
join [CometSource].[dbo].tblPersonRelationship pr on pr.nPersonRelationshipID = va.nPersonRelationshipID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nPersonID
join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID()

go





--tblMeetingChild
insert into [dbo].VolunteerActivityParty
        ( VolunteerActivityID ,
          PartyID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModeUserID ,
          ProgramID ,
          isSelected
        )
select distinct
   c.VolunteerActivityID
  ,cp.PartyID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
  ,1   
from [CometSource].[dbo].tblMeetingChild mc
join [CometSource].[dbo].tblPersonMeeting pm on pm.nPersonMeetingID = mc.nPersonMeetingID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = mc.nPersonID
 join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype =1
 join [CometSource].[dbo].ConversionVolunteerActivity c on c.nVolunteerActivityID =pm.nPersonMeetingID
and c.Source = 'PersonMeeting'
 join dbo.VolunteerActivity va on va.VolunteerActivityiD = c.VolunteerActivityID
 and va.ProgramID = 7
 join dbo.VolunteerContactType con on con.VolunteerContactTypeDesc = 'Meeting'
 and con.VolunteerContactTypeID = va.VolunteerContactTypeID and con.programid = dbo.fn_programid()
 

--tblMeetingIntFamily
insert into [dbo].VolunteerActivityParty
        ( VolunteerActivityID ,
          PartyID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModeUserID ,
          ProgramID ,
          isSelected
        )
select distinct
   c.VolunteerActivityID
  ,cp.PartyID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
  ,1    
from [CometSource].[dbo].tblMeetingIntFamily mc
join [CometSource].[dbo].tblPersonMeeting pm on pm.nPersonMeetingID = mc.nPersonMeetingID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = mc.nPersonID
 join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype =5
 join [CometSource].[dbo].ConversionVolunteerActivity c on c.nVolunteerActivityID =pm.nPersonMeetingID
and c.Source = 'PersonMeeting'
 join dbo.VolunteerActivity va on va.VolunteerActivityiD = c.VolunteerActivityID
 and va.ProgramID = 7
 join dbo.VolunteerContactType con on con.VolunteerContactTypeDesc = 'Meeting'
 and con.VolunteerContactTypeID = va.VolunteerContactTypeID and con.programid = dbo.fn_programid()
 





---------------------------------------------------------------------------------
--VOLUNTEERACTIVITYPARTY
--Dependencies: VolunteerActivity, Party
---------------------------------------------------------------------------------

--tblMeetingIntFamily - Not Volunteers
insert into [dbo].VolunteerActivityOtherParty
        ( VolunteerActivityID ,
          OtherFirstName ,
		  OtherLastName,
          AddDate ,
          AddUserID,
          ProgramID 
        )
select distinct
   va.VolunteerActivityID
   ,p.FirstName
   ,p.LastName
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()   
from [CometSource].[dbo].tblPersonMeeting pm
     join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pm.nGroupID
	join [CometSource].[dbo].tblMeetingInvolvedPerson I on I.nPersonMeetingID = pm.nPersonMeetingID 
     join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = i.nPersonID
      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype not in (7,8)
	join [CometSource].[dbo].ConversionVolunteerActivity c on c.nVolunteerActivityID =pm.nPersonMeetingID
and (c.Source =  'PersonMeeting_NoVolActivity' or c.source = 'PersonMeeting')
 join dbo.VolunteerActivity va on va.VolunteerActivityID  = c.VolunteerActivityID
 and va.programID = dbo.fn_ProgramID()
 
-- union

-- select distinct
--   va.VolunteerActivityID
--   ,p.FirstName
--   ,p.LastName
--  ,getdate()
--  ,-1
--  ,dbo.fn_ProgramID()   

--  select * 
--from [CometSource].[dbo].tblPersonMeeting pm
--     join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pm.nGroupID
--	join [CometSource].[dbo].tblMeetingInvolvedPerson I on I.nPersonMeetingID = pm.nPersonMeetingID 
--     join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = i.nPersonID
--      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
--	  and p.partytype not in (7,8)
-- join [CometSource].[dbo].tblPersonMeeting pm2 on pm2.nGroupID= pm.nGroupID
-- join [CometSource].[dbo].tblMeetingInvolvedPerson I2 on I2.nPersonMeetingID = pm2.nPersonMeetingID 
--     join [CometSource].[dbo].ConversionPersonToParty cp2 on cp2.PersonID = i2.nPersonID
--      join [dbo].Party p2 on p2.PartyID = cp2.PartyID and p2.programID = dbo.fn_ProgramID() 
--	  and p2.partytype in (7,8)
--join dbo.VolunteerActivity va on va.CaseDetailID = cgc.CaseID and va.VolunteerPartyID = p2.PartyID
--and va.programID = dbo.fn_ProgramID()



