/* 
--INSERTED BELOW


insert into [dbo].Child
        ( PartyID, programID )
select c.PartyID, dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pc.nPersonID

*/



---------------------------------------------------------------------------------
--CHILD 
--Dependencies: Party
---------------------------------------------------------------------------------


 
insert into [dbo].Child
        ( PartyID, ReAbused) --, programID )
select c.PartyID, case when max(r.nChildID) is not null then 1 else 0 end --, dbo.fn_ProgramID()
from [dbo].Party p
join [CometSource].[dbo].ConversionPersonToParty c on p.PartyID = c.PartyID
left join [CometSource].[dbo].tblPersonChildReabuse r on r.nChildID = c.PersonID
where PartyType = (select top 1 PartyType from [dbo].PartyType where PartyTypeNameShort = 'Child' and programID = 1)
and not exists (select 1 from Child ch where ch.PartyID = p.PartyID)
group by c.PartyID
--having case when max(r.nChildID) is not null then 1 else 0 end = 1


