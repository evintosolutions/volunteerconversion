-----------------------------------------------------------------
--PlacementType
--Dependencies: 
---------------------------------------------------------------------------------

 
with t as (
select distinct 
f.sFacilityCat
from [CometSource].[dbo].tblFacility f
where len(f.sFacilityCat) > 0

union

select distinct 
f.sFacilityCatDesc sFacilityCat
from [CometSource].[dbo].[_tblFacilityCat] f
where len(f.sFacilityCatDesc) > 0

)

insert into [dbo].PlacementType
        ( PlacementTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sFacilityCat
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sFacilityCat not in (select PlacementTypeDesc from dbo.PlacementType where programid = dbo.fn_programid())
order by sFacilityCat




-----------------------------------------------------------------------------
--PlacementFacility
--Dependencies: PlacementType, LicensingAgency, County
---------------------------------------------------------------------------------
 
insert into [dbo].PlacementFacility
        ( FacilityName ,
          PlacementTypeID ,
          Address ,
          Address2 ,
          City ,
          State ,
          Zip ,
          CountyID ,
          SupervisorLastName ,
          SupervisorFirstName ,
          Phone ,
          Fax ,
          Email ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          LicensingAgencyID ,
          programId
        )
select distinct
f.sFacilityName
,pt.PlacementTypeID
,coalesce(f.sAddr1,'')
,f.sAddr2
,coalesce(f.sCity,'')
,coalesce(f.sState,'')
,coalesce(f.sZip,'')
,c.CountyID
,f.sSuperLName
,f.sSuperFName
,coalesce(f.sPhone,'') + coalesce(' ' + f.sExt,'')
,f.sFax
,f.sEmail
,1
,getdate()
,-1
,getdate()
,-1
,null--la.LicensingAgencyID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblFacility f
join [dbo].PlacementType pt on pt.PlacementTypeDesc = f.sFacilityCat and pt.programID = dbo.fn_ProgramID()
left join [dbo].County c on c.CountyName = f.sCounty and c.programID = dbo.fn_ProgramID()
--left join [dbo].LicensingAgency la on la.LicensingAgencyName = f.sFacilityName and la.programID = dbo.fn_ProgramID()
--left join [dbo].LicensingAgency la on la.LicensingAgencyName = f.sPlacementFacilityAgency and la.programID = dbo.fn_ProgramID()
--where len(f.sFacilityName) > 0
where f.sFacilityName not in 
(select FacilityName from dbo.PlacementFacility where programid = dbo.fn_programid() and
PlacementTypeID = pt.PlacementTypeID and
Address = coalesce(f.sAddr1,'') and 
Address2=f.sAddr2
and City =  coalesce(f.sCity,'')
and State = coalesce(f.sState,'')
and Zip = coalesce(f.sZip,'')
and CountyID = c.CountyID
and SupervisorLastName = f.sSuperLName
and SupervisorFirstName = f.sSuperFName
and Phone = coalesce(f.sPhone,'') + coalesce(' ' + f.sExt,'')
and Fax = f.sFax
and Email = f.sEmail)


