
---------------------------------------------------------------------------------
--HEARING VOLUNTEER ACTIVITY TYPE
--Dependencies: 
---------------------------------------------------------------------------------
 

with t as (
select distinct
sVolunteerActivity
from [CometSource].[dbo].tblHearingVolunteerActivity va
where len(ltrim(rtrim(va.sVolunteerActivity)) ) > 0

union

select distinct sVolunteerActivityDesc sVolunteerActivity
from [CometSource].[dbo].[_tblVolunteerActivity]
where len(ltrim(rtrim(sVolunteerActivityDesc))) > 0
)

insert into [dbo].HearingVolunteerActivityType
        ( HearingVolunteerActivityTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sVolunteerActivity
, 1
, getdate()
, -1
, getdate()
, -1
,dbo.fn_ProgramID()
from t
where sVolunteerActivity not in (select HearingVolunteerActivityTypeDesc from [dbo].HearingVolunteerActivityType where programid = dbo.fn_Programid())
order by sVolunteerActivity


---------------------
---------------------------------------------------------------------------------
--HEARING VOLUNTEER ACTIVITY
--Dependencies: HearingVolunteerActivityType, HearingVolunteerInput
---------------------------------------------------------------------------------
 
insert into [dbo].HearingVolunteerActivity
        ( HearingVolunteerInputID ,
          HearingVolunteerActivityTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select
  vi.HearingVolunteerInputID
, at.HearingVolunteerActivityTypeID
, getdate()
, -1
, getdate()
, -1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblHearingVolunteerActivity va
join [CometSource].[dbo].tblHearingVolunteer hv on hv.nHearingVolunteerID = va.nHearingVolunteerID
join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = hv.nHearingID
join [dbo].HearingVolunteerInput vi on vi.HearingID = hh.HearingID and vi.programID = dbo.fn_ProgramID()
join [dbo].HearingVolunteerActivityType at on at.HearingVolunteerActivityTypeDesc = va.sVolunteerActivity and at.programID = dbo.fn_ProgramID()
