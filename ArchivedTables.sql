

--Agency
/*
use [Volunteer];
go

insert into [Volunteer].[dbo].Agency
        ( AgencyName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sAgencyDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblCriminalCheckAgency]
where len(sAgencyDesc) > 0
order by sAgencyDesc

*/




--PartyType
/*
use [Volunteer];
go

insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Child', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Atty', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Case', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Int', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Family', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Judge', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Sprvsr', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Vol', 1, getdate(),-1,getdate(),-1)
insert into [Volunteer].[dbo].PartyType
        ( PartyTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID
        )
values  ( 'Vol', 1, getdate(),-1,getdate(),-1)
go

*/




------------------------------------------------------------------------------
--LicensingAgency
--Dependencies: 
---------------------------------------------------------------------------------
use [Volunteer];
go
 ----------------------------------------------

--with t as (
--  select distinct
--    sPlacementFacilityAgency
--  from [CometSource].[dbo]._tblPlacementFacilityAgency f
--  where len(sPlacementFacilityAgency) > 0

--union

--  select distinct
--    sPlacementFacilityAgency
--  from [CometSource].[dbo].tblFacility f
--  where len(sPlacementFacilityAgency) > 0
 
--)

--insert into [Volunteer].[dbo].LicensingAgency
--        ( LicensingAgencyName ,
--          Active ,
--          AddDate ,
--          AddUserID ,
--          ModDate ,
--          ModUserID ,
--          programID
--        )
--select distinct
----sFacilityName
--sPlacementFacilityAgency
--,1
--,getdate()
--,-1
--,getdate()
--,-1
--,dbo.fn_ProgramID()
--from t
--where len(sPlacementFacilityAgency) > 0
--order by sPlacementFacilityAgency




---------------------------------------------------------------------------------
--CaseClosingLog
--Dependencies: Case
---------------------------------------------------------------------------------
/*
use [Volunteer];
go
 

if not exists (select 1 from [Volunteer].[dbo].SystemUser where Name = 'Empty' and programID = dbo.fn_ProgramID())
insert into [Volunteer].[dbo].SystemUser
        ( UserName ,
          Name ,
          LastName ,
          Email ,
          AdminRole ,
          StaffRole ,
          VolunteerRole ,
          SupervisorRole ,
          PartyID ,
          UserPassword ,
          programID
        )
values  ( 'Empty' , -- UserName - nvarchar(128)
          'Empty' , -- Name - nvarchar(60)
          'Empty' , -- LastName - nvarchar(60)
          'Empty@Empty.com' , -- Email - nvarchar(128)
          0 , -- AdminRole - bit
          0 , -- StaffRole - bit
          0 , -- VolunteerRole - bit
          0 , -- SupervisorRole - bit
          0 , -- PartyID - int
          '123456' , -- UserPassword - nvarchar(128)
          dbo.fn_ProgramID()  -- programID - int
        )

declare @UserID int;
select @UserID = UserID from [Volunteer].[dbo].SystemUser where Name = 'Empty' and programID = dbo.fn_ProgramID();

insert into [Volunteer].[dbo].CaseClosingLog (
  LogDate
  ,LogType
  ,UserID
  ,CaseID
  ,programID
)
select distinct
dProgramClosed
,'C'
,@UserID
,cgc.CaseID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty cpp on cpp.PersonID = pc.nPersonID
join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pc.nGroupID
where isdate(dProgramClosed) = 1
*/



