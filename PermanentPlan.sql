


---------------------------------------------------------------------------------
--PermanentPlan 
--Dependencies: Party, CaseInfo, PermanentPlanType
---------------------------------------------------------------------------------


with t as (
select distinct 
sPlanType
from [CometSource].[dbo].tblCOPlan
where len(sPlanType) > 0

union

select distinct 
nPlanTypeDesc sPlanType
from [CometSource].[dbo].[_tblPlanType]
where len(nPlanTypeDesc) > 0

--union all

--select distinct 
--sPlanType
--from [CometSource].[dbo].[_tblFosterCarePlanType]
--where len(sPlanType) > 0

)

insert into [dbo].PermanentPlanType
        ( PermanentPlanTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sPlanType
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sPlanType not in (select PermanentPlanTypeDesc  from [dbo].PermanentPlanType where programid = dbo.fn_programid())
order by sPlanType




------------------------------------------------------------------------------
--PermanentPlan
--Dependencies: Party, CaseInfo, PermanentPlanType
---------------------------------------------------------------------------------

insert into [dbo].PermanentPlan
        ( HearingID ,
          CaseChildID ,
          PermanentPlanTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct
hh.HearingID
,cc.CaseChildID
,ppt.PermanentPlanTypeID
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblCOPlan p
join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = p.nHearingID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = p.nPersonID
join [dbo].Hearing h on h.HearingID = hh.HearingID
join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.CaseID = h.CaseID and cc.programID = dbo.fn_ProgramID()
join [dbo].PermanentPlanType ppt on ppt.PermanentPlanTypeDesc = p.sPlanType and ppt.programID = dbo.fn_ProgramID()

---------------
