
---------------------------------------------------------------------------------
--PETITION CHILD
--Dependencies: Petition
---------------------------------------------------------------------------------


if not exists (select 1 from INFORMATION_SCHEMA.columns where TABLE_NAME = 'PetitionChild' and COLUMN_NAME = 'nPetitionID') 
alter table PetitionChild add nPetitionID int null;
go
 
insert into [dbo].PetitionChild
        ( PetitionID ,
          CaseChildID ,
          programID ,
          nPetitionID
        )
select
-- pp.PetitionID
op.PetitionID
, cc.CaseChildID
,dbo.fn_ProgramID()
,p.nPetitionID
from [CometSource].[dbo].tblPetition p
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = p.nPersonID
join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.programID = dbo.fn_ProgramID()
--Made change for Issue 747, item 4. 
--join [CometSource].[dbo].ConversionPetitionToPetition pp on pp.sPetitionNum = p.sPetitionNum
--Issue 747: Adding join to PetitionCaseType
left join [dbo].PetitionCaseType ct on ct.PetitionCaseTypeDesc = p.sPetitionCat and ct.programId = dbo.fn_ProgramID()
left join dbo.Petition op on op.PetitionNumber = p.sPetitionNum
and op.CaseID = cc.CaseID
and (op.PetitionCaseTypeID = ct.PetitionCaseTypeID
or op.PetitionCaseTypeID is null)
where op.PetitionID is not null