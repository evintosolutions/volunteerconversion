
update v
set v.Available= 0
 from dbo.Volunteer v
join dbo.Party p on p.PartyID = v.PartyID
and p.programID =dbo.fn_ProgramID()
where v.DateDischarged is not null
and v.Available = 1

update p
set p.Active = 0
 from dbo.Party p
join dbo.volunteer v on p.PartyID = v.PartyID
and p.programID =dbo.fn_ProgramID()
where v.DateDischarged is not null
and p.Active = 1
 

update v
set Available = 1
from [dbo].Party p
join [dbo].Volunteer v on v.PartyID = p.PartyID
where p.programID = dbo.fn_ProgramID()
and v.IsSupervisor = 1
and p.Active = 1


update dbo.Volunteer
set paidstaff = 1
from dbo.Volunteer v
where IsSupervisor = 1
and exists(select  1 from  [dbo].Party p where p.PartyID = v.PartyID and p.ProgramID = dbo.fn_ProgramID())


update cv
set ReleasedDate = null
, CaseReleaseReasonID = null
from [dbo].CaseVolunteerChild cvc
join [dbo].CaseVolunteer cv on cvc.CaseVolunteerID = cv.CaseVolunteerID
where cvc.ReleasedDate is null
and cvc.programID = dbo.fn_ProgramID();




update ci
set CloseDate = null
,CourtClosureDate = null
,CourtClosureReasonID = null
,ProgramClosureDate = null
,ProgramClosureReasonID = null
,IsClosed = 0
from [dbo].CaseVolunteerChild cvc
join [dbo].CaseChild cc on cvc.CaseChildID = cc.CaseChildID
join [dbo].CaseInfo ci on ci.CaseID = cc.CaseID
where cvc.ReleasedDate is null
and cvc.programID = dbo.fn_ProgramID();



if object_id('tempdb..#casechildref') is not null drop table #casechildref;



with cc as (
  select ch.CaseID, min(ch.ProgramClosureDate) MinClosureDate, max(ch.ProgramClosureDate) MaxClosureDate
  from [dbo].CaseInfo ci
  join [dbo].CaseChild ch on ci.CaseID = ch.CaseID
  where ci.programID = dbo.fn_ProgramID()  
  group by ch.CaseID
  having min(case when ch.ProgramClosureDate is null then 0 else 1 end) > 0
)
select ch.CaseID, max(ch.CaseChildID) RefCaseChildID
into #casechildref
from cc
join [dbo].CaseChild ch on ch.CaseID = cc.CaseID and ch.ProgramClosureDate = cc.MaxClosureDate
group by ch.CaseID

update ci
set CloseDate = ch.ProgramClosureDate
,CourtClosureDate = ch.CourtClosureDate
,CourtClosureReasonID = ch.CourtClosureReasonID
,ProgramClosureDate = ch.ProgramClosureDate
,ProgramClosureReasonID = ch.ProgramClosureReasonID
,IsClosed = 1
from [dbo].CaseInfo ci
join #casechildref ref on ref.CaseID = ci.CaseID
join [dbo].CaseChild ch on ch.CaseChildID = ref.RefCaseChildID
where ci.programID = dbo.fn_ProgramID()




declare @defaultuserid int; 
select top 1 @defaultuserid = UserID from [dbo].SystemUser where Email = 'administrator@evintosolutions.com';
insert into [dbo].CaseClosingLog (
  LogDate
, LogType
, UserID
, CaseID
, programID
--, CourtClosureDate
--, CourtClosureReasonID
--, ProgramClosureDate
--, ProgramClosureReasonID
--, CourtReOpenDate
--, CourtReOpenReasonID
--, ProgramReOpenDate
--, ProgramReOpenReasonID
)
select 
ch.ProgramClosureDate
,'C'
,@defaultuserid 
,ch.CaseID
,dbo.fn_ProgramID()
from #casechildref ref
join [dbo].CaseChild ch on ch.CaseChildID = ref.RefCaseChildID
where ch.programID = dbo.fn_ProgramID()
and not exists (select 1 from dbo.CaseClosingLog ccl where ccl.CaseID = ch.CaseID and ccl.programID = dbo.fn_ProgramID());



if not exists (select 1 from sys.indexes where name = 'ix_temp_casechildref_CaseID') create index ix_temp_casechildref_CaseID on #casechildref (CaseID);
if not exists (select 1 from sys.indexes where name = 'ix_temp_casechildref_RefCaseChildID') create index ix_temp_casechildref_RefCaseChildID on #casechildref (CaseID);
if not exists (select 1 from sys.indexes where name = 'ix_temp_CaseVolunteer') create index ix_temp_CaseVolunteer on CaseVolunteer (CaseID);
if not exists (select 1 from sys.indexes where name = 'ix_temp_CaseChild') create index ix_temp_CaseChild on CaseChild (CaseChildID);




update cv
set ReleasedDate = ch.ProgramClosureDate
--,CaseReleaseReasonID = 
from [dbo].CaseVolunteer cv
join #casechildref ref on ref.CaseID = cv.CaseID
join [dbo].CaseChild ch on ch.CaseChildID = ref.RefCaseChildID
where cv.ReleasedDate is null
--Issue 1415
--or ch.programclosuredate > cv.ReleasedDate
and cv.programID = dbo.fn_ProgramID();



update cvc
set ReleasedDate = ch.ProgramClosureDate
--,CaseReleaseReasonID = 
from [dbo].CaseVolunteerChild cvc
join #casechildref ref on exists (select 1 from [dbo].CaseChild ch where ch.CaseID = ref.CaseID and ch.CaseChildID = cvc.CaseChildID)
join [dbo].CaseChild ch on ch.CaseChildID = ref.RefCaseChildID
where cvc.ReleasedDate is null
and cvc.programID = dbo.fn_ProgramID();



update pcap
set ReleasedDate = ch.ProgramClosureDate
from [dbo].PartyCaseAssociatedParty pcap
join [dbo].CaseAssociatedParty cap on pcap.CaseAssociatedPartyID = cap.CaseAssociatedPartyID
join #casechildref ref on ref.CaseID = cap.CaseInfoID
join [dbo].CaseChild ch on ch.CaseChildID = ref.RefCaseChildID
--Adding where clause per issue 846
where (ReleasedDate is null
--Taking out this condition per issue 846
--or ch.ProgramClosureDate > ReleasedDate
)
and pcap.ProgramID = dbo.fn_ProgramID()

update p
set active = 0
from [dbo].Party p 
join dbo.CaseChild cc on cc.ChildPartyID = p.PartyID
join #casechildref ref on ref.RefCaseChildID = cc.CaseChildID
where p.programID = dbo.fn_ProgramID()


--Issue 721
update v
set v.Assigned = 1
 from dbo.Volunteer v
join dbo.CaseVolunteer cv on cv.PartyID = v.PartyID 
and cv.programID = dbo.fn_ProgramID() and v.programID is null
where cv.ReleasedDate is null
and v.Assigned <> 1



drop index ix_temp_casechildref_CaseID on #casechildref;
drop index ix_temp_casechildref_RefCaseChildID on #casechildref;
drop index ix_temp_CaseVolunteer on CaseVolunteer;
drop index ix_temp_CaseChild on CaseChild;



------------------------------
--FIX ACTIVE STATUS IN ONE WAY
------------------------------
 

update p
set Active = case 
              when len(pv.dDateSworn) = 0 or pv.dDateSworn is null then 0
              when isdate(pv.dDateDischarge) = 1 or pv.dDateDischarge is not null  then 0
             else 1 end
from dbo.Party p
join [CometSource].dbo.ConversionPersonToParty cpp on cpp.PartyID = p.PartyID
join [CometSource].dbo.tblPersonVolunteer pv on pv.nPersonID = cpp.PersonID
where p.programID = dbo.fn_ProgramID()




if exists (select 1 from INFORMATION_SCHEMA.columns where TABLE_NAME = 'PetitionChild' and COLUMN_NAME = 'nPetitionID') alter table dbo.PetitionChild drop column nPetitionID;




------------------------------------
------------------------------------
/*	
* 
*	RowNumber will count down to show progress
*	Cases that are not updated will be skipped (not shown in output)
*
*	Revised 4 Mar 2013, 
*		- added drop table at beginning of script
*		- Removed select statement that displays progress, SQL Manager is running out of memory on large batches.
*		- To explicitly reduce batch size, uncomment line 43 "Top XXXX" where XXXX is the number of records in batch
*/
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#StartDate') IS NOT NULL DROP TABLE #StartDate
GO

DECLARE @ProgramID int
/*SET PROGRAM ID**/
SET @ProgramID = dbo.fn_ProgramID()
/*****************/

declare @RowCount int
DECLARE @CaseID int
DECLARE @PetitionDate DATETIME
DECLARE @CaseStartDate DATETIME

--Create temp table
CREATE TABLE #StartDate(
	ID int identity(1, 1)
	,CaseID int
	,PetitionDate DATETIME
	,CaseStartDate DATETIME
)

--Load Temp table
insert into #StartDate
(
	CaseID
	,PetitionDate
	,CaseStartDate
)
select 
	--TOP 10000
		Petition.CaseID
		, Min(Petition.petitiondate) PetitionDate 
		, CaseInfo.StartDate
	from Petition 
	join CaseInfo on CaseInfo.CaseID = Petition.CaseID
	where Petition.programID = @ProgramID 
		AND Petition.PetitionDate IS NOT NULL
		AND Petition.PetitionEndDate IS NULL
	group by Petition.CaseID, CaseInfo.StartDate

--Store row count
select @RowCount = COUNT(*) from #StartDate

--Loop through temp table, no cursors please!
WHILE @RowCount > 0
BEGIN
	-- read row from temp table
	select 
		@CaseID = CaseID
		,@PetitionDate = PetitionDate
		,@CaseStartDate = CaseStartDate
		from #StartDate WHERE ID = @RowCount

	-- update caseinfo.startdate if:
	IF @CaseStartDate is NULL OR (@CaseStartDate > @PetitionDate)
	BEGIN
	/* suppress display - SQL Manager runs out of memory. 
		-- display progress
		SELECT @RowCount AS RowNumber, @CaseID AS CaseID, @CaseStartDate AS CaseStartDate, @PetitionDate AS FirstPetitionDate
	*/
		-- update record
		UPDATE CaseInfo 
			SET StartDate = @PetitionDate
			WHERE CaseID = @CaseID
	END	
	
	SET @RowCount = @RowCount - 1
END

--Remove temp table
IF OBJECT_ID('tempdb..#StartDate') IS NOT NULL DROP TABLE #StartDate

------------------------------------------------------------------------
-------------------------------------------------------------------------


--Issue 747: Update CaseChild.InitialNotes with the Petition End Date for situtations where multiple children on same Petition, and some have EndDate, whereas others have null EndDate
update dbo.CaseChild
set InitialNotes =
InitialNotes + coalesce(' Petition:' + p.sPetitionNum + '  End Date:' + convert(varchar,month(p.dEndDate)) + '/' + convert(varchar,day(p.dEndDate)) + '/' + convert(varchar,year(p.dEndDate))
,'')
from dbo.CaseChild cc
join [CometSource].[dbo].ConversionPersonToParty cp on cc.ChildPartyID = cp.PartyID and cc.programID =dbo.fn_ProgramID()
join CometSource.dbo.tblPetition p on cp.PersonID = p.nPersonID
left join [dbo].PetitionCaseType ct on ct.PetitionCaseTypeDesc = p.sPetitionCat and ct.programId = dbo.fn_ProgramID()
where p.dEndDate is not null 
--and 
--exists (select sPetitionNum from CometSource.dbo.tblPetition
--	  where sPetitionNum = p.sPetitionNum
--	  and dEndDate is null) 
and p.sPetitionNum <> '1'
    and len(p.sPetitionNum) > 2


--Issue 975
update dbo.CaseInfo 
	set countyid = cc.ResidencyCountyID
	from dbo.caseInfo CI
	join dbo.CaseChild cc on cc.CaseID = ci.CaseID 
	and cc.programID = dbo.fn_ProgramID() and ci.programID = dbo.fn_ProgramID()
	where ci.CountyID is null and cc.ResidencyCountyID is not null


--Issue 1410.2
update dbo.PlacementHistory
set PlacementToDate = cc.closedate
from dbo.PlacementHistory p
join dbo.CaseChild cc on cc.CaseChildID = p.CaseChildID
and p.programID = dbo.fn_ProgramID()
and p.PlacementToDate is null
and cc.CloseDate is not null


--Issue 1410.1
update dbo.CaseInfo
set ClosingNotes = 'Closed: ' + convert(varchar(500),month([CloseDate])) +'/'+ convert(varchar(500),day([CloseDate])) +'/'+ convert(varchar(500),year([CloseDate])) 
 + char(10)+ClosingNotes 
 from dbo.CaseInfo where 
 programID = dbo.fn_programid()
 and CloseDate is not null

--Issue 1405.18
update dbo.Volunteer
set SupervisorPartyID = null 
from dbo.Volunteer v
join dbo.Party p on p.PartyID = v.PartyID
where v.PartyID = v.SupervisorPartyID
and p.programID = dbo.fn_ProgramID()



--Issue 1444
update dbo.Party
set BestPhone = (
case 
when p.CellPhone is null and p.WorkPhone is null then 1
when p.HomePhone is null and p.WorkPhone is null then 2
when p.CellPhone is null and p.HomePhone is null then 3
when v.IsSupervisor = 1 and p.WorkPhone is not null then 3
when p.HomePhone is not null then 1
when p.CellPhone is not null then 2
end 
)
from dbo.Party p
left join dbo.Volunteer v on v.partyid = p.partyid 
where p.PartyType in (7,8,9)
and p.BestPhone is null
and (p.HomePhone is not null or p.WorkPhone is not null or p.CellPhone is not null)
and p.programID = dbo.fn_ProgramID()


update dbo.Party
set BestEmail = (
case 
when p.WorkEmail is null then 1
when p.HomeEmail is null then 2
when v.IsSupervisor = 1 and p.WorkEmail is not null then 2
when p.HomeEmail is not null then 1
when p.WorkEmail is not null then 2
end 
)
from dbo.Party p
left join dbo.Volunteer v on v.partyid = p.partyid 
where p.PartyType in (7,8,9)
and p.BestEmail is null
and (p.HomeEmail is not null or p.WorkEmail is not null)

and p.programID = dbo.fn_ProgramID()


update cvv
 set cvv.ContactLogApprovalPartyID = cvs.PartyID
 from dbo.CaseVolunteer cvv
 join dbo.Volunteer v on cvv.PartyID = v.PartyID and v.IsVolunteer = 1 and cvv.CaseSupervisor = 0
 join dbo.CaseVolunteer cvs on cvs.CaseID = cvv.CaseID and cvs.CaseSupervisor = 1 
join dbo.CaseInfo ci on ci.CaseID = cvv.CaseID
 where cvs.ReleasedDate is null and cvv.ReleasedDate is null
 and ci.programID = dbo.fn_programid()
 and cvv.programID = dbo.fn_programid()

update dbo.volunteer
set lastbackgroundcheckdate =  vccl.backgroundcheckdate
from dbo.Volunteer v
join dbo.VolunteerCertifyCheckLog vccl on vccl.PartyID = v.PartyID
join dbo.Party p on p.PartyID = vccl.PartyID 
and p.programID = dbo.fn_ProgramID()
and vccl.programId = dbo.fn_ProgramID()
where vccl.backgroundcheckdate is not null  

update dbo.volunteer
set DriversLicenseConfirmDate =  vccl.AutoCheckDate
from dbo.Volunteer v
join dbo.VolunteerCertifyCheckLog vccl on vccl.PartyID = v.PartyID
join dbo.Party p on p.PartyID = vccl.PartyID 
and p.programID = dbo.fn_ProgramID()
and vccl.programId = dbo.fn_ProgramID()
where vccl.AutoCheckDate is not null  


update dbo.volunteer
set AutoInsurConfirmDate  =  vccl.InsuranceCheckDate
from dbo.Volunteer v
join dbo.VolunteerCertifyCheckLog vccl on vccl.PartyID = v.PartyID
join dbo.Party p on p.PartyID = vccl.PartyID 
and p.programID = dbo.fn_ProgramID()
and vccl.programId = dbo.fn_ProgramID()
where vccl.InsuranceCheckDate is not null  