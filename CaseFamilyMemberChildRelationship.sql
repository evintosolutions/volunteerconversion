
---------------------------------------------------------------------------------
--CASE FAMILY MEMBER CHILD RELATIONSHIP
--Dependencies: CaseChild, RelationshipType
---------------------------------------------------------------------------------

 
insert into [dbo].CaseFamilyMemberChildRelationship
        ( CaseFamilyMemberID ,
          CaseChildID ,
          RelationshipTypeID ,
          programID
        )
select
  cfm.CaseFamilyMemberID
, cc.CaseChildID
, rt.RelationshipTypeID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonRelationship pr
join [CometSource].[dbo].tblRelationshipChildFamily cf on pr.nPersonRelationshipID = cf.nPersonRelationshipID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nPersonID
join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.programID = dbo.fn_ProgramID()
join [CometSource].[dbo].ConversionPersonToParty cpf on cpf.PersonID = pr.nRelationshipID
join [dbo].CaseFamilyMember cfm on cfm.CaseID = cc.CaseID and cfm.FamilyPartyID = cpf.PartyID
join [dbo].RelationshipType rt on rt.RelationshipTypeName = cf.sRelationID and rt.programID = dbo.fn_ProgramID()
