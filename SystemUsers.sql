

declare @tablename varchar(255);
set @tablename = 'SystemUser_backup_'+replace(replace(replace((replace(dbo.fn_programname(),' ','_')),'.','_'),'-',''),',','') + convert(char(4),year(getdate())) + case when len(month(getdate())) < 2 then '0' + convert(char(1),month(getdate())) else convert(char(2),month(getdate())) end + case when len(day(getdate())) < 2 then '0' + convert(char(1),day(getdate())) else convert(char(2),day(getdate())) end
declare @cmd varchar(max);

select @cmd = 
'insert into dbo.SystemUser' +
        '( UserName ,'+
          'Name ,'+
          'LastName ,'+
          'Email ,'+
          'AdminRole ,'+
          'StaffRole ,'+
          'VolunteerRole ,'+
          'SupervisorRole ,'+
          'PartyID ,'+
          'UserPassword ,'+
          'programID)'+
' select su.UserName' +
'  , su.name' + 
'  , su.LastName' +
'  , su.Email' +
'  , su.AdminRole' +
'  , su.StaffRole' +
'  , su.VolunteerRole' +
'  , su.SupervisorRole' +
'  , max(p.PartyID)' +
'  , su.UserPassword' +
'  , ' + convert(varchar,dbo.fn_ProgramID()) + 
' from ' + @tablename + ' su' +
' left join Party p on ((p.LastName = su.LastName and p.FirstName = su.name) or p.HomeEmail = su.Email or p.WorkEmail = su.Email)' +
' and exists (select 1 from dbo.Volunteer v where v.PartyID = p.PartyID and p.programID = su.programID)' +
' where not exists (select 1 from SystemUser su2 where su2.UserName = su.UserName and su2.programID = su.programID)' +
' and su.programID = ' + convert(varchar,dbo.fn_ProgramID()) + 
' group by' +
' su.UserName' +
'  , su.name' + 
'  , su.LastName' +
'  , su.Email' +
'  , su.AdminRole' +
'  , su.StaffRole' +
'  , su.VolunteerRole' +
'  , su.SupervisorRole' +
'  , su.UserPassword;';

exec(@cmd);