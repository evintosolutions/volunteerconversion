

with t as

(
select distinct
sEducation
from [CometSource].[dbo].tblPersonVolunteer
where len(sEducation) > 0
and sEducation not in (select EducationTypeDesc from dbo.EducationType where programID = dbo.fn_ProgramID())
union

select distinct
sEducation
from [CometSource].[dbo].tblPersonFamily
where len(sEducation) > 0
and sEducation not in (select EducationTypeDesc from dbo.EducationType where programID = dbo.fn_ProgramID())

--union

--select distinct sVolunteerEducation sEducation
--from [CometSource].[dbo].[_tblVolunteerEducation]
--where len(sVolunteerEducation) > 0

)

insert into [dbo].EducationType
        ( EducationTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sEducation
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
order by sEducation;

------------------------------



insert into [dbo].Education
        ( PartyID ,
          EducationTypeID ,
          DegreeID ,
          Major ,
          GraduationDate ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
c.PartyID
, t.EducationTypeID
,null
,null
,null
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer p
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
join [dbo].EducationType t on t.EducationTypeDesc = p.sEducation and t.programID = dbo.fn_ProgramID()

------------------------------

