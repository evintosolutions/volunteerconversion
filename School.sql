
--SchoolType

insert into [dbo].SchoolType
        ( SchoolTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select 
  'Educ'
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
  where 'Educ' not in (select SchoolTypeDesc from dbo.SchoolType where programID = dbo.fn_ProgramID())


--School

insert into [dbo].School
        ( SchoolName ,
          SchoolTypeID ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sSchool
,st.SchoolTypeID
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [dbo].SchoolType st on 
--st.SchoolTypeDesc = 'Educ' and 
st.programID = dbo.fn_ProgramID()
where len(sSchool) > 0
and sSchool not in (select SchoolName from dbo.school where programid = dbo.fn_programid())
order by sSchool



 
insert into [dbo].SchoolHistory
        ( ChildPartyID ,
          SchoolID ,
          SchoolStartDate ,
          SchoolEndDate ,
          SchoolGrade ,
          ContactName ,
          ContactTitle ,
          ContactPhone ,
          ContactEmail ,
          Notes ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select
  c.PartyID
  ,sc.SchoolID
  ,null
  ,null
  ,pc.sGrade
  ,null
  ,null
  ,null
  ,null
  ,null
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pc.nPersonID
join [dbo].School sc on sc.SchoolName = pc.sSchool and sc.programID = dbo.fn_ProgramID()
--where pc.sGrade is not null
