--In SSMS, Enable SQLCMD mode. Menu Query>SQLCMD
--:ON ERROR EXIT
--GO
--:CONNECT localhost -U <loginnamne> -P <password>
--use [Volunteer]
--go

:r C:\EvintoClients\CodeRepos\SourceDataCleanup.sql
:r C:\EvintoClients\CodeRepos\Quant_Counts_Comet_Access.sql
:r C:\EvintoClients\CodeRepos\Quant_Counts_Comet_Import.sql

--Check 1078, number 16, 17

:r C:\EvintoClients\CodeRepos\InitDB.sql
:r C:\EvintoClients\CodeRepos\Ethnicity.sql
:r C:\EvintoClients\CodeRepos\Jurisdiction.sql
:r C:\EvintoClients\CodeRepos\County.sql
:r C:\EvintoClients\CodeRepos\EmploymentStatus.sql
:r C:\EvintoClients\CodeRepos\Party.sql
:r C:\EvintoClients\CodeRepos\VolunteerLookupTables.sql
:r C:\EvintoClients\CodeRepos\VolunteerSupervisor.sql
:r C:\EvintoClients\CodeRepos\Volunteer.sql
:r C:\EvintoClients\CodeRepos\VolunteerJurisdiction.sql
:r C:\EvintoClients\CodeRepos\FamilyMember.sql
:r C:\EvintoClients\CodeRepos\ApplicantReference.sql
:r C:\EvintoClients\CodeRepos\AssociatedParty.sql
:r C:\EvintoClients\CodeRepos\ApplicantTraining.sql
:r C:\EvintoClients\CodeRepos\BackgroundCheck.sql
:r C:\EvintoClients\CodeRepos\Disability.sql
:r C:\EvintoClients\CodeRepos\Education.sql
:r C:\EvintoClients\CodeRepos\EmergencyContact.sql
:r C:\EvintoClients\CodeRepos\Employment.sql
:r C:\EvintoClients\CodeRepos\Interview.sql
--Archiving Attorney script per issue 813
--:r C:\EvintoClients\CodeRepos\Attorney.sql
:r C:\EvintoClients\CodeRepos\Language.sql
:r C:\EvintoClients\CodeRepos\Abuse.sql
:r C:\EvintoClients\CodeRepos\GeoLocation.sql
:r C:\EvintoClients\CodeRepos\School.sql
:r C:\EvintoClients\CodeRepos\TrainingActivity.sql
:r C:\EvintoClients\CodeRepos\CaseClosureReason.sql
:r C:\EvintoClients\CodeRepos\CaseInfo.sql
:r C:\EvintoClients\CodeRepos\Child.sql
--:r C:\EvintoClients\CodeRepos\CaseClosinglog.sql
:r C:\EvintoClients\CodeRepos\HearingType.sql
:r C:\EvintoClients\CodeRepos\EligibleStatus.sql
:r C:\EvintoClients\CodeRepos\CaseChildStatus.sql
:r C:\EvintoClients\CodeRepos\ChildReferral.sql
:r C:\EvintoClients\CodeRepos\FinalPlacementType.sql
:r C:\EvintoClients\CodeRepos\CaseChild.sql
:r C:\EvintoClients\CodeRepos\CaseChildCaseChildStatus.sql
:r C:\EvintoClients\CodeRepos\Eligible.sql
:r C:\EvintoClients\CodeRepos\RelationshipType.sql
:r C:\EvintoClients\CodeRepos\CaseChildRemovedFrom.sql
:r C:\EvintoClients\CodeRepos\CaseReleaseReason.sql
:r C:\EvintoClients\CodeRepos\CaseVolunteer.sql
:r C:\EvintoClients\CodeRepos\CaseVolunteerChild.sql
:r C:\EvintoClients\CodeRepos\CaseFamilyMember.sql
:r C:\EvintoClients\CodeRepos\CaseFamilyMemberChildRelationship.sql
:r C:\EvintoClients\CodeRepos\Concerns.sql
:r C:\EvintoClients\CodeRepos\FamilyMemberConcern.sql
:r C:\EvintoClients\CodeRepos\HearingLocation.sql
:r C:\EvintoClients\CodeRepos\HearingStatus.sql
:r C:\EvintoClients\CodeRepos\Judge.sql
:r C:\EvintoClients\CodeRepos\Hearing.sql
:r C:\EvintoClients\CodeRepos\HearingChild.sql
:r C:\EvintoClients\CodeRepos\HearingDetail.sql
:r C:\EvintoClients\CodeRepos\HearingFamily.sql
:r C:\EvintoClients\CodeRepos\HearingOutcome.sql
:r C:\EvintoClients\CodeRepos\OrderService.sql
:r C:\EvintoClients\CodeRepos\Petition.sql
:r C:\EvintoClients\CodeRepos\PetitionChild.sql
--Taking out per issue 1078, number 18
--:r C:\EvintoClients\CodeRepos\HearingPetition.sql
:r C:\EvintoClients\CodeRepos\HearingVolunteerInput.sql
:r C:\EvintoClients\CodeRepos\HearingVolunteerActivity.sql
:r C:\EvintoClients\CodeRepos\VolunteerActivity.sql
:r C:\EvintoClients\CodeRepos\VolunteerActivityParty.sql
:r C:\EvintoClients\CodeRepos\Allegation.sql
:r C:\EvintoClients\CodeRepos\CaseAssociatedParty.sql
:r C:\EvintoClients\CodeRepos\PermanentPlan.sql
:r C:\EvintoClients\CodeRepos\LicensingAgency.sql
:r C:\EvintoClients\CodeRepos\PlacementFacility.sql
:r C:\EvintoClients\CodeRepos\PlacementHistory.sql
:r C:\EvintoClients\CodeRepos\Visitation.sql
:r C:\EvintoClients\CodeRepos\PartyCaseAssociatedParty.sql
:r C:\EvintoClients\CodeRepos\SystemUsers.sql
:r C:\EvintoClients\CodeRepos\Note.sql
:r C:\EvintoClients\CodeRepos\ChildClosingLog.sql
:r C:\EvintoClients\CodeRepos\FinalUpdates.sql
:r C:\EvintoClients\CodeRepos\FinalUpdates_CodeTables.sql
--
:r C:\EvintoClients\CodeRepos\ReLinkVolSupervisors.sql
:r C:\EvintoClients\CodeRepos\Quant_Counts_Optima.sql
