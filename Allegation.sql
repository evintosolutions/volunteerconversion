
---------------------------------------------------------------------------------
--ALLEGATIONOUTCOME
--Dependencies: CaseChild, CaseFamilyMember, AbuseType, HearingOutcome
---------------------------------------------------------------------------------

 
insert into [dbo].AllegationOutcome
        ( Description ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          Active ,
          programID
        )
select 'Substantiataed', getdate(), -1, getdate(), -1, 1, dbo.fn_ProgramID()
where 'Substantiataed' not in (select Description from dbo.AllegationOutcome where programID = dbo.fn_ProgramID())

insert into [dbo].AllegationOutcome
        ( Description ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          Active ,
          programID
        )
select 'Dismissed', getdate(), -1, getdate(), -1, 1, dbo.fn_ProgramID()
where 'Dismissed' not in (select Description from dbo.AllegationOutcome where programID = dbo.fn_ProgramID())





---------------------------------------------------------------------------------
--ALLEGATION
--Dependencies: CaseChild, CaseFamilyMember, AbuseType, HearingOutcome
---------------------------------------------------------------------------------

 
insert into [dbo].Allegation
        ( PetitionChildID ,
          CaseFamilyMemberID ,
          AllegationAbuseTypeID ,
          NegotiatedAbuseTypeID ,
          AllegationOutcomeID ,
          programID
        )
select distinct
pc.PetitionChildID
,fm.CaseFamilyMemberID
,at.AbuseTypeID
,null--at2.AbuseTypeID
,ao.AllegationOutcomeID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblAllegation a
join [dbo].AbuseType at on at.AbuseTypeDesc = a.sAllegationCat and at.programID = dbo.fn_ProgramID()
join [CometSource].[dbo].tblPetition pet on pet.nPetitionID = a.nPetitionID
join [CometSource].[dbo].ConversionPetitionToPetition pp on pp.sPetitionNum = pet.sPetitionNum 
join [dbo].PetitionChild pc on pc.PetitionID = pp.PetitionID and pc.programID = dbo.fn_ProgramID() and pc.nPetitionID = a.nPetitionID
left join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = a.nPersonID
left join [dbo].CaseFamilyMember fm on fm.FamilyPartyID = c.PartyID and fm.programID = dbo.fn_ProgramID()
left join [CometSource].[dbo].tblCOAdjucation adj on a.nAllegationID = adj.nAllegationID
--left join [dbo].AbuseType at2 on at2.AbuseTypeDesc = adj.sNegAllegationCat and at2.programID = dbo.fn_ProgramID()
left join [dbo].AllegationOutcome ao on ao.Description = adj.sOutcome and ao.programID = dbo.fn_ProgramID()
