
--FinalPlacementType
with t as(
  select distinct sFinalPlacementDesc
  from [CometSource].[dbo].[_tblFinalPlacement]
  where len(sFinalPlacementDesc) > 1
  
  union
  
  select distinct sFinalPlacement sFinalPlacementDesc
  from [CometSource].[dbo].tblPersonChild
  where len(sFinalPlacement) > 1
)

insert into [dbo].FinalPlacementType
        ( FinalPlacementTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sFinalPlacementDesc
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sFinalPlacementDesc not in (select FinalPlacementTypeName from dbo.FinalPlacementType where programid = dbo.fn_ProgramID())
order by sFinalPlacementDesc