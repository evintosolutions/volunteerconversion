

DROP TABLE [CometSource].[dbo].[ConversionPersonToParty]
GO

/****** Object:  Table [CometSource].[dbo].[ConversionPersonToParty]    Script Date: 03/12/2012 22:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CometSource].[dbo].[ConversionPersonToParty](
	[PersonID] [int] NOT NULL,
	[PartyID] [int] NOT NULL
) ON [PRIMARY]

GO



    declare @PersonID int;
    declare @PartyID int;
    declare @PartyType int;
    declare @LastName varchar(255);
    declare @FirstName varchar(255);
    declare @MiddleName varchar(255);
    declare @AKAName varchar(255);
    declare @SSN char(11);
    declare @Birthdate datetime;
    declare @HomeEmail varchar(255);
    declare @WorkEmail varchar(255);
    declare @Gender int;
    declare @Citizen bit;
    declare @EthnicityID int;
    declare @MaritalStatusID int;
    declare @DriversLicense bit;
    declare @EmploymentStatusID int;
    declare @SubstanceAbuse bit;
    declare @PriorPlacements tinyint;
    declare @PriorPlacementsMonths smallint;
    declare @Note varchar(5000);
    declare @Deceased bit;
    declare @Active bit;
    declare @AddDate smalldatetime;
    declare @AddUserID int;
    declare @ModDate smalldatetime;
    declare @ModUserID int;
    declare @Address varchar(255);
    declare @Address2 varchar(255);
    declare @City varchar(255);
    declare @State varchar(255);
    declare @Zip varchar(255);
    declare @CountyID int;
    declare @HomePhone varchar(255);
    declare @CellPhone varchar(255);
    declare @Fax varchar(255);
    declare @Photo varchar(255);
    declare @WorkPhone varchar(15);
    declare @PermissionToCall bit;
    declare @HeightFt smallint;
    declare @HeightIn smallint;
    declare @WeightLbs int;
	declare @ProgramID int;
declare @Hispanic int;

	select @ProgramID = dbo.fn_ProgramID()
 
     declare crs cursor for
   
      select distinct
              p.nPersonID PersonID,
              pt.PartyType PartyType,
              sLName LastName,
              coalesce(sFName,'') FirstName,
              sMName MiddleName,
              coalesce(pc.sAKA,pf.sAKA) AKAName,
              coalesce(pc.sSSN,pv.sSSN) SSN,
              case when isdate(coalesce(convert(datetime,pc.dDOB),convert(datetime,pv.dDOB),convert(datetime,pf.dDOB))) = 1 then coalesce(pc.dDOB,pv.dDOB,pf.dDOB) end Birthdate,
             case
when sHomeEmail like '%@%' and sHomeEmail like '%,com%' then replace(sHomeEmail,',com','.com')
when sHomeEmail like '%@%' and sHomeEmail like '%.%' then sHomeEmail
else null
end as HomeEmail,
case
when sEmpEmail like '%@%' and sEmpEmail like '%,com%' then replace(sEmpEmail,',com','.com')
when sEmpEmail like '%@%' and sEmpEmail like '%.%' then sEmpEmail
else null
end as WorkEmail,
              case left(ltrim(rtrim(coalesce(pc.sGender,pv.sGender,pf.sGender))),1) when 'M' then 0 when 'F' then 1 else 1 end Gender,
              case when pv.nPersonID is not null then 1 else 0 end Citizen,
              eth.EthnicityID EthnicityID,
              null MaritalStatusID,
              0 DriversLicense,
              null EmploymentStatusID,
              coalesce(pf.fSubstanceAbuse,0) SubstanceAbuse,
              convert(tinyint,pc.nNumPlacements) PriorPlacements,
              convert(smallint,pc.sPlacementLength) PriorPlacementsMonths,
              pf.sNote,
              --case when isdate(pc.dDateDeceased) = 1 then 'Deceased Date: ' + convert(varchar,pc.dDateDeceased) else '' end
              --+ case when len(pc.Deceased) > 0 then 'Deceased: ' + convert(varchar,pc.Deceased) else '' end
              --+ case when isdate(pc.dFatherInfoRelease) = 1 then 'Father Info Release Date: ' + convert(varchar,pc.dFatherInfoRelease) else '' end
              --+ case when isdate(pc.dMotherInfoRelease) = 1 then 'Mother Info Release Date: ' + convert(varchar,pc.dMotherInfoRelease) else '' end 
              --+ case when pc.fSafePerm is not null then 'Safe Perm: ' + convert(varchar,pc.fSafePerm) else '' end 
                --[Note],
              0, --case when pc.Deceased = 'Deceased' or pf.fDeceased = 1 then 1 else 0 end Deceased,
              1, --case when p.bInactive = 0 then 1 else 0 end Active,
              getdate() AddDate,
              -1 AddUserID,
              getdate() ModDate,
              -1 ModUserID,
              sHomeAddr1 Address,
              sHomeAddr2 Address2,
              sHomeCity City,
              upper(ltrim(rtrim(sHomeState))) State,
              case when len(nHomeZip) > 0 then nHomeZip end Zip,
              c.CountyID CountyID,
              sHomePhone HomePhone,
              sMobile CellPhone,
	      isnull(p.sworkfax,p.shomefax),
              null Photo,
              p.sWorkPhone + coalesce(' ' + p.sWorkExt,'') WorkPhone,
              0 PermissionToCall,
              null HeightFt,
              null HeightIn,
              null WeightLbs,
              dbo.fn_ProgramID() ProgramID
,case 
	 when (eth.EthnicityDesc like '%Hispanic%' or eth.EthnicityDesc like '%Latino%' ) and eth.EthnicityDesc not like '%Not%' then 1
	 else 0
	 end
      from [CometSource].[dbo].tblPerson p
      join [dbo].PartyType pt on pt.PartyTypeNameShort = p.sCategory and pt.programID = 1
      left join [CometSource].[dbo].tblPersonChild pc on p.nPersonID = pc.nPersonID and p.sCategory = 'Child'
      left join [CometSource].[dbo].tblPersonFamily pf on p.nPersonID = pf.nPersonID and p.sCategory = 'Family'
      left join [CometSource].[dbo].tblPersonVolunteer pv on p.nPersonID = pv.nPersonID and p.sCategory = 'Vol'
      left join [dbo].Ethnicity eth on eth.EthnicityDesc = coalesce(pc.sEthnicity,pf.sEthnicity,pv.sEthnicity) and eth.programID = dbo.fn_ProgramID()
      --left join [dbo].MaritalStatus mar on mar.MaritalStatusDesc = ????
      left join [dbo].County c on c.CountyName = p.sHomeCounty and c.programID = dbo.fn_ProgramID()
      left join [dbo].EmploymentStatus es on es.EmploymentStatusDesc = p.sEmpStatus and es.programID = dbo.fn_ProgramID()
      where p.sCategory <> 'Judge'
      order by p.nPersonID
      ;





      open crs;
      fetch crs
       into       @PersonID,
                  @PartyType,
                  @LastName,
                  @FirstName,
                  @MiddleName,
                  @AKAName,
                  @SSN ,
                  @Birthdate ,
                  @HomeEmail ,
                  @WorkEmail ,
                  @Gender ,
                  @Citizen ,
                  @EthnicityID ,
                  @MaritalStatusID ,
                  @DriversLicense ,
                  @EmploymentStatusID ,
                  @SubstanceAbuse ,
                  @PriorPlacements ,
                  @PriorPlacementsMonths ,
                  @Note ,
                  @Deceased ,
                  @Active ,
                  @AddDate ,
                  @AddUserID ,
                  @ModDate ,
                  @ModUserID ,
                  @Address ,
                  @Address2 ,
                  @City ,
                  @State ,
                  @Zip ,
                  @CountyID ,
                  @HomePhone ,
                  @CellPhone ,
		  @Fax,
                  @Photo ,
                  @WorkPhone ,
                  @PermissionToCall ,
                  @HeightFt ,
                  @HeightIn ,
                  @WeightLbs ,
				  @ProgramID,
                 @Hispanic
     

     while @@FETCH_STATUS = 0
     begin
        insert into [dbo].Party
                ( PartyType ,
                  LastName ,
                  FirstName ,
                  MiddleName ,
                  AKAName ,
                  SSN ,
                  Birthdate ,
                  HomeEmail ,
                  WorkEmail ,
                  Gender ,
                  Citizen ,
                  EthnicityID ,
                  MaritalStatusID ,
                  DriversLicense ,
                  EmploymentStatusID ,
                  SubstanceAbuse ,
                  PriorPlacements ,
                  PriorPlacementsMonths ,
                  Note ,
                  Deceased ,
                  Active ,
                  AddDate ,
                  AddUserID ,
                  ModDate ,
                  ModUserID ,
                  Address ,
                  Address2 ,
                  City ,
                  State ,
                  Zip ,
                  CountyID ,
                  HomePhone ,
                  CellPhone ,
		  FaxNumber,
                  Photo ,
                  WorkPhone ,
                  PermissionToCall ,
                  HeightFt ,
                  HeightIn ,
                  WeightLbs,
                  programID
		  ,CareerTypeID
,Hispanic
                )
        values  ( @PartyType ,
                  @LastName ,
                  @FirstName ,
                  @MiddleName ,
                  @AKAName ,
                  @SSN ,
                  @Birthdate ,
                  @HomeEmail ,
                  @WorkEmail ,
                  @Gender ,
                  @Citizen ,
                  @EthnicityID ,
                  @MaritalStatusID ,
                  @DriversLicense ,
                  @EmploymentStatusID ,
                  @SubstanceAbuse ,
                  @PriorPlacements ,
                  @PriorPlacementsMonths ,
                  @Note ,
                  @Deceased ,
                  @Active ,
                  @AddDate ,
                  @AddUserID ,
                  @ModDate ,
                  @ModUserID ,
                  @Address ,
                  @Address2 ,
                  @City ,
                  @State ,
                  @Zip ,
                  @CountyID ,
                  @HomePhone ,
                  @CellPhone ,
		  @Fax,
                  @Photo ,
                  @WorkPhone ,
                  @PermissionToCall ,
                  @HeightFt ,
                  @HeightIn ,
                  @WeightLbs ,
                  @ProgramID
		  ,null 
,@Hispanic
                )
             
        set @PartyID = SCOPE_IDENTITY();
        
        insert into [CometSource].[dbo].[ConversionPersonToParty]
                ( [PersonID], [PartyID] )
        values  ( @PersonID ,@PartyID );

      
        fetch crs
         into     @PersonID ,
                  @PartyType ,
                  @LastName ,
                  @FirstName ,
                  @MiddleName ,
                  @AKAName ,
                  @SSN ,
                  @Birthdate ,
                  @HomeEmail ,
                  @WorkEmail ,
                  @Gender ,
                  @Citizen ,
                  @EthnicityID ,
                  @MaritalStatusID ,
                  @DriversLicense ,
                  @EmploymentStatusID ,
                  @SubstanceAbuse ,
                  @PriorPlacements ,
                  @PriorPlacementsMonths ,
                  @Note ,
                  @Deceased ,
                  @Active ,
                  @AddDate ,
                  @AddUserID ,
                  @ModDate ,
                  @ModUserID ,
                  @Address ,
                  @Address2 ,
                  @City ,
                  @State ,
                  @Zip ,
                  @CountyID ,
                  @HomePhone ,
                  @CellPhone ,
		   @Fax,
                  @Photo ,
                  @WorkPhone ,
                  @PermissionToCall ,
                  @HeightFt ,
                  @HeightIn ,
                  @WeightLbs ,
                  @ProgramID,
		@Hispanic
;
     end
     
     close crs;
     deallocate crs;

go

