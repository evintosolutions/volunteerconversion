
---------------------------------------------------------------------------------
--CaseAssociatedParty
--Dependencies: Party, CaseInfo
---------------------------------------------------------------------------------

 
insert into [dbo].CaseAssociatedParty
        ( AssociatedPartyID ,
          CaseInfoID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          ProgramID
        )
select distinct
ap.AssociatedPartyID
,cgc.CaseID
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [dbo].AssociatedParty ap
join [dbo].Party p on ap.PartyID = p.PartyID and ap.programID = p.programID
join [CometSource].[dbo].ConversionPersonToParty cpp on p.PartyID = cpp.PartyID
join [CometSource].[dbo].tblPersonRelationship pr on pr.nRelationshipID = cpp.PersonID
join [CometSource].[dbo].tblPersonChild pc on pc.nPersonID = pr.nPersonID
join [CometSource].[dbo].tblGroup g on g.nGroupID = pc.nGroupID
join [CometSource].[dbo].ConversionGroupToCase cgc on pc.nGroupID = cgc.nGroupID
where ap.programID = dbo.fn_ProgramID()

        
/*
select distinct
c.PartyID
,ipt.InterestedPartyTypeID
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonRelationship pr
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pr.nRelationshipID
join [dbo].Party p on p.PartyID = c.PartyID
left join [CometSource].[dbo].tblRelationshipChildIntPerson ip on ip.nPersonRelationshipID = pr.nPersonRelationshipID
left join [dbo].InterestedPartyType ipt on ipt.InterestedPartyTypeDesc = ip.sIntPsnRole and ipt.programID = dbo.fn_ProgramID()
where p.PartyType in(select PartyType from [dbo].PartyType where PartyTypeNameShort in('Atty','Case','Int'))
*/

/*
select distinct
ap.AssociatedPartyID
,cg.CaseID
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonInterestedPerson pip
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pip.nPersonID
join [CometSource].[dbo].tblPersonRelationship pr on pr.nRelationshipID = pip.nPersonID
join [CometSource].[dbo].tblRelationshipChildIntPerson ip on ip.nPersonRelationshipID = pr.nPersonRelationshipID
join [CometSource].[dbo].ConversionGroupToCase cg on cg.nGroupID = pip.nGroupID
join [dbo].AssociatedParty ap on ap.PartyID = cp.PartyID
join [dbo].InterestedPartyType ipt on ipt.InterestedPartyTypeDesc = ip.sIntPsnRole and ipt.programID = dbo.fn_ProgramID()
*/