


--ALTER 
--FUNCTION dbo.fn_ProgramID()
--RETURNS int
--AS
--BEGIN
--    RETURN 141

--END




update[192-168-0-5].volunteer.dbo.applicanttrainingcourse
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.applicanttrainingcourse A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.CourseName in 
  (select CourseName from [192.168.0.2].volunteer.[dbo].[applicanttrainingcourse] where programID  = 110 and Active= 1)
  


insert into dbo.ApplicantTrainingCourse
([CourseName]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      coursename
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[ApplicantTrainingCourse]
  where programID = 110 and CourseName not in 
  (select CourseName from[192-168-0-5].volunteer.dbo.applicanttrainingcourse where programID  = dbo.fn_ProgramID()
  and active = 1
  )
  and Active= 1



update[192-168-0-5].volunteer.dbo.abusetype
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.Abusetype A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.abusetypedesc in 
  (select abusetypedesc from [192.168.0.2].volunteer.[dbo].[Abusetype] where programID  = 110 and Active= 1)
  


insert into dbo.AbuseType
([AbuseTypeDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [AbuseTypeDesc]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[AbuseType]
  where programID = 110 and AbuseTypeDesc not in 
  (select AbuseTypeDesc from[192-168-0-5].volunteer.dbo.AbuseType where programID  = dbo.fn_ProgramID()
  and active = 1
  )
  and Active= 1



update[192-168-0-5].volunteer.dbo.AllegationOutcome
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.AllegationOutcome A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.Description in 
  (select Description from [192.168.0.2].volunteer.[dbo].[AllegationOutcome] where programID  = 110 and Active= 1)
  


insert into dbo.AllegationOutcome
([Description]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [Description]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[AllegationOutcome]
  where programID = 110 and Active= 1 and Description not in 
  (select Description from[192-168-0-5].volunteer.dbo.AllegationOutcome where programID  = dbo.fn_ProgramID()
  and active = 1
  )




update[192-168-0-5].volunteer.dbo.casereleasereason
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.casereleasereason A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.CaseReleaseReasonDesc in 
  (select CaseReleaseReasonDesc from [192.168.0.2].volunteer.[dbo].[CASERELEASEREASON] where programID  = 110 and Active= 1)
  


insert into dbo.CaseReleaseReason
([CaseReleaseReasonDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CaseReleaseReasonDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CASERELEASEREASON]
  where programID = 110 and Active= 1 and CASERELEASEREASONDesc not in 
  (select CASERELEASEREASONDesc from[192-168-0-5].volunteer.dbo.CASERELEASEREASON where programID  = dbo.fn_ProgramID())



update[192-168-0-5].volunteer.dbo.CHILDREFERRAL
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CHILDREFERRAL A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.ChildReferralDesc in 
  (select ChildReferralDesc from [192.168.0.2].volunteer.[dbo].[CHILDREFERRAL] where programID  = 110 and Active= 1)
  


insert into dbo.ChildReferral
([ChildReferralDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CHILDREFERRALDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CHILDREFERRAL]
  where programID = 110 and Active= 1 and ChildReferralDesc not in 
  (select ChildReferralDesc from[192-168-0-5].volunteer.dbo.CHILDREFERRAL where programID  = dbo.fn_ProgramID())
  




update[192-168-0-5].volunteer.dbo.CHILDREFERRALREASON
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CHILDREFERRALREASON A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.ChildReferralDesc in 
  (select ChildReferralDesc from [192.168.0.2].volunteer.[dbo].[CHILDREFERRALREASON] where programID  = 110 and Active= 1)
  


insert into dbo.ChildReferralREASON
([ChildReferralDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CHILDREFERRALDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CHILDREFERRALREASON]
  where programID = 110 and Active= 1 and ChildReferralDesc not in 
  (select ChildReferralDesc from[192-168-0-5].volunteer.dbo.CHILDREFERRALREASON where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.CONCERNS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CONCERNS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.ConcernsDesc in 
  (select ConcernsDesc from [192.168.0.2].volunteer.[dbo].[CONCERNS] where programID  = 110 and Active= 1)
  


insert into dbo.Concerns
([ConcernsDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CONCERNSDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CONCERNS]
  where programID = 110 and Active= 1 and ConcernsDesc not in 
  (select ConcernsDesc from[192-168-0-5].volunteer.dbo.CONCERNS where programID  = dbo.fn_ProgramID())
  




update[192-168-0-5].volunteer.dbo.DOCUMENTTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.DOCUMENTTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.DocTypeDesc in 
  (select DocTypeDesc from [192.168.0.2].volunteer.[dbo].[DOCUMENTTYPE] where programID  = 110 and Active= 1
  AND AreaID = A.AreaID
  )
  


insert into dbo.DocumentType
([DocTypeDesc]
,AreaID
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [DOCTYPEDESC]
	  ,AreaID
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[DOCUMENTTYPE] D
  where programID = 110 and Active= 1 and DocTypeDesc not in 
  (select DocTypeDesc from[192-168-0-5].volunteer.dbo.DOCUMENTTYPE where programID  = dbo.fn_ProgramID()
  AND AreaID = D.AreaID
  )



update[192-168-0-5].volunteer.dbo.ELIGIBLESTATUS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.ELIGIBLESTATUS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.EligibleStatusDesc in 
  (select EligibleStatusDesc from [192.168.0.2].volunteer.[dbo].[ELIGIBLESTATUS] where programID  = 110 and Active= 1)
  


insert into dbo.EligibleStatus
([EligibleStatusDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [ELIGIBLESTATUSDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[ELIGIBLESTATUS]
  where programID = 110 and Active= 1 and ELIGIBLESTATUSDesc not in 
  (select ELIGIBLESTATUSDesc from[192-168-0-5].volunteer.dbo.ELIGIBLESTATUS where programID  = dbo.fn_ProgramID())
  


  



update[192-168-0-5].volunteer.dbo.INTERESTEDPARTYTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.INTERESTEDPARTYTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.INTERESTEDPARTYTYPEDesc in 
  (select INTERESTEDPARTYTYPEDesc from [192.168.0.2].volunteer.[dbo].INTERESTEDPARTYTYPE where programID  = 110 and Active= 1)
  


insert into dbo.INTERESTEDPARTYTYPE
([INTERESTEDPARTYTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [INTERESTEDPARTYTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].INTERESTEDPARTYTYPE
  where programID = 110 and Active= 1 and INTERESTEDPARTYTYPEDesc not in 
  (select INTERESTEDPARTYTYPEDesc from[192-168-0-5].volunteer.dbo.INTERESTEDPARTYTYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.PETITIONCASETYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.PETITIONCASETYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.PETITIONCASETYPEDesc in 
  (select PETITIONCASETYPEDesc from [192.168.0.2].volunteer.[dbo].PETITIONCASETYPE where programID  = 110 and Active= 1)
  


insert into dbo.PETITIONCASETYPE
([PETITIONCASETYPEDesc]
      ,[Active]
     ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [PETITIONCASETYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PETITIONCASETYPE
  where programID = 110 and Active= 1 and PETITIONCASETYPEDesc not in 
  (select PETITIONCASETYPEDesc from[192-168-0-5].volunteer.dbo.PETITIONCASETYPE where programID  = dbo.fn_ProgramID())
  

update[192-168-0-5].volunteer.dbo.VOLUNTEERACTIVITYTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERACTIVITYTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.VOLUNTEERACTIVITYTYPEDesc in 
  (select VOLUNTEERACTIVITYTYPEDesc from [192.168.0.2].volunteer.[dbo].VOLUNTEERACTIVITYTYPE where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERACTIVITYTYPE
([VOLUNTEERACTIVITYTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERACTIVITYTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERACTIVITYTYPE
  where programID = 110 and Active= 1 and VOLUNTEERACTIVITYTYPEDesc not in 
  (select VOLUNTEERACTIVITYTYPEDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERACTIVITYTYPE where programID  = dbo.fn_ProgramID())
  



update[192-168-0-5].volunteer.dbo.VOLUNTEERCONTACTTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERCONTACTTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.VOLUNTEERCONTACTTYPEDesc in 
  (select VOLUNTEERCONTACTTYPEDesc from [192.168.0.2].volunteer.[dbo].VOLUNTEERCONTACTTYPE where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERCONTACTTYPE
([VOLUNTEERCONTACTTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERCONTACTTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERCONTACTTYPE
  where programID = 110 and Active= 1 and VOLUNTEERCONTACTTYPEDesc not in 
  (select VOLUNTEERCONTACTTYPEDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERCONTACTTYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.DEGREE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.DEGREE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.DegreeDesc in 
  (select DegreeDesc from [192.168.0.2].volunteer.[dbo].DEGREE where programID  = 110 and Active= 1)
  


insert into dbo.DEGREE
(DegreeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      DEGREEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].DEGREE
  where programID = 110 and Active= 1 and DegreeDesc not in 
  (select DegreeDesc from[192-168-0-5].volunteer.dbo.DEGREE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.DISABILITYTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.DISABILITYTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.DISABILITYTYPEDesc in 
  (select DISABILITYTYPEDesc from [192.168.0.2].volunteer.[dbo].DISABILITYTYPE where programID  = 110 and Active= 1)
  


insert into dbo.DISABILITYTYPE
(DISABILITYTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      DISABILITYTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].DISABILITYTYPE
  where programID = 110 and Active= 1 and DISABILITYTYPEDesc not in 
  (select DISABILITYTYPEDesc from[192-168-0-5].volunteer.dbo.DISABILITYTYPE where programID  = dbo.fn_ProgramID())
  

update[192-168-0-5].volunteer.dbo.EDUCATIONTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.EDUCATIONTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.EDUCATIONTYPEDesc in 
  (select EDUCATIONTYPEDesc from [192.168.0.2].volunteer.[dbo].EDUCATIONTYPE where programID  = 110 and Active= 1)
  


insert into dbo.EDUCATIONTYPE
(EDUCATIONTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      EDUCATIONTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].EDUCATIONTYPE
  where programID = 110 and Active= 1 and EDUCATIONTYPEDesc not in 
  (select EDUCATIONTYPEDesc from[192-168-0-5].volunteer.dbo.EDUCATIONTYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.EMPLOYMENTSTATUS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.EMPLOYMENTSTATUS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.EMPLOYMENTSTATUSDesc in 
  (select EMPLOYMENTSTATUSDesc from [192.168.0.2].volunteer.[dbo].EMPLOYMENTSTATUS where programID  = 110 and Active= 1)
  


insert into dbo.EMPLOYMENTSTATUS
(EMPLOYMENTSTATUSDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      EMPLOYMENTSTATUSDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].EMPLOYMENTSTATUS
  where programID = 110 and Active= 1 and EMPLOYMENTSTATUSDesc not in 
  (select EMPLOYMENTSTATUSDesc from[192-168-0-5].volunteer.dbo.EMPLOYMENTSTATUS where programID  = dbo.fn_ProgramID())
  



update[192-168-0-5].volunteer.dbo.GEOLOCATION
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.GEOLOCATION A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.GEOLOCATIONDesc in 
  (select GEOLOCATIONDesc from [192.168.0.2].volunteer.[dbo].GEOLOCATION where programID  = 110 and Active= 1)
  


insert into dbo.GEOLOCATION
(GEOLOCATIONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      GEOLOCATIONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].GEOLOCATION
  where programID = 110 and Active= 1 and GEOLOCATIONDesc not in 
  (select GEOLOCATIONDesc from[192-168-0-5].volunteer.dbo.GEOLOCATION where programID  = dbo.fn_ProgramID())



update[192-168-0-5].volunteer.dbo.LANGUAGETYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.LANGUAGETYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.LANGUAGETYPEDesc in 
  (select LANGUAGETYPEDesc from [192.168.0.2].volunteer.[dbo].LANGUAGETYPE where programID  = 110 and Active= 1)
  


insert into dbo.LANGUAGETYPE
(LANGUAGETYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      LANGUAGETYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].LANGUAGETYPE
  where programID = 110 and Active= 1 and LANGUAGETYPEDesc not in 
  (select LANGUAGETYPEDesc from[192-168-0-5].volunteer.dbo.LANGUAGETYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.MARITALSTATUS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.MARITALSTATUS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.MARITALSTATUSDesc in 
  (select MARITALSTATUSDesc from [192.168.0.2].volunteer.[dbo].MARITALSTATUS where programID  = 110 and Active= 1)
  


insert into dbo.MARITALSTATUS
(MARITALSTATUSDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      MARITALSTATUSDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].MARITALSTATUS
  where programID = 110 and Active= 1 and MARITALSTATUSDesc not in 
  (select MARITALSTATUSDesc from[192-168-0-5].volunteer.dbo.MARITALSTATUS where programID  = dbo.fn_ProgramID())
  



  
  


update[192-168-0-5].volunteer.dbo.RELATIONSHIPTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.RELATIONSHIPTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.RelationshipTypeName in 
  (select RELATIONSHIPTYPENAME from [192.168.0.2].volunteer.[dbo].RELATIONSHIPTYPE where programID  = 110 and Active= 1)
  


insert into dbo.RELATIONSHIPTYPE
(RelationshipTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      RELATIONSHIPTYPENAME
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].RELATIONSHIPTYPE
  where programID = 110 and Active= 1 and RelationshipTypeName not in 
  (select RelationshipTypeName from[192-168-0-5].volunteer.dbo.RELATIONSHIPTYPE where programID  = dbo.fn_ProgramID())



update[192-168-0-5].volunteer.dbo.SCHOOLTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.SCHOOLTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.SchoolTypeDesc in 
  (select SchoolTypeDesc from [192.168.0.2].volunteer.[dbo].SCHOOLTYPE where programID  = 110 and Active= 1)
  


insert into dbo.SCHOOLTYPE
(SchoolTypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      SCHOOLTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].SCHOOLTYPE
  where programID = 110 and Active= 1 and SchoolTypeDesc not in 
  (select SchoolTypeDesc from[192-168-0-5].volunteer.dbo.SCHOOLTYPE where programID  = dbo.fn_ProgramID())


update[192-168-0-5].volunteer.dbo.HEARINGOUTCOMETYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.HEARINGOUTCOMETYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.HEARINGOUTCOMETYPEDesc in 
  (select HEARINGOUTCOMETYPEDesc from [192.168.0.2].volunteer.[dbo].HEARINGOUTCOMETYPE where programID  = 110 and Active= 1)
  


insert into dbo.HEARINGOUTCOMETYPE
(HEARINGOUTCOMETYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HEARINGOUTCOMETYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HEARINGOUTCOMETYPE
  where programID = 110 and Active= 1 and HEARINGOUTCOMETYPEDesc not in 
  (select HEARINGOUTCOMETYPEDesc from[192-168-0-5].volunteer.dbo.HEARINGOUTCOMETYPE where programID  = dbo.fn_ProgramID())

  



update[192-168-0-5].volunteer.dbo.HEARINGSTATUS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.HEARINGSTATUS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.HEARINGSTATUSDesc in 
  (select HEARINGSTATUSDesc from [192.168.0.2].volunteer.[dbo].HEARINGSTATUS where programID  = 110 and Active= 1)
  


insert into dbo.HEARINGSTATUS
(HEARINGSTATUSDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HEARINGSTATUSDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HEARINGSTATUS
  where programID = 110 and Active= 1 and HEARINGSTATUSDesc not in 
  (select HEARINGSTATUSDesc from[192-168-0-5].volunteer.dbo.HEARINGSTATUS where programID  = dbo.fn_ProgramID())
  

update[192-168-0-5].volunteer.dbo.HEARINGTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.HEARINGTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.HEARINGTYPEDesc in 
  (select HEARINGTYPEDesc from [192.168.0.2].volunteer.[dbo].HEARINGTYPE where programID  = 110 and Active= 1)
  


insert into dbo.HEARINGTYPE
(HEARINGTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HEARINGTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HEARINGTYPE
  where programID = 110 and Active= 1 and HEARINGTYPEDesc not in 
  (select HEARINGTYPEDesc from[192-168-0-5].volunteer.dbo.HEARINGTYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.ORDERSERVICETYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.ORDERSERVICETYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.ORDERSERVICETYPEDesc in 
  (select ORDERSERVICETYPEDesc from [192.168.0.2].volunteer.[dbo].ORDERSERVICETYPE where programID  = 110 and Active= 1)
  


insert into dbo.ORDERSERVICETYPE
(ORDERSERVICETYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      ORDERSERVICETYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].ORDERSERVICETYPE
  where programID = 110 and Active= 1 and ORDERSERVICETYPEDesc not in 
  (select ORDERSERVICETYPEDesc from[192-168-0-5].volunteer.dbo.ORDERSERVICETYPE where programID  = dbo.fn_ProgramID())
  




update[192-168-0-5].volunteer.dbo.PLACEMENTREASON
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.PLACEMENTREASON A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.PLACEMENTREASONDesc in 
  (select PLACEMENTREASONDesc from [192.168.0.2].volunteer.[dbo].PLACEMENTREASON where programID  = 110 and Active= 1)
  


insert into dbo.PLACEMENTREASON
(PLACEMENTREASONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      PLACEMENTREASONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PLACEMENTREASON
  where programID = 110 and Active= 1 and PLACEMENTREASONDesc not in 
  (select PLACEMENTREASONDesc from[192-168-0-5].volunteer.dbo.PLACEMENTREASON where programID  = dbo.fn_ProgramID())
  




update[192-168-0-5].volunteer.dbo.PLACEMENTTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.PLACEMENTTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.PLACEMENTTYPEDesc in 
  (select PLACEMENTTYPEDesc from [192.168.0.2].volunteer.[dbo].PLACEMENTTYPE where programID  = 110 and Active= 1)
  


insert into dbo.PLACEMENTTYPE
(PLACEMENTTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      PLACEMENTTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PLACEMENTTYPE
  where programID = 110 and Active= 1 and PLACEMENTTYPEDesc not in 
  (select PLACEMENTTYPEDesc from[192-168-0-5].volunteer.dbo.PLACEMENTTYPE where programID  = dbo.fn_ProgramID())
  




update[192-168-0-5].volunteer.dbo.VISITATIONTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VISITATIONTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.VISITATIONTYPEDesc in 
  (select VISITATIONTYPEDesc from [192.168.0.2].volunteer.[dbo].VISITATIONTYPE where programID  = 110 and Active= 1)
  


insert into dbo.VISITATIONTYPE
(VISITATIONTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      VISITATIONTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VISITATIONTYPE
  where programID = 110 and Active= 1 and VISITATIONTYPEDesc not in 
  (select VISITATIONTYPEDesc from[192-168-0-5].volunteer.dbo.VISITATIONTYPE where programID  = dbo.fn_ProgramID())
  

update[192-168-0-5].volunteer.dbo.TODOTASKCATEGORY
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.TODOTASKCATEGORY A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.TODOTASKCATEGORYDesc in 
  (select TODOTASKCATEGORYDesc from [192.168.0.2].volunteer.[dbo].TODOTASKCATEGORY where programID  = 110 and Active= 1)
  


insert into dbo.TODOTASKCATEGORY
(TODOTASKCATEGORYDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKCATEGORYDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKCATEGORY
  where programID = 110 and Active= 1 and TODOTASKCATEGORYDesc not in 
  (select TODOTASKCATEGORYDesc from[192-168-0-5].volunteer.dbo.TODOTASKCATEGORY where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.TODOTASKSERVICETYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.TODOTASKSERVICETYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.TODOTASKSERVICETYPEDesc in 
  (select TODOTASKSERVICETYPEDesc from [192.168.0.2].volunteer.[dbo].TODOTASKSERVICETYPE where programID  = 110 and Active= 1)
  


insert into dbo.ToDoTaskServiceType
(  ToDoTaskServiceTypeDesc
	  ,ToDoTaskCategoryID
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      ToDoTaskServiceTypeDesc
	  ,TC2.ToDoTaskCategoryID
      ,TS.[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKSERVICETYPE TS
  JOIN [192.168.0.2].volunteer.[dbo].TODOTASKCATEGORY TC ON TC.ToDoTaskCategoryID = TS.ToDoTaskCategoryID AND TC.ProgramID = 110
  JOIN[192-168-0-5].volunteer.[dbo].TODOTASKCATEGORY TC2 ON TC2.ToDoTaskCategoryDesc = TC.ToDoTaskCategoryDesc AND TC2.ProgramID = DBO.fn_ProgramID()
  where TS.ProgramID = 110 and TS.Active= 1 and TODOTASKSERVICETYPEDesc not in 
  (select TODOTASKSERVICETYPEDesc from[192-168-0-5].volunteer.dbo.TODOTASKSERVICETYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.TODOTASKPriorityType
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.TODOTASKPriorityType A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.TODOTASKPriorityTypeDesc in 
  (select TODOTASKPriorityTypeDesc from [192.168.0.2].volunteer.[dbo].TODOTASKPriorityType where programID  = 110 and Active= 1)
  


insert into dbo.TODOTASKPriorityType
(TODOTASKPriorityTypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKPriorityTypeDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKPriorityType
  where programID = 110 and Active= 1 and TODOTASKPriorityTypeDesc not in 
  (select TODOTASKPriorityTypeDesc from[192-168-0-5].volunteer.dbo.TODOTASKPriorityType where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.TODOTASKSTATUSREASON
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.TODOTASKSTATUSREASON A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.TODOTASKSTATUSREASONDesc in 
  (select TODOTASKSTATUSREASONDesc from [192.168.0.2].volunteer.[dbo].TODOTASKSTATUSREASON where programID  = 110 and Active= 1)
  


insert into dbo.TODOTASKSTATUSREASON
(TODOTASKSTATUSREASONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKSTATUSREASONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKSTATUSREASON
  where programID = 110 and Active= 1 and TODOTASKSTATUSREASONDesc not in 
  (select TODOTASKSTATUSREASONDesc from[192-168-0-5].volunteer.dbo.TODOTASKSTATUSREASON where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.TODOTASKSTATUSTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.TODOTASKSTATUSTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.TODOTASKSTATUSTYPEDesc in 
  (select TODOTASKSTATUSTYPEDesc from [192.168.0.2].volunteer.[dbo].TODOTASKSTATUSTYPE where programID  = 110 and Active= 1)
  


insert into dbo.TODOTASKSTATUSTYPE
(TODOTASKSTATUSTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKSTATUSTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKSTATUSTYPE
  where programID = 110 and Active= 1 and TODOTASKSTATUSTYPEDesc not in 
  (select TODOTASKSTATUSTYPEDesc from[192-168-0-5].volunteer.dbo.TODOTASKSTATUSTYPE where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.ethnicity
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.ethnicity A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.ethnicityDesc in 
  (select ethnicityDesc from [192.168.0.2].volunteer.[dbo].ethnicity where programID  = 110 and Active= 1)
  


insert into dbo.ethnicity
(ethnicityDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      ethnicityDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].ethnicity
  where programID = 110 and Active= 1 and ethnicityDesc not in 
  (select ethnicityDesc from[192-168-0-5].volunteer.dbo.ethnicity where programID  = dbo.fn_ProgramID())
  
     

update[192-168-0-5].volunteer.dbo.TRAININGFORMAT
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.TRAININGFORMAT A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.TRAININGFORMATDesc in 
  (select TRAININGFORMATDesc from [192.168.0.2].volunteer.[dbo].TRAININGFORMAT where programID  = 110 and Active= 1)
  


insert into dbo.TRAININGFORMAT
(TRAININGFORMATDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TRAININGFORMATDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TRAININGFORMAT
  where programID = 110 and Active= 1 and TRAININGFORMATDesc not in 
  (select TRAININGFORMATDesc from[192-168-0-5].volunteer.dbo.TRAININGFORMAT where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.CAREERTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CAREERTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.CAREERTYPEDesc in 
  (select CAREERTYPEDesc from [192.168.0.2].volunteer.[dbo].CAREERTYPE where programID  = 110 and Active= 1)
  


insert into dbo.CAREERTYPE
(CAREERTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      CAREERTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CAREERTYPE
  where programID = 110 and Active= 1 and CAREERTYPEDesc not in 
  (select CAREERTYPEDesc from[192-168-0-5].volunteer.dbo.CAREERTYPE where programID  = dbo.fn_ProgramID())
  

update[192-168-0-5].volunteer.dbo.CERTIFICATION
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CERTIFICATION A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.CERTIFICATIONName in 
  (select CERTIFICATIONNAME from [192.168.0.2].volunteer.[dbo].CERTIFICATION where programID  = 110 and Active= 1)
  


insert into dbo.CERTIFICATION
(CERTIFICATIONName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      CERTIFICATIONNAME
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CERTIFICATION
  where programID = 110 and Active= 1 and CERTIFICATIONName not in 
  (select CERTIFICATIONName from[192-168-0-5].volunteer.dbo.CERTIFICATION where programID  = dbo.fn_ProgramID())


update[192-168-0-5].volunteer.dbo.VOLUNTEERDISCHARGEREASON
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERDISCHARGEREASON A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.DISCHARGEREASONDesc in 
  (select DISCHARGEREASONDesc from [192.168.0.2].volunteer.[dbo].VOLUNTEERDISCHARGEREASON where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERDISCHARGEREASON
(DISCHARGEREASONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      DISCHARGEREASONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERDISCHARGEREASON
  where programID = 110 and Active= 1 and DISCHARGEREASONDesc not in 
  (select DISCHARGEREASONDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERDISCHARGEREASON where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.NOTESUBJECT
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.NOTESUBJECT A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.NOTESUBJECTDesc in 
  (select NOTESUBJECTDesc from [192.168.0.2].volunteer.[dbo].[NOTESUBJECT] where programID  = 110 and Active= 1)
  


insert into dbo.NOTESUBJECT
([NOTESUBJECTDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [NOTESUBJECTDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[NOTESUBJECT]
  where programID = 110 and Active= 1 and NOTESUBJECTDesc not in 
  (select NOTESUBJECTDesc from[192-168-0-5].volunteer.dbo.NOTESUBJECT where programID  = dbo.fn_ProgramID())
  



update[192-168-0-5].volunteer.dbo.volunteerreferral
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.volunteerreferral A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.volunteerreferralDesc in 
  (select volunteerreferralDesc from [192.168.0.2].volunteer.[dbo].[volunteerreferral] where programID  = 110 and Active= 1)
  


insert into dbo.volunteerreferral
([volunteerreferralDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [volunteerreferralDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[volunteerreferral]
  where programID = 110 and Active= 1 and volunteerreferralDesc not in 
  (select volunteerreferralDesc from[192-168-0-5].volunteer.dbo.volunteerreferral where programID  = dbo.fn_ProgramID())
  




update[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.VOLUNTEERSTATUSName in 
  (select VOLUNTEERSTATUSNAME from [192.168.0.2].volunteer.[dbo].VOLUNTEERSTATUS where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERSTATUS
(VOLUNTEERSTATUSName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      VOLUNTEERSTATUSNAME
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERSTATUS
  where programID = 110 and Active= 1 and VOLUNTEERSTATUSName not in 
  (select VOLUNTEERSTATUSName from[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUS where programID  = dbo.fn_ProgramID())



update[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUSREASON
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUSREASON A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.VOLUNTEERSTATUSREASONDesc in 
  (select VOLUNTEERSTATUSREASONDesc from [192.168.0.2].volunteer.[dbo].[VOLUNTEERSTATUSREASON] where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERSTATUSREASON
([VOLUNTEERSTATUSREASONDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERSTATUSREASONDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[VOLUNTEERSTATUSREASON]
  where programID = 110 and Active= 1 and VOLUNTEERSTATUSREASONDesc not in 
  (select VOLUNTEERSTATUSREASONDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUSREASON where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.VOLUNTEERTYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERTYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.VOLUNTEERTYPEDesc in 
  (select VOLUNTEERTYPEDesc from [192.168.0.2].volunteer.[dbo].[VOLUNTEERTYPE] where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERTYPE
([VOLUNTEERTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[VOLUNTEERTYPE]
  where programID = 110 and Active= 1 and VOLUNTEERTYPEDesc not in 
  (select VOLUNTEERTYPEDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERTYPE where programID  = dbo.fn_ProgramID())
  
  

update[192-168-0-5].volunteer.dbo.INTERVIEWOUTCOMETYPE
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.INTERVIEWOUTCOMETYPE A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.OutcomeTypeName in 
  (select OutcomeTypeName from [192.168.0.2].volunteer.[dbo].INTERVIEWOUTCOMETYPE where programID  = 110 and Active= 1)
  


insert into dbo.INTERVIEWOUTCOMETYPE
(OutcomeTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      OutcomeTypeName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].INTERVIEWOUTCOMETYPE
  where programID = 110 and Active= 1 and OutcomeTypeName not in 
  (select OutcomeTypeName from[192-168-0-5].volunteer.dbo.INTERVIEWOUTCOMETYPE where programID  = dbo.fn_ProgramID())
  

  
update[192-168-0-5].volunteer.dbo.REFERENCESTATUS
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.REFERENCESTATUS A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.REFERENCESTATUSDesc in 
  (select REFERENCESTATUSDesc from [192.168.0.2].volunteer.[dbo].[REFERENCESTATUS] where programID  = 110 and Active= 1)
  


insert into dbo.REFERENCESTATUS
([REFERENCESTATUSDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [REFERENCESTATUSDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[REFERENCESTATUS]
  where programID = 110 and Active= 1 and REFERENCESTATUSDesc not in 
  (select REFERENCESTATUSDesc from[192-168-0-5].volunteer.dbo.REFERENCESTATUS where programID  = dbo.fn_ProgramID())
  


update[192-168-0-5].volunteer.dbo.VOLUNTEERREJECTEDREASON
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.VOLUNTEERREJECTEDREASON A
  where A.programID = dbo.fn_programid() and  A.active = 0 and A.REJECTEDREASONDesc in 
  (select REJECTEDREASONDesc from [192.168.0.2].volunteer.[dbo].VOLUNTEERREJECTEDREASON where programID  = 110 and Active= 1)
  


insert into dbo.VOLUNTEERREJECTEDREASON
(RejectedReasonDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [RejectedReasonDesc]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERREJECTEDREASON
  where programID = 110 and Active= 1 and RejectedReasonDesc not in 
  (select RejectedReasonDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERREJECTEDREASON where programID  = dbo.fn_ProgramID())
  



  update[192-168-0-5].volunteer.dbo.CaseChildStatus
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CaseChildStatus A
  where A.programID = dbo.fn_programid() and  A.active = 0 and Statusname in 
  (select Statusname from [192.168.0.2].volunteer.[dbo].CaseChildStatus where programID  = 110 and Active= 1)


insert into dbo.CaseChildStatus
(Statusname
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [Statusname]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CaseChildStatus
  where programID = 110 and Active= 1 and StatusName not in 
  (select StatusName from[192-168-0-5].volunteer.dbo.CaseChildStatus where programID  = dbo.fn_ProgramID())



  update[192-168-0-5].volunteer.dbo.CaseClosureReason
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.CaseClosureReason A
  where A.programID = dbo.fn_programid() and  A.active = 0 and CaseClosureReasonDesc in 
  (select CaseClosureReasonDesc from [192.168.0.2].volunteer.[dbo].CaseClosureReason where programID  = 110 and Active= 1)


insert into dbo.CaseClosureReason
(CaseClosureReasonDEsc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      CaseClosureReasonDesc
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CaseClosureReason
  where programID = 110 and Active= 1 and CaseClosureReasonDesc not in 
  (select CaseClosureReasonDesc from[192-168-0-5].volunteer.dbo.CaseClosureReason where programID  = dbo.fn_ProgramID())




    update[192-168-0-5].volunteer.dbo.HearingLocation
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.HearingLocation A
  where A.programID = dbo.fn_programid() and  A.active = 0 and HearingLocationName in 
  (select HearingLocationName from [192.168.0.2].volunteer.[dbo].HearingLocation where programID  = 110 and Active= 1)


insert into dbo.HearingLocation
(HearingLocationName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HearingLocationName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HearingLocation
  where programID = 110 and Active= 1 and HearingLocationName not in 
  (select HearingLocationName from[192-168-0-5].volunteer.dbo.HearingLocation where programID  = dbo.fn_ProgramID())





    update[192-168-0-5].volunteer.dbo.FinalPlacementType
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.FinalPlacementType A
  where A.programID = dbo.fn_programid() and  A.active = 0 and FinalPlacementTypeName in 
  (select FinalPlacementTypeName from [192.168.0.2].volunteer.[dbo].FinalPlacementType where programID  = 110 and Active= 1)


insert into dbo.FinalPlacementType
(FinalPlacementTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      FinalPlacementTypeName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].FinalPlacementType
  where programID = 110 and Active= 1 and FinalPlacementTypeName not in 
  (select FinalPlacementTypeName from[192-168-0-5].volunteer.dbo.FinalPlacementType where programID  = dbo.fn_ProgramID())



 
      update[192-168-0-5].volunteer.dbo.legalstatustype
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.legalstatustype A
  where A.programID = dbo.fn_programid() and  A.active = 0 and legalstatustypeDesc in 
  (select legalstatustypeDesc from [192.168.0.2].volunteer.[dbo].legalstatustype where programID  = 110 and Active= 1)


insert into dbo.legalstatustype
(legalstatustypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      legalstatustypeDesc
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].legalstatustype
  where programID = 110 and Active= 1 and legalstatustypeDesc not in 
  (select legalstatustypeDesc from[192-168-0-5].volunteer.dbo.legalstatustype where programID  = dbo.fn_ProgramID())


  

update[192-168-0-5].volunteer.dbo.NonCaseActivityTypes
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.NonCaseActivityTypes A
  where A.programID = dbo.fn_programid() and  A.active = 0 and NonCaseActivityTypeName in 
  (select NonCaseActivityTypeName from [192.168.0.2].volunteer.[dbo].NonCaseActivityTypes where programID  = 110 and Active= 1)


insert into dbo.NonCaseActivityTypes
(NonCaseActivityTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[programID]
)
SELECT
      NonCaseActivityTypeName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].NonCaseActivityTypes
  where programID = 110 and Active= 1 and NonCaseActivityTypeName not in 
  (select NonCaseActivityTypeName from[192-168-0-5].volunteer.dbo.NonCaseActivityTypes where programID  = dbo.fn_ProgramID())


update[192-168-0-5].volunteer.dbo.PermanentPlanType
set Active = 1  
  FROM[192-168-0-5].volunteer.dbo.PermanentPlanType A
  where A.programID = dbo.fn_programid() and  A.active = 0 and PermanentPlanTypeDesc in 
  (select PermanentPlanTypeDesc from [192.168.0.2].volunteer.[dbo].PermanentPlanType where programID  = 110 and Active= 1)


insert into dbo.PermanentPlanType
(PermanentPlanTypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
	  ,ModUserID
      ,[programID]
)
SELECT
      PermanentPlanTypeDesc
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
	  ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PermanentPlanType
  where programID = 110 and Active= 1 and PermanentPlanTypeDesc not in 
  (select PermanentPlanTypeDesc from[192-168-0-5].volunteer.dbo.PermanentPlanType where programID  = dbo.fn_ProgramID())






INSERT INTO [dbo].[ApplicationProcessConfig]
           ([AppIntro]
           ,[DisplayFamily]
           ,[DisplayPriorEmployment]
           ,[VehicleIntro]
           ,[DisplayVehicleQuestions]
           ,[BackgroundIntro]
           ,[CriminalIntro]
           ,[ReferenceIntro]
           ,[DisplayReferenceCt]
           ,[AgreementIntro]
           ,[ReleaseIntro]
           
           ,[ReferenceSurveySTatusId]
           ,[CertifiedStatusID]
           ,[Displaybirthplace]
           ,[Displayheightandweight]
           ,[DisplaySSN]
           ,[DisplaySpouse]
           ,[DisplayChildren]
           ,[FamilyInto]
           ,[ExperienceInto]
           ,[VolunteerExperienceInto]
           ,[Thankyoutext]
           ,[DisplayEmploymentDetail]
           ,[RequireEmailAddress]
           ,[RequireAddress]
           ,[EmploymentIntro]
		   ,[AddDate]
           ,[AddUserID]
           ,[ModDate]
           ,[ModUserID]
		   ,programID)
SELECT
      [AppIntro]
           ,[DisplayFamily]
           ,[DisplayPriorEmployment]
           ,[VehicleIntro]
           ,[DisplayVehicleQuestions]
           ,[BackgroundIntro]
           ,[CriminalIntro]
           ,[ReferenceIntro]
           ,[DisplayReferenceCt]
           ,[AgreementIntro]
           ,[ReleaseIntro]
           ,[ReferenceSurveySTatusId]
           ,[CertifiedStatusID]
           ,[Displaybirthplace]
           ,[Displayheightandweight]
           ,[DisplaySSN]
           ,[DisplaySpouse]
           ,[DisplayChildren]
           ,[FamilyInto]
           ,[ExperienceInto]
           ,[VolunteerExperienceInto]
           ,[Thankyoutext]
           ,[DisplayEmploymentDetail]
           ,[RequireEmailAddress]
           ,[RequireAddress]
           ,[EmploymentIntro]
      ,GETDATE()
      ,-1
      ,GETDATE()
	  ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].applicationprocessconfig
  where programID = 110 and appintro not in 
  (select appintro from[192-168-0-5].volunteer.dbo.applicationprocessconfig where programID  = dbo.fn_ProgramID())





INSERT INTO [dbo].[ReferenceSurveyConfiguration]
           ([EmailTextBody]
           ,[EmailTextFooter]
           ,[EmailFromAddress]
           ,[programID])
SELECT
      [EmailTextBody]
           ,[EmailTextFooter]
           ,[EmailFromAddress]
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[ReferenceSurveyConfiguration] A
  where programID = 110 and EmailFromAddress not in 
  (select EmailFromAddress from[192-168-0-5].volunteer.dbo.[ReferenceSurveyConfiguration] where programID  = dbo.fn_ProgramID()
   and EmailTextBody = A.EmailTextBody
  )






--update [192-168-0-5].volunteer.dbo.BACKGROUNDCHECKTYPE
--set Active = 1  
--  FROM [192-168-0-5].volunteer.dbo.BACKGROUNDCHECKTYPE A
--  where A.programID = dbo.fn_programid() and  A.active = 0 and A.CheckTypeName in 
--  (select CheckTypeName from [192.168.0.2].volunteer.[dbo].BACKGROUNDCHECKTYPE where programID  = 110 and Active= 1)
  


--insert into dbo.BACKGROUNDCHECKTYPE
--(CheckTypeName
--      ,[Active]
--      ,[AddDate]
--      ,[AddUserID]
--      ,[ModDate]
--      ,[ModUserID]
--      ,[programID]
--)
--SELECT
--      CHECKTYPENAME
--      ,[Active]
--      ,GETDATE()
--      ,-1
--      ,GETDATE()
--      ,-1
--      ,dbo.fn_ProgramID()
--  FROM [192.168.0.2].volunteer.[dbo].BACKGROUNDCHECKTYPE
--  where programID = 110 and Active= 1 and CheckTypeName not in 
--  (select CheckTypeName from [192-168-0-5].volunteer.dbo.BACKGROUNDCHECKTYPE where programID  = dbo.fn_ProgramID())
  





--update [192-168-0-5].volunteer.dbo.COUNTY
--set Active = 1  
--  FROM [192-168-0-5].volunteer.dbo.COUNTY A
--  where A.programID = dbo.fn_programid() and  A.active = 0 and A.CountyName in 
--  (select COUNTYNAME from [192.168.0.2].volunteer.[dbo].COUNTY where programID  = 110 and Active= 1)
  


--insert into dbo.COUNTY
--(CountyName
--      ,[Active]
--      ,[AddDate]
--      ,[AddUserID]
--      ,[ModDate]
--      ,[ModUserID]
--      ,[programID]
--)
--SELECT
--      COUNTYNAME
--      ,[Active]
--      ,GETDATE()
--      ,-1
--      ,GETDATE()
--      ,-1
--      ,dbo.fn_ProgramID()
--  FROM [192.168.0.2].volunteer.[dbo].COUNTY
--  where programID = 110 and Active= 1 and CountyName not in 
--  (select CountyName from [192-168-0-5].volunteer.dbo.COUNTY where programID  = dbo.fn_ProgramID())




--update [192-168-0-5].volunteer.dbo.Jurisdiction
--set Active = 1  
--  FROM [192-168-0-5].volunteer.dbo.Jurisdiction A
--  where A.programID = dbo.fn_programid() and  A.active = 0 and A.JurisdictionName in 
--  (select JurisdictionNAME from [192.168.0.2].volunteer.[dbo].Jurisdiction where programID  = 110 and Active= 1)
  


--INSERT INTO [dbo].[Jurisdiction]
--           ([JurisdictionName]
--           ,[Active]
--           ,[AddDate]
--           ,[AddUserID]
--           ,[ModDate]
--           ,[ModUserID]
--           ,[programID])
--SELECT
--      JurisdictionNAME
--      ,[Active]
--      ,GETDATE()
--      ,-1
--      ,GETDATE()
--      ,-1
--      ,dbo.fn_ProgramID()
--  FROM [192.168.0.2].volunteer.[dbo].Jurisdiction
--  where programID = 110 and Active= 1 and JurisdictionName not in 
--  (select JurisdictionName from [192-168-0-5].volunteer.dbo.Jurisdiction where programID  = dbo.fn_ProgramID())



--update [192-168-0-5].volunteer.dbo.[TrainingTopic]
--set Active = 1  
--  FROM [192-168-0-5].volunteer.dbo.[TrainingTopic] A
--  where A.programID = dbo.fn_programid() and  A.active = 0 and A.[TrainingTopicDesc] in 
--  (select [TrainingTopicDesc] from [192.168.0.2].volunteer.[dbo].[TrainingTopic] where programID  = 110 and Active= 1)
  

--INSERT INTO [dbo].[TrainingTopic]
--           ([TrainingTopicDesc]
--           ,[Active]
--           ,[AddDate]
--           ,[AddUserID]
--           ,[ModDate]
--           ,[ModUserID]
--           ,[programId])
--SELECT
--      [TrainingTopicDesc]
--      ,[Active]
--      ,GETDATE()
--      ,-1
--      ,GETDATE()
--      ,-1
--      ,dbo.fn_ProgramID()
--  FROM [192.168.0.2].volunteer.[dbo].[TrainingTopic]
--  where programID = 110 and Active= 1 and [TrainingTopicDesc] not in 
--  (select [TrainingTopicDesc] from [192-168-0-5].volunteer.dbo.[TrainingTopic] where programID  = dbo.fn_ProgramID())
