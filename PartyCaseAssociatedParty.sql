

--------------------------------------------------------------------------------
--PartyCaseAssociatedParty
--Dependencies: CaseAssociatedParty, Party, CaseReleaseReason
---------------------------------------------------------------------------------

 
insert into [dbo].PartyCaseAssociatedParty
        ( CaseAssociatedPartyID ,
          PartyID ,
          AssignedDate ,
          ReleasedDate ,
          CaseReleaseReasonID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          ProgramID ,
          PartyTypeValue
        )
select 
cap.CaseAssociatedPartyID
,ch.PartyID
,case when isdate(convert(datetime,r.dDateAssigned))=1 then r.dDateAssigned end
,case when isdate(convert(datetime,r.dDateRemoved))=1 then r.dDateRemoved end
,crr.CaseReleaseReasonID
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
,p.PartyType
from [CometSource].[dbo].tblPersonRelationship r
join [CometSource].[dbo].ConversionPersonToParty ch on ch.PersonID = r.nPersonID
join [CometSource].[dbo].ConversionPersonToParty cpp on cpp.PersonID = r.nRelationshipID
join [dbo].Party p on p.PartyID = ch.PartyID and p.programID = dbo.fn_ProgramID()
join [dbo].CaseChild cc on cc.ChildPartyID = ch.PartyID and cc.programID = dbo.fn_ProgramID()
join [dbo].AssociatedParty ap on ap.PartyID = cpp.PartyID and ap.programID = dbo.fn_ProgramID()
join [dbo].CaseAssociatedParty cap on cap.AssociatedPartyID = ap.AssociatedPartyID and cap.CaseInfoID = cc.CaseID and cap.ProgramID = dbo.fn_ProgramID()
left join [dbo].CaseReleaseReason crr on crr.CaseReleaseReasonDesc = r.sRemovalReason and crr.programID = dbo.fn_ProgramID()
