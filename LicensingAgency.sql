------------------------------------------------------------------------------
--LicensingAgency
--Dependencies: 
---------------------------------------------------------------------------------
use [Volunteer];
go
 ----------------------------------------------

with t as (
  select distinct
    sPlacementFacilityAgency
  from [CometSource].[dbo]._tblPlacementFacilityAgency f
  where len(sPlacementFacilityAgency) > 0

union

  select distinct
    sPlacementFacilityAgency
  from [CometSource].[dbo].tblFacility f
  where len(sPlacementFacilityAgency) > 0
 
)

insert into [Volunteer].[dbo].LicensingAgency
        ( LicensingAgencyName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
--sFacilityName
sPlacementFacilityAgency
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where len(sPlacementFacilityAgency) > 0
order by sPlacementFacilityAgency
