
--AbuseType


with t as (
  select distinct sAllegationCatDesc
  from [CometSource].[dbo].[_tblAllegationCat]
  where len(sAllegationCatDesc)>0

  union
  
  select distinct sAllegationCat sAllegationCatDesc
  from [CometSource].[DBO].tblAllegation
  where len(sAllegationCat)>0

)

insert into [dbo].AbuseType
        ( AbuseTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sAllegationCatDesc, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from t
where sAllegationCatDesc not in (select abusetypedesc from dbo.AbuseType where programID = dbo.fn_ProgramID())
order by sAllegationCatDesc





insert into [dbo].PrefAbuse
        ( PartyID ,
          AbuseTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
  c.PartyID
  ,t.AbuseTypeID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer p
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
join [dbo].AbuseType t on t.AbuseTypeDesc = p.sAbusePref and t.programID = dbo.fn_ProgramID()

------------------------------