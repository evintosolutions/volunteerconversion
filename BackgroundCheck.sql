

insert into [dbo].VolunteerCertifyCheckLog
        ( CertifyCheckType ,
          PartyID ,
          BackgroundCheckDate ,
          AutoCheckDate ,
          InsuranceCheckDate ,
          BackgroundCheckComplete ,
          AutoCheckComplete ,
          InsuranceCheckComplete ,
          Notes ,
          programId
        )
select 
  'B'
  , c.PartyID
  ,max
  (case when isdate(convert(datetime,cc.dDateChecked)) = 1 then cc.dDateChecked end)
  ,null
  ,max
  (case when isdate(convert(datetime,au.dDateChecked)) = 1 then au.dDateChecked end)
  ,max
  (case when case when isdate(convert(datetime,cc.dDateChecked)) = 1 then cc.dDateChecked end is not null then 1 else 0 end)
  ,0
  ,max
  (case when au.fCurrent = 1 then 1 else 0 end)
--Set to '' instead of Null so can use Not Like in Notes Update below
  ,''
  --max(
-- (case when isdate(convert(datetime,cc.dDateChecked))) = 1 then (convert(varchar,month(cc.dDateChecked)) + '/' + convert(varchar,day(cc.dDateChecked)) + '/' + convert(varchar,year(cc.dDateChecked)) + '   ') end +
  --case when len(coalesce(cc.sAgency,'')) + len(coalesce(cc.sResults,'')) > 0 then 'Results = ' + coalesce(cc.sAgency,'') + coalesce(' ' + cc.sResults,'') end)
  --)
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer v
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = v.nPersonID
left join [CometSource].[dbo].tblPersonVolunteerAuto au on au.nPersonID = v.nPersonID
left join [CometSource].[dbo].tblPersonVolunteerCriminalCheck cc on cc.nPersonID = v.nPersonID
where cc.sAgency is not null or cc.dDateChecked is not null or au.dDateChecked is not null or au.fCurrent is not null
group by c.PartyID

 


--Add the Notes--

 declare @PartyID int
 declare @PersonID int
 declare @Notes varchar(max)

 declare crsNotes cursor for 
 select  
cl.PartyID
, 
cc.nPersonID,
  (case when isdate(convert(datetime,cc.dDateChecked)) = 1 then (convert(varchar,month(cc.dDateChecked)) + '/' + convert(varchar,day(cc.dDateChecked)) + '/' + convert(varchar,year(cc.dDateChecked)) + '   ') end +
  case when len(coalesce(cc.sAgency,'')) + len(coalesce(cc.sResults,'')) > 0 then 'Results = ' + coalesce(cc.sAgency,'') + coalesce(' ' + cc.sResults,'') end)
from VolunteerCertifyCheckLog cl
join [CometSource].[dbo].ConversionPersonToParty c on cl.PartyID = C.partyID
join [CometSource].[dbo].tblPersonVolunteer v on c.PersonID = v.nPersonID
--left join [CometSource].[dbo].tblPersonVolunteerAuto au on au.nPersonID = v.nPersonID
join [CometSource].[dbo].tblPersonVolunteerCriminalCheck cc on cc.nPersonID = v.nPersonID
where 
(cc.sResults is not null
and (cl.notes not like '%Results = ' + coalesce(cc.sAgency,'') + coalesce(' ' + cc.sResults,'')+'%') )
order by cc.ddatechecked desc
;

open crsNotes;
fetch crsNotes into 
@PartyID,
@PersonID,
@Notes

while @@fetch_status = 0
begin

	update cl
	set cl.Notes = cl.Notes + char(10)+
	@Notes
	from VolunteerCertifyCheckLog cl
----join [CometSource].[dbo].ConversionPersonToParty c on cl.PartyID = C.partyID
----join [CometSource].[dbo].tblPersonVolunteer v on c.PersonID = v.nPersonID
----join [CometSource].[dbo].tblPersonVolunteerCriminalCheck cc on cc.nPersonID = v.nPersonID
	where 
	cl.PartyID = @PartyID 
----and c.PersonID = @PersonID
and (cl.notes not like '%'+@Notes+'%')

fetch crsNotes into 
@PartyID,
@PersonID,
@Notes


end
   
close crsNotes;
deallocate crsNotes;

go








update v
set LastBackgroundCheckDate = case when v.LastBackgroundCheckDate is null or v.LastBackgroundCheckDate < cl.BackgroundCheckDate then cl.BackgroundCheckDate end
,AutoInsurConfirmDate = case when v.AutoInsurConfirmDate is null or v.AutoInsurConfirmDate < cl.InsuranceCheckDate then cl.InsuranceCheckDate end
from VolunteerCertifyCheckLog cl
join Volunteer v on cl.PartyID = v.PartyID and cl.programId = v.programID and cl.PartyID = dbo.fn_ProgramID();








with t as (
	select distinct sAgencyDesc from [CometSource].[dbo]._tblCriminalCheckAgency
where sAgencyDesc not in (select checktypename from dbo.BackgroundCheckType where programID = dbo.fn_ProgramID())
	union

select distinct sAgency sAgencyDesc from [CometSource].[dbo].tblPersonVolunteerCriminalCheck
where sAgency not in (select checktypename from dbo.BackgroundCheckType where programID = dbo.fn_ProgramID())
)

insert into [dbo].BackgroundCheckType
        ( CheckTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sAgencyDesc,1,getdate(),-1,getdate(),-1,dbo.fn_ProgramID()
from t
where len(sAgencyDesc) > 0
order by sAgencyDesc;




------------------------------


with t as (
	select distinct 
	 max(case when isdate(convert(datetime,pv.dDateChecked)) = 1 and pv.sAgency = t.CheckTypeName then pv.dDateChecked end) CheckDate
	,max(case when isdate(convert(datetime,pv.dDateChecked)) = 1 and pv.sAgency = t.CheckTypeName then pv.dDateChecked end) CompleteDate
	,t.CheckTypeID
	,cl.CheckLogID
	,rank() over(partition by t.CheckTypeID, cl.CheckLogID order by case when isdate(convert(datetime,pv.dDateChecked)) = 1 and pv.sAgency = t.CheckTypeName then pv.dDateChecked end desc) RankNum
	from [CometSource].[dbo].tblPersonVolunteerCriminalCheck pv
	join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pv.nPersonID
	cross join [dbo].BackgroundCheckType t 
	join [dbo].VolunteerCertifyCheckLog cl on cl.PartyID = c.PartyID and cl.programId = dbo.fn_ProgramID()
	where t.programID = dbo.fn_ProgramID()
	group by
	t.CheckTypeID
	,pv.dDateChecked
	,pv.sAgency
	,t.CheckTypeName
	,cl.CheckLogID
)

insert into [dbo].BackgroundCheck
        ( CheckPerformed ,
          Passed ,
          CheckDate ,
          CompleteDate ,
          Notes ,
          CheckTypeID ,
          CheckLogID ,
          programID
        )
select distinct 
0
,1
,t.CheckDate
,t.CompleteDate
,null
,t.CheckTypeID
,t.CheckLogID
,dbo.fn_ProgramID()
from t
where t.RankNum = 1

------------------------------


