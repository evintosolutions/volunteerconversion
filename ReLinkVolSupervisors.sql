

  declare @OldPartyID int;
  declare @NewPartyID int;
  declare @ProgramID int;


  set @ProgramID = dbo.fn_ProgramID()


   declare crs cursor for
    select OldPartyID, NewPartyID
    from [CometSource].[dbo].VolReLink
where oldpartyid is not null

	;
    
  
  open crs;

  fetch crs
   into
     @OldPartyID
	 ,@NewPartyID
   
   while @@FETCH_STATUS = 0
   begin
      update dbo.VolunteerCertifyCheckLog
	  set PartyID = @NewPartyID
	  where PartyID = @OldPartyID
	  and PROGRAMid = @ProgramID


	  update dbo.TrainingActivity
	  set PartyID = @NewPartyID
	  where PartyID = @OldPartyID
	  and PROGRAMid = @ProgramID


	  update dbo.ApplicantTraining
	  set PartyID = @NewPartyID
	  where PartyID = @OldPartyID
	  and PROGRAMid = @ProgramID

           update dbo.NonCaseActivity
	  set VolunteerPartyID = @NewPartyID
	  where VolunteerPartyID = @OldPartyID
	  and PROGRAMid = @ProgramID



	Update dbo.Party
	set Active = 0
	where PartyID = @OldPartyID
	and ProgramID = @ProgramID


        Update dbo.Party
	set FirstName= FirstName + ' - Dup '
	where PartyID = @OldPartyID
	and ProgramID = @ProgramID


	Update dbo.volunteer
	set IsVolunteer = 1
	from dbo.volunteer v
join dbo.Party p on p.PartyID = v.PartyID
	where v.PartyID = @NewPartyID
	and p.PartyType <> 10


update pn
set pn.WorkEmail = po.HomeEmail
from dbo.Party pn
join dbo.Party po on  pn.programID = @ProgramID
and po.programID = @ProgramID and po.PartyID = @OldPartyID and pn.PartyID = @NewPartyID
and po.HomeEmail like '%gal.fl.gov%'

update po
set po.HomeEmail = null
from dbo.Party pn
join dbo.Party po on  pn.programID = @ProgramID
and po.programID = @ProgramID and po.PartyID = @OldPartyID and pn.PartyID = @NewPartyID
and po.HomeEmail like '%gal.fl.gov%'


update pn
set pn.WorkEmail = po.WorkEmail
from dbo.Party pn
join dbo.Party po on  pn.programID = @ProgramID
and po.programID = @ProgramID and po.PartyID = @OldPartyID and pn.PartyID = @NewPartyID
and po.WorkEmail like '%gal.fl.gov%'

update po
set po.WorkEmail = null
from dbo.Party pn
join dbo.Party po on  pn.programID = @ProgramID
and po.programID = @ProgramID and po.PartyID = @OldPartyID and pn.PartyID = @NewPartyID
and po.WorkEmail like '%gal.fl.gov%'


      fetch crs
       into
          @OldPartyID
		 ,@NewPartyID
   end
   
   close crs;
   deallocate crs;

