insert into dbo.Eligible
(PartyID, EligibleDate, EligibleEndDate, EligibleStatusID,AddDate, AddUserID, ModDate,  ModUserID, ProgramID
)
select distinct 
	 cp.PartyID
,pc.[dAssignCasa]
  ,pc.[dProgramClosed]
,  es.EligibleStatusID 
  , getdate()
  , -1
  , getdate()
  , -1
  , dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].tblPerson p on pc.nPersonID = p.nPersonID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pc.nPersonID
join [CometSource].[dbo].tblChildEligbleStatus ces on ces.nPersonID = pc.nPersonID
join [dbo].EligibleStatus es on es.EligibleStatusDesc = ces.sEligbleStatus and es.programID = dbo.fn_ProgramID()
