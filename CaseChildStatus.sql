--CaseChildStatus


with t as (
  select distinct
    pc.sIntitialStatus
  from [CometSource].dbo.tblPersonChild pc
  where len(pc.sIntitialStatus) > 0

  union
  
  select distinct
    sHearingTypeDesc sIntitialStatus
  from [CometSource].[dbo].[_tblHearingType]
  where len(sHearingTypeDesc) > 0
)

insert into [dbo].CaseChildStatus
 (StatusName
 , Active
 , AddDate
 , AddUserID
 , ModDate
 , ModUserID
 , programID)
select distinct
  sIntitialStatus
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from t
where sIntitialStatus not in (select StatusName from dbo.CaseChildStatus where programID = dbo.fn_ProgramID())
;
