

 
/*
insert into [dbo].Trainer
        ( TrainerLastName ,
          TrainerFirstName ,
          TrainerTitle ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
coalesce(pt.sLName,p.sLName)
,coalesce(pt.sFName,p.sFName)
,coalesce(pt.sTitle,p.sTitle)
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteerTraining pvt
join [CometSource].[dbo].tblPerson p on pvt.nPersonID = p.nPersonID
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pvt.nPersonID
left join [CometSource].[dbo].[_tblTrainer] pt on pt.nTrainerID = pvt.nTrainerID
where dbo.fn_ProgramID() in(3) or pt.nTrainerID is not null
*/
insert into [dbo].Trainer
        ( TrainerLastName ,
          TrainerFirstName ,
          TrainerTitle ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
coalesce(pt.sLName,'None')
,coalesce(pt.sFName,'None')
,pt.sTitle
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblTrainer] pt 
--where len(pt.sLName) > 0 or len(pt.sFName) > 0
where coalesce(pt.sLName,'None') not in (select TrainerLastName from dbo.Trainer where programID = dbo.fn_ProgramID()
and Trainerfirstname = coalesce(pt.sFName,'None')
and TrainerTitle = pt.sTitle
)




------------------------------

--Issue 725 addition

insert into [dbo].Trainer
        ( TrainerLastName ,
          TrainerFirstName ,
          TrainerTitle ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
'Trainer'
,'Unknown'
,null
--12/13/12 Changing Active to 0 per issue 725
--,1
,0
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
where 'Trainer' not in (select TrainerLastName from dbo.Trainer where programID = dbo.fn_ProgramID()
and TrainerFirstName = 'Unknown')
;
 


---------------------



 
with t as (
select distinct 
sTrainingTypeDesc
from [CometSource].[dbo].[_tblTrainingType] 
where len(sTrainingTypeDesc) > 0

union
select distinct
sTrainingType sTrainingTypeDesc
from [CometSource].[dbo].tblPersonVolunteerTraining
where len(sTrainingType) > 0
)

insert into [dbo].TrainingFormat
        ( TrainingFormatDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sTrainingTypeDesc
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
WHERE sTrainingTypeDesc NOT IN (SELECT TrainingFormatDesc from dbo.TrainingFormat where programID = dbo.fn_ProgramID())
order by sTrainingTypeDesc;






insert into [dbo].TrainingFormat
        ( TrainingFormatDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
'Other'
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
WHERE 'Other' NOT IN (SELECT TrainingFormatDesc from dbo.TrainingFormat where programID = dbo.fn_ProgramID())






----------------------


declare @helper table (TrainingTopic varchar(500));
 
with l as
(
    select sTrainingTopic, dDateCompleted, nID, row_number() over(partition by sTrainingTopic order by dDateCompleted desc, nID desc) RankNum
    from [CometSource].[dbo].tblPersonVolunteerTraining pvt
    --Issue 719: Changing from <> 'Initial' to ='Inservice'
	where sType = 'Inservice'
    and sTrainingTopic is not null
    group by sTrainingTopic, dDateCompleted, nID
)


insert into @helper
select distinct 
l.sTrainingTopic
from l
join [CometSource].[dbo].tblPersonVolunteerTraining vt on vt.nID = l.nID
where l.RankNum = 1 

--Issue 719: taking out
--insert into @helper
--select distinct sTrainingTopicDesc
--from [CometSource].[dbo].[_tblTrainingTopic]
--where len(sTrainingTopicDesc) > 0



insert into [dbo].TrainingTopic
        ( TrainingTopicDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct 
h.TrainingTopic
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from @helper h
WHERE h.TrainingTopic NOT IN (SELECT TrainingTopicDesc from dbo.TrainingTopic where programID = dbo.fn_ProgramID())
order by h.TrainingTopic






insert into [dbo].TrainingTopic
        ( TrainingTopicDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct 
'Other'
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
WHERE 'Other' NOT IN (SELECT TrainingTopicDesc from dbo.TrainingTopic where programID = dbo.fn_ProgramID())




------------------------------

--Issue 725: Add Generic Trainer
declare @GenericTrainer int;
select @GenericTrainer = (select TrainerID from dbo.Trainer where TrainerFirstName = 'Unknown'
and TrainerLastName = 'Trainer' and programID = dbo.fn_ProgramID())

declare @OtherTopic int = (select TrainingTopicID from dbo.TrainingTopic where programId = dbo.fn_ProgramID() and TrainingTopicDesc = 'Other') 

declare @OtherFormat int = (select TrainingFormatid from dbo.TrainingFormat where programId = dbo.fn_ProgramID() and TrainingFormatDesc = 'Other')

  
insert into [dbo].TrainingActivity
        ( PartyID ,
          ScheduleDate ,
          CompleteDate ,
          TrainingTypeID ,
          TrainingTopicID ,
          TrainingFormatID ,
          TrainerID ,
          Hours ,
          Mileage ,
          Notes ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          TrainingID ,
          ActivityStatus ,
          programID
        )
select distinct
  c.PartyID
   ,case when isdate(convert(datetime,pvt.dDateScheduled)) = 1 and datediff([day],'1/1/1901',pvt.dDateScheduled) > 0 and datediff([day],pvt.dDateScheduled,'12/31/2050') > 0 then pvt.dDateScheduled 
  when pvt.dDateCompleted is not null then pvt.dDateCompleted
  else '1/1/2010' end
,case when isdate(convert(datetime,pvt.dDateCompleted)) = 1 and datediff([day],'1/1/1901',pvt.dDateCompleted) > 0 and datediff([day],pvt.dDateCompleted,'12/31/2050') > 0 then pvt.dDateCompleted end
  ,'I'
 ,isnull(tt.TrainingTopicID,@OtherTopic)
   ,isnull(tf.TrainingFormatID,@OtherFormat)
  ,isnull(t.TrainerID, @GenericTrainer)
  ,nHours
  ,null
  ,null
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,null
  ,'A'
  ,dbo.fn_ProgramID() 
from [CometSource].[dbo].tblPersonVolunteerTraining pvt
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pvt.nPersonID
left join [dbo].TrainingTopic tt on tt.TrainingTopicDesc = pvt.sTrainingTopic and tt.programId = dbo.fn_ProgramID()
left join [dbo].TrainingFormat tf on tf.TrainingFormatDesc = pvt.sTrainingType and tf.programId = dbo.fn_ProgramID()
join dbo.Volunteer VV on VV.PartyID = c.PartyID
left join [CometSource].[dbo].[_tblTrainer] ttr on ttr.nTrainerID = pvt.nTrainerID
--left join [CometSource].[dbo].tblPerson pt on pt.nPersonID = pvt.nTrainerID
left join [dbo].Trainer t on t.TrainerLastName = ttr.sLName and t.TrainerFirstName = ttr.sFName and 
t.TrainerTitle = ttr.sTitle
and t.programId = dbo.fn_ProgramID()
--Issue 719, changing from <> 'Initial' to = 'InService'
where pvt.sType in ('InService','In Service', 'In-Service') 


