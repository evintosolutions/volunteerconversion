
---------------------------------------------------------------------------------
--HEARING PETITION
--Dependencies: Petition
---------------------------------------------------------------------------------

 
insert into [dbo].HearingPetition
        ( HearingID ,
          PetitionID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
  ch.HearingID
, cp.PetitionID
, getdate()
, -1
, getdate()
, -1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblHearingPetiton hp
join [CometSource].[dbo].tblPetition p on p.nPetitionID = hp.nPetitionID
join [CometSource].[dbo].ConversionHearingToHearing ch on ch.nHearingID = hp.nHearingID
join [CometSource].[dbo].ConversionPetitionToPetition cp on cp.sPetitionNum = p.sPetitionNum
