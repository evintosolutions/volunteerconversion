--CHANGE THE PROGRAMID!!!!!!!!!!!!!!!!!!

DECLARE @ProgramID  NVARCHAR(100) = (select dbo.fn_Programid()); 
DECLARE @tableName	NVARCHAR(100);
DECLARE @rowCnt		INT;
DECLARE @sqlStmt	NVARCHAR(1000);
DECLARE @para		NVARCHAR(100);
DECLARE @tbl TABLE(TableName  NVARCHAR(100),[RowCount]  INT)

DECLARE CUR_TABLE CURSOR FOR
	SELECT TABLE_NAME 
	FROM INFORMATION_SCHEMA.TABLES
	WHERE TABLE_TYPE = 'BASE TABLE' 
	order by table_name

OPEN CUR_TABLE
FETCH NEXT FROM CUR_TABLE 
INTO @tableName

WHILE @@FETCH_STATUS = 0
BEGIN

    
	IF @tablename in (select t.name from sys.columns c	
		join sys.tables t on t.object_id=c.object_id
		where c.name = 'programid') 
		BEGIN
		SET @sqlStmt = 'SET @rowCnt = (SELECT COUNT(*) FROM '+ @tableName + ' 
		where programid = '+@ProgramID+')'
		SET @para = '@rowCnt NVARCHAR(10) OUTPUT'
		END
	IF @tableName in ('Volunteer', 'Attorney','Child')
		BEGIN
		SET @sqlStmt = 'SET @rowCnt = (SELECT COUNT(*) FROM '+ @tableName + ' t
		where exists(select  1 from [Volunteer].[dbo].Party p where p.PartyID = t.PartyID and p.ProgramID = '+@ProgramID+'))'
		SET @para = '@rowCnt NVARCHAR(10) OUTPUT'
		END
--	ELSE
--		BEGIN
--		SET @sqlStmt = 'SET @rowCnt = (SELECT COUNT(*) FROM '+ @tableName + ')'
--		SET @para = '@rowCnt NVARCHAR(10) OUTPUT'
--		END

	EXECUTE SP_EXECUTESQL @sqlStmt,@para, @rowCnt = @rowCnt OUTPUT

	INSERT INTO @tbl VALUES(@tableName,@rowCnt)

	FETCH NEXT FROM CUR_TABLE
	INTO @tableName;
END;

CLOSE CUR_TABLE
DEALLOCATE CUR_TABLE

SELECT * FROM @tbl

