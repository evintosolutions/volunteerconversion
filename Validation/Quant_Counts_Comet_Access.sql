
SELECT MSysObjects.Name, DCount("*", [Name]) As RecordTotal
FROM MsysObjects
WHERE (Left$([Name],1)<>"~") AND (Left$([Name],4) <> "Msys") AND (MSysObjects.Type)=1
ORDER BY MSysObjects.Name;