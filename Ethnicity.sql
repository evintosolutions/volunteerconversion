--Ethnicity

insert into [dbo].Ethnicity
        ( EthnicityDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sEthnicityDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblEthnicity]
where len(sEthnicityDesc) > 0
and sEthnicityDesc not in (select sEthnicityDesc from dbo.Ethnicity where programID = dbo.fn_ProgramID())
order by sEthnicityDesc
