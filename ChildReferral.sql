--ChildReferral

insert into [dbo].ChildReferral
        ( ChildReferralDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sCaseReferralDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblCaseReferral]
where len(sCaseReferralDesc)>0
and sCaseReferralDesc not in (select ChildReferralDesc from dbo.ChildReferral where programID = dbo.fn_ProgramID())
order by sCaseReferralDesc
