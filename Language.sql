

--LanguageType


with t as (
select distinct sLanguageDesc
from [CometSource].[dbo].[_tblLanguage]
where len(sLanguageDesc) > 0

union 

select distinct 
sPrimaryLang sLanguageDesc
from [CometSource].[dbo].tblPersonChild

union 

select distinct 
sPrimaryLang sLanguageDesc
from [CometSource].[dbo].tblPersonVolunteer

union 

select distinct 
sSecondaryLang sLanguageDesc
from [CometSource].[dbo].tblPersonChild

union 

select distinct 
sSecondaryLang sLanguageDesc
from [CometSource].[dbo].tblPersonVolunteer
)
insert into [dbo].LanguageType
        ( LanguageTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sLanguageDesc
, 1
, getdate()
, -1
, getdate()
, -1
,dbo.fn_ProgramID()
from t
where len(sLanguageDesc) > 0
and sLanguageDesc not in (select LanguageTypeDesc from dbo.LanguageType where programID = dbo.fn_ProgramID())
order by sLanguageDesc


 
--Child: Primary language
insert into [dbo].Language
        ( PartyID ,
          LanguageTypeID ,
          PrimaryLanguage ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
  c.PartyID
  ,lt.LanguageTypeID
  ,1
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pc.nPersonID
join [dbo].LanguageType lt on lt.LanguageTypeDesc = sPrimaryLang and lt.programID = dbo.fn_ProgramID()

union

--Child: Secondary language
select distinct
  c.PartyID
  ,lt.LanguageTypeID
  ,0
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pc.nPersonID
join [dbo].LanguageType lt on lt.LanguageTypeDesc = sSecondaryLang and lt.programID = dbo.fn_ProgramID()

union

--Volunteer: Primary language
select distinct
  c.PartyID
  ,lt.LanguageTypeID
  ,1
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer pv
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pv.nPersonID
join [dbo].LanguageType lt on lt.LanguageTypeDesc = sPrimaryLang and lt.programID = dbo.fn_ProgramID()

union

--Volunteer: Secondary language
select distinct
  c.PartyID
  ,lt.LanguageTypeID
  ,0
  ,1
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer pv
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pv.nPersonID
join [dbo].LanguageType lt on lt.LanguageTypeDesc = sSecondaryLang and lt.programID = dbo.fn_ProgramID()
------------------------------