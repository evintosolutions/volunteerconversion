
------------------------------------------------------------------------------
--HEARING FAMILY
--Dependencies: 
---------------------------------------------------------------------------------

 
insert into [dbo].HearingFamily
        ( HearingID ,
          CaseFamilyMemberID ,
          programID
        )
select
  ch.HearingID
, cfm.CaseFamilyMemberID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblHearingFamily hf
join [CometSource].[dbo].ConversionHearingToHearing ch on ch.nHearingID = hf.nHearingID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = hf.nPersonID
join [dbo].CaseFamilyMember cfm on cfm.FamilyPartyID = cp.PartyID and cfm.programID = dbo.fn_ProgramID()
