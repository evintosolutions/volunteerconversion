

insert into [dbo].Employment
        ( PartyID ,
          CurrentEmployer ,
          Company ,
          Address ,
          Address2 ,
          City ,
          State ,
          Zip ,
          EmploymentStatusID ,
          Title ,
          WorkHours ,
          SupervisorLastName ,
          SupervisorFirstName ,
          PhoneNumber ,
          FromDate ,
          ToDate ,
          ReasonForLeaving ,
          Responsibilities ,
          PermissionToContact ,
          BarNumber ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
   c.PartyID
  ,1 --case when p.sCategory = 'Atty' or len(p.sEmployer) > 0 then 1 else 0 end
  ,coalesce(atty.sFirmName,p.sEmployer)
  ,sWorkAddr1
  ,sWorkAddr2
  ,sWorkCity
  ,sWorkState
  ,sWorkZip
  ,es.EmploymentStatusID
  ,sPosition
  ,sWorkHours
  ,sSupervisor
  ,sSupervisor
  ,null
  ,null
  ,null
  ,null
  ,null
  ,0
  ,atty.sBarNum
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPerson p
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID 
--and p.nCategory in (2,3,7,8)
join dbo.Party prt on prt.PartyID = c.PartyID and prt.PartyType <> 5 and prt.PartyType <> 1
left join [dbo].EmploymentStatus es on es.EmploymentStatusDesc = p.sEmpStatus and es.programID = dbo.fn_ProgramID()
left join [CometSource].[dbo].tblPersonAtty atty on atty.nPersonID = p.nPersonID