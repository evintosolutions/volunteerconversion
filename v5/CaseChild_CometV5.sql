---------------------------------------------------------------------------------
--CASE CHILD
--Dependencies: Child, Party, ChildReferral, HearingType
---------------------------------------------------------------------------------
use [Volunteer];
go
 

--declare @CaseChildHelper table (nPersonID int, strHistory varchar(max));
--insert into @CaseChildHelper
--  SELECT 
--       ch1.nPersonID
--     , replace(ltrim(rtrim(STUFF(
--     (SELECT + ', Prior CASA Assign: ' 
--      + convert(varchar,month(ch2.dAssignCASA)) + '/' + convert(varchar,day(ch2.dAssignCASA)) + '/' + convert(varchar,year(ch2.dAssignCASA))
--      + ' to ' + convert(varchar,month(ch2.dProgramClosed)) + '/' + convert(varchar,day(ch2.dProgramClosed)) + '/' + convert(varchar,year(ch2.dProgramClosed))
--      FROM [CometSource].[dbo].tblPersonChildHistory ch2
--      WHERE ch2.nPersonID = ch1.nPersonID
--      ORDER BY ch2.dProgramClosed desc
--      FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
--    FROM [CometSource].[dbo].tblPersonChild ch1
--    where isdate(ch1.dAssignCASA) + isdate(ch1.dProgramClosed) = 2
--  GROUP BY ch1.nPersonID
--  having 
--  len(replace(ltrim(rtrim(STUFF(
--     (SELECT + ', Prior CASA Assign: ' 
--      + convert(varchar,month(ch2.dAssignCASA)) + '/' + convert(varchar,day(ch2.dAssignCASA)) + '/' + convert(varchar,year(ch2.dAssignCASA))
--      + ' to ' + convert(varchar,month(ch2.dProgramClosed)) + '/' + convert(varchar,day(ch2.dProgramClosed)) + '/' + convert(varchar,year(ch2.dProgramClosed))
--      FROM [CometSource].[dbo].tblPersonChildHistory ch2
--      WHERE ch2.nPersonID = ch1.nPersonID
--      ORDER BY ch2.dProgramClosed desc
--      FOR XML PATH('') ),1,1,''))),'&#x0D;','')) > 1;

declare @CaseChildHelper2 table (nPersonID int, sNote varchar(max));
insert into @CaseChildHelper2
  SELECT 
       ch1.nPersonID
     , replace(ltrim(rtrim(STUFF(
     (SELECT + ',' + ch2.sNote 
      FROM [CometSource].[dbo].tblPersonChild ch2
      WHERE ch2.nPersonID = ch1.nPersonID
      ORDER BY ch2.dProgramClosed desc
      FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
    FROM [CometSource].[dbo].tblPersonChild ch1
  GROUP BY ch1.nPersonID
  having 
  len(replace(ltrim(rtrim(STUFF(
     (SELECT + ',' + sNote
      FROM [CometSource].[dbo].tblPersonChild ch2
      WHERE ch2.nPersonID = ch1.nPersonID
      ORDER BY ch2.dProgramClosed desc
      FOR XML PATH('') ),1,1,''))),'&#x0D;','')) > 1      

/*
InitialNotes: overlay versus concatenate
*/

insert into [Volunteer].[dbo].CaseChild
        ( CaseID ,
          ChildPartyID ,
          ChildReferralID ,
          ReferredBy ,
          ResidencyZip ,
          ResidencyCountyID ,
          RemovedFromHome ,
          RemovedDate ,
          RemovedCaseAffiliatedPartyID ,
          MostRecentEffortsDate ,
          InitialCaseStatusID ,
          AssignedToCASADate ,
          InitialNotes ,
          VolunteerRecommendAccepted ,
          CaseworkerRecommendDifferent ,
          EligibleStatusID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          CloseDate ,
          CaseClosureReasonID ,
          SchoolIEP ,
          SchoolRenewalDate ,
          Active ,
          CourtClosureDate ,
          CourtClosureReasonID ,
          ProgramClosureDate ,
          ProgramClosureReasonID ,
          ClosingNotes ,
          programID ,
          FinalPlacementTypeID ,
          SafePermHome,
		  ChildReferralDate,
		  ChildReferralReasonID
        )
select distinct
    cg.CaseID
  , cp.PartyID
  , cr.ChildReferralID
  , null
  , case when len(p.nHomeZip) > 0 then p.nHomeZip end
  , cty.CountyID
  , pc.fRemoved
  , case when isdate(pc.dDateRemoved)=1 then pc.dDateRemoved end
  , null
  , case when isdate(pc.dRecentEffort)=1 then pc.dRecentEffort end
  --Issue 716, changing InitialCaseStatusID to null
  --, ccs.CaseChildStatusID --ht.HearingTypeID
  ,null as InitialCaseStatusID
  , case when isdate(pc.dAssignCASA)=1 then pc.dAssignCASA end as AssignedtoCASADate
  , left(ltrim(rtrim(
    coalesce(h2.sNote + '. ','')
    --+ coalesce(h.strHistory + '. ','')
    --+ case when pc.dPriorityCase=1 or len(pc.PriorityCase) > 1 then 'Priority Case: ' + coalesce(pc.PriorityCase,'') + '. ' else '' end
    + coalesce('Court Closed: ' + convert(varchar,month(pc.dCourtClosed)) + '/' + convert(varchar,day(pc.dCourtClosed)) + '/' + convert(varchar,year(pc.dCourtClosed)) + '. ','')
    --+ coalesce(' Court Closure Reason: ' + pc.sCourtReason + '.','')
    --+ coalesce(' Siblings: ' + convert(varchar,nSiblings) + '.','')
    --+ coalesce(' Eligible Status: ' + pc.UeligbleStatus + '.','')
    )),5000)
  , pc.fCASARec
  , pc.fCaseDiff
  , es.EligibleStatusID as EligibleStatusID
  , getdate()
  , -1
  , getdate()
  , -1
  , case when isdate(pc.dProgramClosed)=1 then pc.dProgramClosed end
  , ccr.CaseClosureReasonID
  , 0
  , null
  , case when isdate(pc.dProgramClosed) = 1 then 0 else 1 end
  , case when isdate(pc.dCourtClosed) = 1 then pc.dCourtClosed end
  , ccrc.CaseClosureReasonID
  , case when isdate(pc.dProgramClosed) = 1 then pc.dProgramClosed end
  , ccr.CaseClosureReasonID
  , pc.sClosingNote
  , dbo.fn_ProgramID()
  , fp.FinalPlacementTypeID
  ,isnull(pc.fSafePermHome,0) as SafePermHome--This is not in Comet 3.9
,null as ChildReferralDate
,null as ChildReferralReasonID
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].tblPerson p on pc.nPersonID = p.nPersonID
left join [CometSource].[dbo].tblChildEligbleStatus ces on ces.nPersonID = pc.nPersonID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pc.nPersonID
join [CometSource].[dbo].ConversionGroupToCase cg on cg.nGroupID = pc.nGroupID
left join [Volunteer].[dbo].ChildReferral cr on cr.ChildReferralDesc = pc.sReferredBy and cr.programID = dbo.fn_ProgramID()
left join [Volunteer].[dbo].County cty on cty.CountyName = p.sHomeCounty and cty.programID = dbo.fn_ProgramID()
--left join [CometSource].[dbo].tblPersonChildHistory ch on p.nPersonID = ch.nPersonID
--left join @CaseChildHelper h on ch.nPersonID = h.nPersonID
left join @CaseChildHelper2 h2 on p.nPersonID = h2.nPersonID
left join [Volunteer].[dbo].EligibleStatus es on es.EligibleStatusDesc = ces.sEligbleStatus and es.programID = dbo.fn_ProgramID()
left join [Volunteer].[dbo].CaseClosureReason ccr on ccr.CaseClosureReasonDesc = pc.sProgramReason and ccr.programId = dbo.fn_ProgramID()
left join [Volunteer].[dbo].CaseClosureReason ccrc on ccrc.CaseClosureReasonDesc = pc.sCourtReason and ccrc.programId = dbo.fn_ProgramID()
left join [Volunteer].[dbo].HearingType ht on ht.HearingTypeDesc = pc.sIntitialStatus and ht.programID = dbo.fn_ProgramID()
left join [Volunteer].[dbo].FinalPlacementType fp on fp.FinalPlacementTypeName = pc.sFinalPlacement and fp.programID = dbo.fn_ProgramID()
left join [Volunteer].[dbo].CaseChildStatus ccs on ccs.StatusName = pc.sIntitialStatus and ccs.programID = dbo.fn_ProgramID()
