
---------------------------------------------------------------------------------
--CASE INFO
--Dependencies: CaseClosureReason
---------------------------------------------------------------------------------




/****** Object:  Table [dbo].[ConversionPersonToParty]    Script Date: 03/12/2012 22:41:26 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CometSource].[dbo].[ConversionGroupToCase]') AND type in (N'U'))
DROP TABLE [CometSource].[dbo].ConversionGroupToCase
GO

/****** Object:  Table [dbo].[ConversionGroupToCase]    Script Date: 03/12/2012 22:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CometSource].[dbo].ConversionGroupToCase(
	[nGroupID] [int] NOT NULL,
	[CaseID] [int] NOT NULL
) ON [PRIMARY]

GO




 
  declare @caseinfohelper_notes table (
      nGroupID int
    , ClosingNotes varchar(max)
  );
  insert into @caseinfohelper_notes
  SELECT 
       pc1.nGroupID
     , replace(ltrim(rtrim(STUFF(
     (SELECT + ', ' + sClosingNote
      FROM [CometSource].[dbo].tblPersonChild pc2
      WHERE pc2.nGroupID = pc1.nGroupID
      and pc2.sClosingNote is not null
      ORDER BY pc2.dProgramClosed
      FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
    FROM [CometSource].[dbo].tblPersonChild pc1
    where pc1.sClosingNote is not null
  GROUP BY pc1.nGroupID ;

  declare @caseinfohelper_notes2 table (
      nGroupID int
    , ClosingNotes varchar(max)
  );
  insert into @caseinfohelper_notes2
  SELECT 
       pc1.nGroupID
     , replace(ltrim(rtrim(STUFF(
     (SELECT + ', ' + sFinalPlacement
      FROM [CometSource].[dbo].tblPersonChild pc2
      WHERE pc2.nGroupID = pc1.nGroupID
      and pc2.sFinalPlacement is not null
      ORDER BY pc2.dProgramClosed
      FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
    FROM [CometSource].[dbo].tblPersonChild pc1
    where pc1.sFinalPlacement is not null
  GROUP BY pc1.nGroupID ;
  
  declare @caseinfohelper_dates table (
      nGroupID int
    , ProgramClosedFlag int
    , ProgramClosed datetime
    , ProgramClosureReason varchar(500)
    , CourtClosedFlag int
    , CourtClosed datetime
    , CourtClosureReason varchar(500)
  );
  insert into @caseinfohelper_dates
  SELECT 
       pc1.nGroupID
     , min(case when isdate(convert(datetime,pc1.dProgramClosed))=1 then 1 else 0 end)
     , max(case when isdate(convert(datetime,pc1.dProgramClosed))=1 then pc1.dProgramClosed end)
     , max(pc1.sProgramReason)
     , min(case when isdate(convert(datetime,pc1.dCourtClosed))=1 then 1 else 0 end)
     , max(case when isdate(convert(datetime,pc1.dCourtClosed))=1 then pc1.dCourtClosed end)
     , max(pc1.sCourtReason)
    FROM [CometSource].[dbo].tblPersonChild pc1
  GROUP BY pc1.nGroupID ;
  
  declare @CaseID int;
  declare @nGroupID int;
  declare @CaseNumber varchar(255);
  declare @CaseName varchar(255);
  declare @PriorityCase varchar(255);
  declare @StartDate datetime;
  declare @CloseDate datetime;
  declare @ClosePartyID int;
  declare @CourtClosureDate datetime;
  declare @CourtClosureReasonID int;
  declare @ProgramClosureDate datetime;
  declare @ProgramClosureReasonID int;
  declare @ClosingNotes varchar(max);
  declare @IsClosed int;
  declare @ProgramID int;
  declare @JurisdictionID int;

  set @ProgramID = dbo.fn_ProgramID()
  set @JurisdictionID = (select Jurisdictionid from dbo.Jurisdiction where JurisdictionName = dbo.fn_Jurisdiction()
and programid = dbo.fn_ProgramID())


   declare crs cursor for
  select 
     g.nGroupID
    ,left(g.sGroupNum,50)
    ,left(g.sGroupName,50)
    ,0 PriorityCase
    ,g.dCreated
    ,case when hd.ProgramClosedFlag = 1 then hd.ProgramClosed end
    ,null ClosePartyID
    ,case when hd.CourtClosedFlag = 1 then hd.CourtClosed end
    ,cr.CaseClosureReasonID
    ,case when hd.CourtClosedFlag = 1 then hd.ProgramClosed end
    ,pr.CaseClosureReasonID
    ,left(coalesce(hn.ClosingNotes + ' ','') + coalesce(hn2.ClosingNotes,''),5000)
    ,case when hd.ProgramClosed is null then 0 else 1 end IsClosed
    ,@ProgramID ProgramID
	,@JurisdictionID JurisdictionID
  from [CometSource].[dbo].tblGroup g
  left join @caseinfohelper_notes hn on g.nGroupID = hn.nGroupID
  left join @caseinfohelper_notes2 hn2 on g.nGroupID = hn2.nGroupID
  left join @caseinfohelper_dates hd on g.nGroupID = hd.nGroupID
  left join [dbo].CaseClosureReason cr on cr.CaseClosureReasonDesc = hd.CourtClosureReason and cr.programId = dbo.fn_ProgramID()
  left join [dbo].CaseClosureReason pr on pr.CaseClosureReasonDesc = hd.ProgramClosureReason and pr.programId = dbo.fn_ProgramID()
    ;
    
  open crs;

  fetch crs
   into       
     @nGroupID
    ,@CaseNumber
    ,@CaseName
    ,@PriorityCase
    ,@StartDate
    ,@CloseDate
    ,@ClosePartyID
    ,@CourtClosureDate
    ,@CourtClosureReasonID
    ,@ProgramClosureDate
    ,@ProgramClosureReasonID
    ,@ClosingNotes
    ,@IsClosed
    ,@ProgramID
	,@JurisdictionID
   
   while @@FETCH_STATUS = 0
   begin
      insert into [dbo].CaseInfo
              ( CaseNumber ,
                CaseName ,
                PriorityCase ,
                StartDate ,
                CloseDate ,
                ClosePartyID ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                CourtClosureDate ,
                CourtClosureReasonID ,
                ProgramClosureDate ,
                ProgramClosureReasonID ,
                ClosingNotes ,
                IsClosed ,
                programID,
		JurisdictionID
              )
      values  ( @CaseNumber ,
                @CaseName ,
                @PriorityCase ,
                @StartDate ,
                @CloseDate ,
                @ClosePartyID ,
                getdate() ,
                -1 ,
                getdate() ,
                -1 ,
                @CourtClosureDate ,
                @CourtClosureReasonID ,
                @ProgramClosureDate ,
                @ProgramClosureReasonID ,
                @ClosingNotes ,
                @IsClosed ,
                @ProgramID,
		@JurisdictionID
              )
           
      set @CaseID = SCOPE_IDENTITY();
      
      insert into [CometSource].[dbo].[ConversionGroupToCase]
              ( [nGroupID], [CaseID] )
      values  ( @nGroupID ,@CaseID );

      --print 'ID = ' + convert(varchar, @PersonID) + '  New ID = ' + convert(varchar, @PartyID);
   
      fetch crs
       into     
           @nGroupID
          ,@CaseNumber
          ,@CaseName
          ,@PriorityCase
          ,@StartDate
          ,@CloseDate
          ,@ClosePartyID
          ,@CourtClosureDate
          ,@CourtClosureReasonID
          ,@ProgramClosureDate
          ,@ProgramClosureReasonID
          ,@ClosingNotes
          ,@IsClosed
          ,@ProgramID
		  ,@JurisdictionID
   end
   
   close crs;
   deallocate crs;

go
