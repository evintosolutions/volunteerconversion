---------------------------------------------------------------------------------
--HearingType
--Dependencies: 
---------------------------------------------------------------------------------

 

with t as (
  select distinct sHearingType
  from [CometSource].[dbo].tblHearingType

  union
  select distinct sHearingTypeDesc sHearingType
  from [CometSource].[dbo].[_tblHearingType]
)

insert into [dbo].HearingType
        ( HearingTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sHearingType
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where len(sHearingType) > 0
and sHearingType not in (select HearingTypeDesc from dbo.hearingtype where programID = dbo.fn_ProgramID())
order by sHearingType