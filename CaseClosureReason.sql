--CaseClosureReason

with t as (
  select distinct sClosureReasonDesc
  from [CometSource].[dbo].[_tblClosureReason]
  where len(sClosureReasonDesc)>0

  union 
  
  select distinct sCourtReason sClosureReasonDesc
  from [CometSource].[dbo].tblPersonChild
  where len(sCourtReason) > 0

  union 
  
  select distinct sProgramReason sClosureReasonDesc
  from [CometSource].[dbo].tblPersonChild
  where len(sProgramReason) > 0
  
)

insert into [dbo].CaseClosureReason
        ( CaseClosureReasonDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct sClosureReasonDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from t
where sClosureReasonDesc not in (select CaseClosureReasonDesc from dbo.CaseClosureReason where programId = dbo.fn_ProgramID())
order by sClosureReasonDesc
