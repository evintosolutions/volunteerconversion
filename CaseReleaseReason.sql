
--CaseReleaseReason

insert into [dbo].CaseReleaseReason
        ( CaseReleaseReasonDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sRemovalReason, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblRemovalReason]
where len(sRemovalReason)>0
and sRemovalReason not in (select CaseReleaseReasonDesc from [dbo].CaseReleaseReason where programid = dbo.fn_programid())
order by sRemovalReason
