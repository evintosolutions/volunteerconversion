

declare @helper table (CourseName varchar(100), CourseHours int, programID int);

with l as
(
    select sTrainingTopic, dDateCompleted, nID, row_number() over(partition by sTrainingTopic order by dDateCompleted desc, nID desc) RankNum
    from [CometSource].[dbo].tblPersonVolunteerTraining pvt
    where sType = 'Initial'
    and sTrainingTopic is not null
and sTrainingTopic not in (select coursename from dbo.ApplicantTrainingCourse where programID = dbo.fn_ProgramID())
    group by sTrainingTopic, dDateCompleted, nID
    
)

insert into @helper
select distinct 
l.sTrainingTopic
,case when isnumeric(vt.nHours) = 1 then vt.nHours end
,dbo.fn_ProgramID()
from l
join [CometSource].[dbo].tblPersonVolunteerTraining vt on vt.nID = l.nID
where l.RankNum = 1

--Issue 719,taking out
--insert into @helper
--select distinct
--sTrainingTopicDesc
--,null
--,dbo.fn_ProgramID()
--from [CometSource].[dbo]._tblTrainingTopic tt
--where not exists (select 1 from @helper h where h.CourseName = tt.sTrainingTopicDesc)

insert into [dbo].[ApplicantTrainingCourse]
        ( [CourseName] ,
          [CourseHours] ,
          [programID] ,
          [Active] ,
          [AddDate] ,
          [AddUserID] ,
          [ModDate] ,
          [ModUserID]
        )
select distinct
CourseName
,case when dbo.fn_ProgramID() = 3 then 30 when dbo.fn_ProgramID() = 4 then coalesce(CourseHours,1) else 1 end
,programID
,1
,getdate()
,-1
,getdate()
,-1
from @helper
where len(CourseName) > 0
order by CourseName

--from [CometSource].[dbo].[_tblTrainingTopic] t
--join l on l.sTrainingTopic = t.sTrainingTopicDesc and l.RankNum = 1
--join [CometSource].[dbo].tblPersonVolunteerTraining vt on vt.nID = l.nID
--order by t.sTrainingTopicDesc



insert into [dbo].[ApplicantTraining]
        ( [CourseID] ,
          [PartyID] ,
          [TrainingDate] ,
          [Notes] ,
          [programID] ,
          [CompletedDate]
        )
select distinct 
atc.CourseID
,c.PartyID
,case when isdate(convert(datetime,pvt.dDateScheduled)) = 1  then pvt.dDateScheduled else convert(date,'1/1/11') end
,null
,dbo.fn_ProgramID()
,case when isdate(convert(datetime,pvt.dDateCompleted)) = 1  then pvt.dDateCompleted else 
--Issue 746, change to null
--convert(date,'1/1/11') 
null 
end
from [CometSource].[dbo].tblPersonVolunteerTraining pvt
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pvt.nPersonID
join [dbo].ApplicantTrainingCourse atc on atc.CourseName = pvt.sTrainingTopic and atc.programID = dbo.fn_ProgramID()
join dbo.Volunteer v on v.PartyID = c.PartyID
where pvt.sType = 'Initial'
--and not exists (select 1 from [dbo].Volunteer v where v.PartyID = c.PartyID)


------------------------------



