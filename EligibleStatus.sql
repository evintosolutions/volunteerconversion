--EligibleStatus


insert into [dbo].EligibleStatus
        ( EligibleStatusDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sEligbleStatus, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblEligbleStatus]
where len(sEligbleStatus)>0
and sEligbleStatus not in (select EligibleStatusDesc from dbo.EligibleStatus where programID = dbo.fn_ProgramID())
order by sEligbleStatus