

  declare @volunteerinfohelper table (
      nPersonID int
    , PolicyHolder varchar(max)
    , Skills varchar(max)
    , Interests varchar(max)
  );
  insert into @volunteerinfohelper
  SELECT 
       pc1.nPersonID
     , max(aut.sPolicyHolder)
     , replace(ltrim(rtrim(STUFF(
     (SELECT + ', ' + sSkill
      FROM [CometSource].[dbo].tblPerson pc2
      join [CometSource].[dbo].tblPersonVolunteerAuto aut on pc2.nPersonID = aut.nPersonID
      join [CometSource].[dbo].tblPersonVolunteerSkill sk on pc2.nPersonID = sk.nPersonID
      --join [CometSource].[dbo].tblPersonVolunteerInterest interest on sk.nPersonID = interest.nPersonID
      WHERE pc2.nPersonID = pc1.nPersonID
      FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
     , replace(ltrim(rtrim(STUFF(
     (SELECT + ', ' + sInterest
      FROM [CometSource].[dbo].tblPerson pc2
      join [CometSource].[dbo].tblPersonVolunteerAuto aut on pc2.nPersonID = aut.nPersonID
      --join [CometSource].[dbo].tblPersonVolunteerSkill sk on pc2.nPersonID = sk.nPersonID
      join [CometSource].[dbo].tblPersonVolunteerInterest interest on aut.nPersonID = interest.nPersonID
      WHERE pc2.nPersonID = pc1.nPersonID
      FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
    FROM [CometSource].[dbo].tblPerson pc1
    left join [CometSource].[dbo].tblPersonVolunteerAuto aut on aut.nPersonID = pc1.nPersonID
  GROUP BY pc1.nPersonID ;
  
  --Then insert non-supervisors
insert into [dbo].Volunteer
        ( PartyID ,
          SupervisorPartyID ,
          UserID ,
          Available ,
		  AvailableDate,
          VolunteerTypeID ,
          VolunteerStatusID ,
          VolunteerStatusReasonID ,
          SkillsandInterests ,
          DriversLicenseNumber ,
          DriversLicenseState ,
          DriversLicenseExpireDate ,
          DriversLicenseConfirmDate ,
          DriversLicenseConfirmPartyID ,
          AutoAccess ,
          AutoInsurCompany ,
          AutoInsurExpireDate ,
          AutoInsurConfirmDate ,
          AutoInsurConfirmPartyID ,
          VolunteerReferralID ,
          BackgroundCheckPermission ,
          ShareInfoPermission ,
          ApplicationSignature ,
          ApplicationDate ,
          JudgeDenialReason ,
          PrefMaleFemale ,
          PrefAgeFrom ,
          PrefAgeTo ,
          PrefIndividualGroup ,
          AcceptedDate ,
          SwornDate ,
          InitalTrainingDate ,
          OnLeaveFromDate ,
          OnLeaveToDate ,
          DateDischarged ,
          Notes ,
          ApplicationNotes ,
          CertifiedDate ,
          RejectedDate ,
          LastBackgroundCheckDate ,
          ReferencesCompletedDate ,
          IsVolunteer ,
          IsSupervisor ,
          Skills ,
          Interests ,
          RejectedReasonID ,
          Assigned ,
          DischargeReasonID ,
          EligibleForRehire ,
          AutoInsurPolicyNumber ,
          programID
        )
select distinct
  c.PartyID
  ,csup.PartyID
  ,-1
  ,coalesce(pv.fAvailability,0)
  ,null as AvailableDate
  ,t.VolunteerTypeID
  ,st.VolunteerStatusID
  ,null
  ,null
  ,pv.sDLNum
  ,null
  ,null
  ,null
  ,null
  ,null
  ,h.PolicyHolder
  ,null
  ,null
  ,null
  ,ref.VolunteerReferralID
  ,null
  ,0
  ,null
  ,null
  ,null
  ,case when pv.sGenderPref in('M','Male') or pv.sGenderPref = '0' then 0 when pv.sGenderPref in('F','Female') or pv.sGenderPref = '1' then 1 end
  ,case when isnumeric(pv.nAgePrefStart) = 1 then convert(tinyint,pv.nAgePrefStart) end
  ,case when isnumeric(pv.nAgePredEnd) = 1 then convert(tinyint,pv.nAgePredEnd) end
  ,case when pv.sGroupPref in('G','Group') then 0 when pv.sGroupPref in('I','Individual') then 1 end
  ,case when isdate(pv.dDateAccepted) = 1 then pv.dDateAccepted end
  ,case when isdate(pv.dDateSworn) = 1 then pv.dDateSworn end
  ,case when isdate(pv.dDateInitTraining) = 1 then pv.dDateInitTraining end
  ,case when isdate(pv.dLeaveDate) = 1 then pv.dLeaveDate end
  ,case when isdate(pv.dReturnDate) = 1 then pv.dReturnDate end
  ,case when isdate(pv.dDateDischarge) = 1 then pv.dDateDischarge end
  ,null
  --left(coalesce(pv.sNote + '. ','') + case when len(pv.sDefaultSupervisor) > 0 then 'Default Supervisor: ' + pv.sDefaultSupervisor else '' end,5000)
  ,null
  ,null
  ,null
  ,null
  ,null
  ,case when p.sCategory = 'Vol' then 1 else 0 end
  ,case when p.sCategory = 'Sprvsr' then 1 else 0 end
  ,left(h.Skills,5000)
  ,left(h.Interests,5000)
  ,null
  ,coalesce(case when pv.sVolunteerStatus in ('Assigned to case') then 1 else 0 end,0)
  ,dr.DischargeReasonID
  ,1
  ,null
  ,null--dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer pv
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pv.nPersonID
join [CometSource].[dbo].tblPerson p on pv.nPersonID = p.nPersonID
left join [CometSource].[dbo].ConversionPersonToParty csup on csup.PersonID = pv.nSupervisorID
left join [dbo].VolunteerType t on t.VolunteerTypeDesc = pv.sVolunteerCat and t.programId = dbo.fn_ProgramID()
left join [dbo].VolunteerStatus st on st.VolunteerStatusName = pv.sVolunteerStatus and st.programID = dbo.fn_ProgramID()
left join @volunteerinfohelper h on h.nPersonID = p.nPersonID 
left join [dbo].VolunteerReferral ref on ref.VolunteerReferralDesc = pv.sVolunteerReferral and ref.programId = dbo.fn_ProgramID()
left join [dbo].VolunteerDischargeReason dr on dr.DischargeReasonDesc = pv.sDischargeReason and dr.programID = dbo.fn_ProgramID()
where p.sCategory <> 'Sprvsr'
order by c.PartyID

go