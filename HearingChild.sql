
---------------------------------------------------------------------------------
--HEARING CHILD
--Dependencies: Hearing, CaseChild
---------------------------------------------------------------------------------

 
insert into [dbo].HearingChild
        ( HearingID, CaseChildID, programID )
select distinct
  h.HearingID
, cc.CaseChildID  
,dbo.fn_ProgramID()
from [dbo].Hearing h
join [dbo].CaseChild cc on h.CaseID = cc.CaseID and cc.programID = dbo.fn_ProgramID()
