--EmploymentStatus


insert into [dbo].EmploymentStatus
        ( EmploymentStatusDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct p.sEmpStatus, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPerson p
where len(p.sEmpStatus) > 0
and p.sEmpStatus not in (select EmploymentStatusdesc from dbo.EmploymentStatus where programID = dbo.fn_ProgramID())
order by p.sEmpStatus
