
---------------------------------------------------------------------------------
--HEARING OUTCOME TYPE
--Dependencies: 
---------------------------------------------------------------------------------
 

with t as (

select 
ho.sHearingOutcome
from [CometSource].[dbo].tblHearingOutcome ho

 union

select ho2.nHearingOutcomeDesc sHearingOutcome
from [CometSource].[dbo].[_tblHearingOutcome] ho2

)

insert into [dbo].HearingOutcomeType
        ( HearingOutcomeTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sHearingOutcome
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sHearingOutcome not in (select HearingOutcomeTypeDesc from dbo.HearingOutcomeType where programid = dbo.fn_programid())
order by sHearingOutcome


-------------------------------------------------------------------------------
--HEARING OUTCOME
--Dependencies: 
---------------------------------------------------------------------------------

 
insert into [dbo].HearingOutcome
        ( HearingID ,
          HearingOutcomeTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select
  ch.HearingID
, hot.HearingOutcomeTypeID
, getdate()
, -1
, getdate()
, -1
, dbo.fn_ProgramID()
from [CometSource].[dbo].tblHearingOutcome ho
join [CometSource].[dbo].ConversionHearingToHearing ch on ch.nHearingID = ho.nHearingID
join [dbo].HearingOutcomeType hot on hot.HearingOutcomeTypeDesc = ho.sHearingOutcome and hot.programID = dbo.fn_ProgramID()
