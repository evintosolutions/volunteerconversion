-----------------------------------------------------------------------
--PlacementReason
--Dependencies: 
---------------------------------------------------------------------------------

 
with t as (
select distinct 
p.sPlacementReason
from [CometSource].[dbo].tblCOPlacement p
where len(p.sPlacementReason) > 0

union

select distinct 
p.sPlacementReasonDesc sPlacementReason
from [CometSource].[dbo].[_tblPlacementReason] p
where len(p.sPlacementReasonDesc) > 0

)

insert into [dbo].PlacementReason
        ( PlacementReasonDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sPlacementReason
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sPlacementReason not in (select PlacementReasonDesc from dbo.Placementreason where programid = dbo.fn_programid())
order by sPlacementReason





---------------------------------------------------------------------------------
--PlacementHistory
--Dependencies: CaseChild, CaseInfo, Hearing, PlacementFacility, PlacementReason, FinalPlacementType
---------------------------------------------------------------------------------

 
insert into [dbo].PlacementHistory
        ( CaseChildID ,
          CaseID ,
          HearingID ,
          PlacementFacilityID ,
          PlacementNonFacilityID ,
          PlacementFromDate ,
          PlacementToDate ,
          PlacementReasonID ,
          ReasonableDistance ,
          InCounty ,
          Notes ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          WithSiblings ,
          PlacementTypeID ,
          programID
        )
select cc.CaseChildID
,cc.CaseID
,hh.HearingID
,pf.PlacementFacilityID
,null
,case when isdate(convert(datetime,p.dPlacementDate))=1 then dPlacementDate end
,case when isdate(convert(datetime,p.dPlacementEndDate))=1 then dPlacementEndDate end
,pr.PlacementReasonID
,case when p.f30Miles = 1 then 1 else 0 end
,case when p.fWithinCounty = 1 then 1 else 0 end
,coalesce(p.sNote,'')
,getdate()
,-1
,getdate()
,-1
,0
,pf.PlacementTypeID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblCOPlacement p
join [CometSource].[dbo].tblPerson tp on tp.nPersonID = p.nPersonID and tp.sCategory = 'Child'
left join [dbo].PlacementReason pr on pr.PlacementReasonDesc = p.sPlacementReason and pr.programID = dbo.fn_ProgramID()
left join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
left join [dbo].CaseChild cc on cc.ChildPartyID = c.PartyID and cc.ProgramID = dbo.fn_ProgramID()
left join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = p.nHearingID
left join [dbo].Hearing h on h.HearingID = hh.HearingID
left join [CometSource].[dbo].tblFacility f on f.nFacilityID = p.nFacilityID
left join [dbo].PlacementFacility pf 
  on pf.FacilityName = f.sFacilityName 
  and exists (select 1 from [dbo].PlacementType pt where pt.PlacementTypeID = pf.PlacementTypeID and pt.PlacementTypeDesc = f.sFacilityCat and pt.programID = dbo.fn_ProgramID())
  and pf.programId = dbo.fn_ProgramID()
--where isdate(p.dPlacementDate)=1 

