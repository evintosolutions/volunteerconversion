--PetitionCaseType

with t as (
  select distinct sPetitionCatDesc
  from [CometSource].[dbo].[_tblPetitionCat]
  where len(sPetitionCatDesc)>0

  union
  
  select distinct sPetitionCat sPetitionCatDesc
  from [CometSource].[dbo].tblPetition
  where len(sPetitionCat) > 0

)
insert into [dbo].PetitionCaseType
        ( PetitionCaseTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct sPetitionCatDesc,1,getdate(),-1,getdate(),-1,dbo.fn_ProgramID()
from t
where sPetitionCatDesc not in (select PetitionCaseTypeDesc  from dbo.PetitionCaseType where programid = dbo.fn_ProgramID())
order by sPetitionCatDesc


---------------------------------------------------------------------------------
--PETITION
--Dependencies: Petition
---------------------------------------------------------------------------------

/****** Object:  Table [dbo].[ConversionPersonToParty]    Script Date: 03/12/2012 22:41:26 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CometSource].[dbo].[ConversionPetitionToPetition]') AND type in (N'U'))
DROP TABLE [CometSource].[dbo].ConversionPetitionToPetition
GO

/****** Object:  Table [dbo].[ConversionPetitionToPetition]    Script Date: 03/12/2012 22:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CometSource].[dbo].ConversionPetitionToPetition(
	[sPetitionNum] [varchar](100) NOT NULL,
	[PetitionID] [int] NOT NULL
) ON [PRIMARY]

GO
 


  declare @PetitionID int;
  declare @CaseID int;
  declare @PetitionNumber varchar(50)
  declare @PetitionDate date
  declare @PetitionEndDate date
  declare @PetitionCaseTypeID int
  declare @ProgramID int

  set @ProgramID = dbo.fn_ProgramID()

   declare crs cursor for
    select distinct
        cc.CaseID
      , p.sPetitionNum
      , min(case when isdate(convert(datetime,p.dPetitionDate)) = 1 then p.dPetitionDate end)
       --Issue 747, items 1 and 2. Changing to use null if any of the Petition records has a null EndDate
	  ,case
	  when exists (select sPetitionNum from CometSource.dbo.tblPetition
	  where sPetitionNum = p.sPetitionNum
	  and dEndDate is null)
	  then null 
	  else 
	  max(case when isdate(convert(datetime,p.dEndDate)) = 1 then p.dEndDate end)
      end
      , ct.PetitionCaseTypeID
      , dbo.fn_ProgramID() ProgramID
    from [CometSource].[dbo].tblPetition p
    join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = p.nPersonID
    join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.programID = dbo.fn_ProgramID()
    --Making left join for Issue 747, item number 3
    left join [dbo].PetitionCaseType ct on ct.PetitionCaseTypeDesc = p.sPetitionCat and ct.programId = dbo.fn_ProgramID()
    where p.sPetitionNum <> '1'
    and len(p.sPetitionNum) > 2
    --and p.sPetitionNum like '02-1066%'
    --and isdate(p.dPetitionDate) = 1
    group by
        cc.CaseID
      , p.sPetitionNum
      , ct.PetitionCaseTypeID
    ;
  
  open crs;

  fetch crs
   into
     @CaseID
    ,@PetitionNumber
    ,@PetitionDate
    ,@PetitionEndDate
    ,@PetitionCaseTypeID
    ,@ProgramID
   
   while @@FETCH_STATUS = 0
   begin
      insert into [dbo].Petition
              ( CaseID ,
                PetitionNumber ,
                PetitionDate ,
                PetitionEndDate ,
                PetitionCaseTypeID ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                programID
              )
      values  ( 
           @CaseID
          ,@PetitionNumber
          ,@PetitionDate
          ,@PetitionEndDate
          ,@PetitionCaseTypeID
          ,getdate()
          ,-1
          ,getdate()
          ,-1
          ,@ProgramID
      )
           
      set @PetitionID = SCOPE_IDENTITY();
      
      insert into [CometSource].[dbo].[ConversionPetitionToPetition]
              ( [sPetitionNum], [PetitionID] )
      values  ( @PetitionNumber ,@PetitionID );

      --print 'ID = ' + convert(varchar, @PersonID) + '  New ID = ' + convert(varchar, @PartyID);
   
      fetch crs
       into
           @CaseID
          ,@PetitionNumber
          ,@PetitionDate
          ,@PetitionEndDate
          ,@PetitionCaseTypeID
          ,@ProgramID
   end
   
   close crs;
   deallocate crs;

go
