------------------------------
--PROGRAM SPECIFIC CODE
------------------------------

use [Volunteer];
go
 
--UPDATE Active flag for Party

--FOR programid of 6
if dbo.fn_ProgramID() = 6
begin
  --update p
  --set Active = case 
  --              when pv.sVolunteerStatus in('Active Board Member','Active Friend of CASA','Application/Training Process','Assigned to Case','Friend of CASA--All Inclusive','Friend of CASA--Intern','Friend of CASA--Misc.','In Training','On Leave','Resting')
  --                then 1
  --              else 0
  --             end
  --from [Volunteer].[dbo].Party p
  --join [CometSource].dbo.ConversionPersonToParty cpp on cpp.PartyID = p.PartyID
  --join [CometSource].dbo.tblPersonVolunteer pv on nPersonID = cpp.PersonID
  --where p.programID = dbo.fn_ProgramID()

  update v
  set IsVolunteer = 0
  from [Volunteer].[dbo].Volunteer v
  join [CometSource].dbo.ConversionPersonToParty cpp on cpp.PartyID = v.PartyID
  join [CometSource].dbo.tblPersonVolunteer pv on nPersonID = cpp.PersonID
  where pv.dDateSworn is null and sVolunteerStatus = 'In Training'
  and exists (select 1 from [Volunteer].[dbo].Party p where p.PartyID = v.PartyID and p.programID = dbo.fn_ProgramID())
end

--FOR programid of 4
if dbo.fn_ProgramID() = 4
begin
  --update p
  --set Active = 0
  --from [Volunteer].[dbo].Party p
  --join [Volunteer].[dbo].Volunteer v on v.PartyID = p.PartyID
  --join [Volunteer].[dbo].VolunteerType vt on vt.VolunteerTypeID = v.VolunteerTypeID and vt.VolunteerTypeDesc = 'Inactive Volunteer'
  --where p.programID = dbo.fn_ProgramID()
  
  update v
  set Assigned = 1
  from [Volunteer].[dbo].Party p
  join [Volunteer].[dbo].Volunteer v on v.PartyID = p.PartyID
  join [Volunteer].[dbo].VolunteerStatus vs on vs.VolunteerStatusID = v.VolunteerStatusID and vs.VolunteerStatusName in('Assigned to Case -- Available','Assigned to Case -- Not Available')
  where p.programID = dbo.fn_ProgramID()
  
  --update p
  --set Active = 0
  --from [Volunteer].[dbo].Party p
  --join [Volunteer].[dbo].Volunteer v on v.PartyID = p.PartyID
  --join [Volunteer].[dbo].VolunteerStatus vs on vs.VolunteerStatusID = v.VolunteerStatusID and vs.VolunteerStatusName in('Discharged From Duty')
  --where p.programID = dbo.fn_ProgramID()  
end

--FOR programid of 3: tblPersonVolunteer, sVolunteerStatus -> 'Inactive'
if dbo.fn_ProgramID() = 3
begin
  declare @placeholder int;
  --update p
  --set Active = 0
  --from [CometSource].[dbo].tblPersonVolunteer pv
  --join [CometSource].[dbo].ConversionPersonToParty cpp on cpp.PersonID = pv.nPersonID
  --join [Volunteer].[dbo].Party p on p.PartyID = cpp.PartyID
  --where p.programID = 3
  --and pv.sVolunteerStatus = 'Inactive'
end
