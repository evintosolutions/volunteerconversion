


---------------------------------------------------------------------------------
--CASE VOLUNTEER
--Dependencies: CaseInfo, Party, CaseReleaseReason
---------------------------------------------------------------------------------

 
insert into [dbo].CaseVolunteer
        ( AssignedDate ,
          ReleasedDate ,
          CaseID ,
          PartyID ,
          CaseReleaseReasonID ,
          JurisdictionID ,
          CertificationID ,
          programID ,
          ContactLogApprovalPartyID ,
          CaseSupervisor
        )
select distinct
  min(case when isdate(convert(datetime,pr.dDateAssigned))=1 then pr.dDateAssigned end)
, max(case when isdate(convert(datetime,pr.dDateRemoved))=1 then pr.dDateRemoved end)
, cg.CaseID
, cp.PartyID
, max(rr.CaseReleaseReasonID)
, null
, null
,dbo.fn_ProgramID()
--Issue 718: Contact Log ApprovalPartyID null instead of v.SupervisorPartyID
--,v.SupervisorPartyID
,null as ContactLogApprovalPartyID
--Issue 720: Use v.IsSupervisor to set CaseSupervisor vale
,case
when v.IsSupervisor = 1 then 1
else 0
end as CaseSupervisor
from [CometSource].[dbo].tblPersonRelationship pr 
join [CometSource].[dbo].tblPersonChild pc on pr.nPersonID = pc.nPersonID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nRelationshipID
join [CometSource].[dbo].ConversionGroupToCase cg on cg.nGroupID = pc.nGroupID
join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() and p.PartyType in (select PartyType from [dbo].PartyType where programID = 1 and PartyTypeNameShort in('Vol','Sprvsr'))
left join [dbo].Volunteer v on v.PartyID = p.PartyID
left join [dbo].CaseReleaseReason rr on rr.CaseReleaseReasonDesc = pr.sRemovalReason and rr.programID = dbo.fn_ProgramID()
group by cg.CaseID, cp.PartyID, v.SupervisorPartyID, v.IsSupervisor
--having min(case when isdate(pr.dDateAssigned)=1 then pr.dDateAssigned end) is not null;







/*
--their supervisors

 
insert into [dbo].CaseVolunteer
        ( AssignedDate ,
          ReleasedDate ,
          CaseID ,
          PartyID ,
          CaseReleaseReasonID ,
          JurisdictionID ,
          CertificationID ,
          programID
        )
select distinct
  min(case when isdate(pr.dDateAssigned)=1 then pr.dDateAssigned end)
, max(case when isdate(pr.dDateRemoved)=1 then pr.dDateRemoved end)
, cg.CaseID
, v.SupervisorPartyID
, rr.CaseReleaseReasonID
, null
, null
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonRelationship pr 
join [CometSource].[dbo].tblPersonChild pc on pr.nPersonID = pc.nPersonID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nRelationshipID
join [CometSource].[dbo].ConversionGroupToCase cg on cg.nGroupID = pc.nGroupID
join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() and p.PartyType = (select PartyType from [dbo].PartyType where programID = 1 and PartyTypeNameShort = 'Vol')
join [dbo].Volunteer v on v.PartyID = p.PartyID
left join [dbo].CaseReleaseReason rr on rr.CaseReleaseReasonDesc = pr.sRemovalReason and rr.programID = dbo.fn_ProgramID()
where v.SupervisorPartyID is not null
group by cg.CaseID, v.SupervisorPartyID, rr.CaseReleaseReasonID
having min(case when isdate(pr.dDateAssigned)=1 then pr.dDateAssigned end) is not null;
*/


