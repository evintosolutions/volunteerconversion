
---------------------------------------------------------------------------------
--HEARING DETAIL
--Dependencies: Hearing, HearingType
---------------------------------------------------------------------------------

 
insert into [dbo].HearingDetail
        ( HearingID ,
          HearingTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select 
  ch.HearingID
, ht2.HearingTypeID
, getdate()
, -1
, getdate()
, -1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblHearingType ht
join [CometSource].[dbo].ConversionHearingToHearing ch on ch.nHearingID = ht.nHearingID
join [dbo].HearingType ht2 on ht2.HearingTypeDesc = ht.sHearingType and ht2.programID = dbo.fn_ProgramID()
