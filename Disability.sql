

--DisabilityType

with t as (
  select distinct sDisabilityDesc
  from [CometSource].[dbo].[_tblDisability]
  where len(sDisabilityDesc)>0
  and sDisabilityDesc not in (select DisabilityTypeDesc from dbo.DisabilityType where programID = dbo.fn_ProgramID())

  union
  
  select distinct sDisabilityPref sDisabilityDesc
  from [CometSource].[dbo].tblPersonVolunteer
  where len(sDisabilityPref) > 0
  and sDisabilityPref not in (select DisabilityTypeDesc from dbo.DisabilityType where programID = dbo.fn_ProgramID())

)

insert into [dbo].DisabilityType
        ( DisabilityTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
	,AssessmentCategoryID
        )
select distinct sDisabilityDesc, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID(),null
from t
order by sDisabilityDesc




insert into [dbo].Disability
        ( PartyID ,
          DisabilityTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
  c.PartyID
  ,t.DisabilityTypeID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonDisability pd
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pd.nPersonID
join [dbo].DisabilityType t on t.DisabilityTypeDesc = pd.sDisability and t.programID = dbo.fn_ProgramID()

------------------------------
 
insert into [dbo].PrefDisability
        ( PartyID ,
          DisabilityTypeID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct
  c.PartyID
  ,t.DisabilityTypeID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer p
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
left join [dbo].DisabilityType t on t.DisabilityTypeDesc = p.sDisabilityPref and t.programID = dbo.fn_ProgramID()
where p.sDisabilityPref is not null

--select *
--from [CometSource].[dbo].tblPersonVolunteer p
--join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
--where not exists (select 1 from dbo.Volunteer v where v.PartyID = c.PartyID)
--order by p.nPersonID

------------------------------

