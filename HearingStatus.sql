
---------------------------------------------------------------------------------
--HEARING STATUS
--Dependencies: 
---------------------------------------------------------------------------------


insert into [dbo].HearingStatus
        ( HearingStatusDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
ltrim(rtrim(hs.sHearingStatusDesc))
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblHearingStatus] hs
where len(ltrim(rtrim(hs.sHearingStatusDesc))) > 0
and ltrim(rtrim(hs.sHearingStatusDesc)) not in (select HearingStatusDesc from dbo.Hearingstatus where programid = dbo.fn_ProgramID())