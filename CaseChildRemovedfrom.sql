
------------------------------------------------------------------------------
--CASE CHILD REMOVED FROM
--Dependencies: CaseChild, RelationshipType
---------------------------------------------------------------------------------


insert into [dbo].CaseChildRemovedFrom
        ( CaseChildID ,
          RelationshipTypeID ,
          programID
        )
select
  cc.CaseChildID
, rt.RelationshipTypeID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pc.nPersonID
join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.programID = dbo.fn_ProgramID()
join [dbo].RelationshipType rt on rt.RelationshipTypeName = pc.sRemovedRelation and rt.programID = dbo.fn_ProgramID()
