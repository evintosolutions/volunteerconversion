
---------------------------------------------------------------------------------
--CASE FAMILY MEMBER
--Dependencies: Party, CaseInfo
---------------------------------------------------------------------------------


insert into [dbo].CaseFamilyMember
        ( FamilyPartyID ,
          CaseID ,
          RelationshipTypeID ,
          programID
        )
select distinct
fp.PartyID
,cc.CaseID
,null
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonRelationship pr
join [CometSource].[dbo].tblPerson p on p.nPersonID = pr.nRelationshipID and p.sCategory = 'Family'
join [CometSource].[dbo].ConversionPersonToParty fp on fp.PersonID = pr.nRelationshipID
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nPersonID
join [dbo].CaseChild cc on cc.ChildPartyID = cp.PartyID and cc.programID = dbo.fn_ProgramID()

union

select distinct 
  cp.PartyID
, cg.CaseID
, null
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonFamily pf
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pf.nPersonID
join [CometSource].[dbo].ConversionGroupToCase cg on cg.nGroupID = pf.nGroupID
