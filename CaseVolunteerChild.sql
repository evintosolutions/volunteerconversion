 
---------------------------------------------------------------------------------
--CASE VOLUNTEER CHILD
--Dependencies: CaseInfo, Party, CaseReleaseReason
---------------------------------------------------------------------------------


 
insert into [dbo].CaseVolunteerChild
        ( CaseVolunteerID ,
          CaseChildID ,
          AssignedDate ,
          ReleasedDate ,
          PrepareCourtReport ,
          CaseReleaseReasonID ,
          programID
        )
select distinct
  cv.CaseVolunteerID
, cc.CaseChildID
, case when isdate(convert(datetime,pr.dDateAssigned))=1 then pr.dDateAssigned end
, case when isdate(convert(datetime,pr.dDateRemoved))=1 then pr.dDateRemoved end
, 1
, crr.CaseReleaseReasonID
, dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonRelationship pr 
join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nRelationshipID
join [dbo].CaseVolunteer cv on cv.PartyID = cp.PartyID and cv.programID = dbo.fn_ProgramID()
join [CometSource].[dbo].ConversionPersonToParty cpc on cpc.PersonID = pr.nPersonID
join [dbo].CaseChild cc on cc.ChildPartyID = cpc.PartyID and cc.programID = dbo.fn_ProgramID() and cc.CaseID = cv.CaseID
left join [dbo].CaseReleaseReason crr on crr.CaseReleaseReasonDesc = pr.sRemovalReason and crr.programID = dbo.fn_ProgramID()
--where case when isdate(pr.dDateAssigned)=1 then pr.dDateAssigned end is not null;
