
--InterestedPartyType


insert into [dbo].InterestedPartyType
        ( InterestedPartyTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sIntPsnRoleDesc
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblIntPsnRole]
where len(sIntPsnRoleDesc) > 0
and sIntPsnRoleDesc not in (select InterestedPartyTypeDesc from dbo.InterestedPartyType where programID = dbo.fn_ProgramID())
order by sIntPsnRoleDesc



------------------------------


insert into [dbo].AssociatedParty
        ( PartyID ,
          InterestedPartyTypeID ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
c.PartyID
,ipt.InterestedPartyTypeID
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonRelationship pr
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pr.nRelationshipID
join [dbo].Party p on p.PartyID = c.PartyID
left join [CometSource].[dbo].tblRelationshipChildIntPerson ip on ip.nPersonRelationshipID = pr.nPersonRelationshipID
left join [dbo].InterestedPartyType ipt on ipt.InterestedPartyTypeDesc = ip.sIntPsnRole and ipt.programID = dbo.fn_ProgramID()
where p.PartyType in(select PartyType from [dbo].PartyType where PartyTypeNameShort in('Atty','Case','Int'))





--For tblPerson records that are potential AssociatedParties but are not already in tblPersonRelationship
insert into [dbo].AssociatedParty
        ( PartyID ,
          InterestedPartyTypeID ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
cpp.PartyID
,null
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPerson p
join [CometSource].[dbo].ConversionPersonToParty cpp on cpp.PersonID = p.nPersonID
join [dbo].Party par on par.PartyID = cpp.PartyID
left join [CometSource].[dbo].tblPersonRelationship pr on pr.nRelationshipID = p.nPersonID
where par.PartyType in(select PartyType from [dbo].PartyType where PartyTypeNameShort in('Atty','Case','Int'))
and pr.nPersonID is null





