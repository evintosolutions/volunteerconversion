


update dbo.li_plc
set [bothchild id] = [child id]
where [bothchild id] is null and [child id] is not null


update dbo.advgal 
set [Misc Trainee Items] = l.[Misc Trainee Items]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[Misc Trainee Items] is not null


update dbo.advgal 
set [ChildABuseReg_Date] = l.[ChildABuseReg_Date]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[ChildABuseReg_Date] is not null


    update dbo.advgal 
set [ChildABuseReg_Finding] = l.[ChildABuseReg_Finding]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[ChildABuseReg_Finding] is not null

    update dbo.advgal 
set [Date Fingerprinted] = l.[Date Fingerprinted]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[Date Fingerprinted] is not null
      

	      update dbo.advgal 
set [Date FingerPrintReCheck] = l.[Date FingerPrintReCheck]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[Date FingerPrintReCheck] is not null



    	      update dbo.advgal 
set [Date FingerPrintEval] = l.[Date FingerPrintEval]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[Date FingerPrintEval] is not null


        	      update dbo.advgal 
set [DateCriminalCheck] = l.[DateCriminalCheck]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[DateCriminalCheck] is not null


              	      update dbo.advgal 
set [Date reg] = l.[Date reg]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[Date reg] is not null

              	      update dbo.advgal 
set [date done] = l.[date done]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[date done] is not null


	                	      update dbo.advgal 
set [crimcheck] = l.[crimcheck]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[crimcheck] is not null


	  	                	      update dbo.advgal 
set [crimcheckfinding] = l.[crimcheckfinding]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[crimcheckfinding] is not null

	  	                	      update dbo.advgal 
set [intrv 1date] = l.[intrv 1date]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[intrv 1date] is not null

	  	  	                	      update dbo.advgal 
set [intrv 2date] = l.[intrv 2date]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[intrv 2date] is not null


	  	  	                	      update dbo.advgal 
set [court visit] = l.[court visit]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[court visit] is not null


	  	  	                	      update dbo.advgal 
set [dateapprecd] = l.dateapprecd
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.dateapprecd is not null


update dbo.child
set grants = c.grantslu
from dbo.child ch 
join dbo.casework c on c.[child id] = ch.[child id]
where c.grantslu is not null


update dbo.child
set ccounty = c.County

from dbo.child ch 
join dbo.casework c on c.[child id] = ch.[child id]
where c.County is not null



update dbo.advgal 
set [Date Sworn] = l.[Date Sworn]
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]
where l.[date sworn] is not null



update dbo.Casework
set [cFamily ID] = ch.[family id]
from dbo.Casework cw
join dbo.Child ch on ch.[Child ID] = cw.[Child ID]

update dbo.li_plc
set [bothchild id] = [child id red]
where [bothchild id] is null

update dbo.li_plc
set [removed from home?] = 'Y'
where [date removed from home] is not null

update dbo.li_adv
set [cFamily ID] = c.[cfamily id]
from dbo.li_adv a
join dbo.casework c  on c.[case id] = a.[case id]
where c.[cfamily id] is not null

update dbo.li_adv
set [cchild ID] = c.[child id]
from dbo.li_adv a
join dbo.casework c  on c.[case id] = a.[case id]
where c.[child id] is not null

update dbo.li_adv
set [cdate closed] = c.[date closed]
from dbo.li_adv a
join dbo.casework c  on c.[case id] = a.[case id]
where c.[date closed] is not null


CREATE FUNCTION dbo.udf_GetNumeric
(@strAlphaNumeric VARCHAR(256))
RETURNS VARCHAR(256)
AS
BEGIN
DECLARE @intAlpha INT
SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric)
BEGIN
WHILE @intAlpha > 0
BEGIN
SET @strAlphaNumeric = STUFF(@strAlphaNumeric, @intAlpha, 1, '' )
SET @intAlpha = PATINDEX('%[^0-9]%', @strAlphaNumeric )
END
END
RETURN ISNULL(@strAlphaNumeric,0)
END
GO



update CasaManagerSource.dbo.LI_SERV
set [cfamily id] = 
 CasaManagerSource.dbo.udf_GetNumeric([cfamily id]) 
from CasaManagerSource.dbo.LI_serv


update dbo.li_serv
set [case id lu] = c.[case id]
from dbo.li_Serv s
join dbo.casework c on c.[child id] = s.[cchild id]
where c.[case id] is not null

update dbo.li_serv
set [familyid_visit] = [cfamily id]

update dbo.li_serv
set [childid_visit] = [cchild id]

update dbo.advgal

set rsvp = l.rsvp
from dbo.advgal a
join dbo.li_train l on l.[vol id] = a.[vol id]


update dbo.li_iep
set [cfamily id] = ch.[family id]
from dbo.LI_IEP l
join dbo.Child ch on ch.[Child ID] = l.[Child ID]

update CasaManagerSource.dbo.LI_prof
set [cChild ID both] = CasaManagerSource.dbo.udf_GetNumeric([cChild ID both]) 
from CasaManagerSource.dbo.LI_Prof


update CasaManagerSource.dbo.LI_SCHOOLS
set [bothchild id] = [child id red] where [bothChild ID] is null


update CasaManagerSource.dbo.LI_health
set [Child IDcalc] = CasaManagerSource.dbo.udf_GetNumeric([Child IDcalc]) 
from CasaManagerSource.dbo.LI_health

:r C:\EvintoClients\InitDB.sql

:r C:\EvintoClients\EyeColor.sql
:r C:\EvintoClients\MaritalStatus.sql
:r C:\EvintoClients\CareerType.sql
:r C:\EvintoClients\County.sql
:r C:\EvintoClients\EmploymentStatus.sql
:r C:\EvintoClients\Ethnicity.sql
:r C:\EvintoClients\Race.sql

:r C:\EvintoClients\PermianBasin\Party_457.sql
:r C:\EvintoClients\VolunteerLookupTables.sql
:r C:\EvintoClients\VolunteerSupervisor.sql
:r C:\EvintoClients\Volunteer.sql
:r C:\EvintoClients\FamilyMember.sql

:r C:\EvintoClients\PermianBasin\AssociatedParty_457.sql
:r C:\EvintoClients\PermianBasin\Employment_457.sql


:r C:\EvintoClients\ApplicantTraining.sql
:r C:\EvintoClients\TrainingActivity_LITRAIN.sql
:r C:\EvintoClients\VolunteerCertifyCheckLog.sql 
:r C:\EvintoClients\BackgroundCheck.sql 
:r C:\EvintoClients\Interview.sql
:r C:\EvintoClients\Education.sql
:r C:\EvintoClients\Language.sql
:r C:\EvintoClients\Abuse.sql
:r C:\EvintoClients\CaseClosureReason.sql


:r C:\EvintoClients\JohnsonTX\CaseInfo_291.sql
:r C:\EvintoClients\Child.sql
:r C:\EvintoClients\HearingType.sql
:r C:\EvintoClients\EligibleStatus.sql
:r C:\EvintoClients\CaseChildStatus.sql

:r C:\EvintoClients\Eligible_new.sql
:r C:\EvintoClients\Disability.sql
:r C:\EvintoClients\ChildReferral.sql

:r C:\EvintoClients\JohnsonTX\CaseChild_291.sql
:r C:\EvintoClients\CaseChildCaseChildStatus.sql
:r C:\EvintoClients\RelationshipType.sql
:r C:\EvintoClients\CaseReleaseReason.sql
:r C:\EvintoClients\CaseVolunteer.sql
:r C:\EvintoClients\CaseVolunteerChild.sql
:r C:\EvintoClients\CaseFamilyMember.sql
:r C:\EvintoClients\CaseFamilyMemberChildRelationship.sql
:r C:\EvintoClients\Concerns.sql
:r C:\EvintoClients\FamilyMemberConcern.sql
:r C:\EvintoClients\HearingLocation.sql
:r C:\EvintoClients\HearingStatus.sql
:r C:\EvintoClients\Judge.sql

:r C:\EvintoClients\Camden\Hearing_398.sql
:r C:\EvintoClients\CrossTimbers\HearingChild_235.sql
:r C:\EvintoClients\CrossTimbers\HearingFamily_235.sql
:r C:\EvintoClients\CrossTimbers\HearingDetail_235.sql
:r C:\EvintoClients\CrossTimbers\HearingOutcome_235.sql

--
:r C:\EvintoClients\CrossTimbers\HearingVolunteerInput_235.sql

:r C:\EvintoClients\JohnsonTX\Petition_291.sql
:r C:\EvintoClients\PetitionChild.sql

:r C:\EvintoClients\VolunteerActivity.sql
:r C:\EvintoClients\VolunteerActivityParty.sql
:r C:\EvintoClients\VolunteerActivityOtherParty.sql
:r C:\EvintoClients\NonCaseVolunteerActivity.sql

:r C:\EvintoClients\Allegation_new.sql
:r C:\EvintoClients\CaseAssociatedParty.sql
:r C:\EvintoClients\PlacementFacility.sql
:r C:\EvintoClients\PlacementHistory.sql
:r C:\EvintoClients\PartyCaseAssociatedParty.sql
:r C:\EvintoClients\SystemUsers.sql
:r C:\EvintoClients\ChildClosingLog.sql
:r C:\EvintoClients\ToDo.sql
:r C:\EvintoClients\Inquiry.sql
:r C:\EvintoClients\Note.sql
:r C:\EvintoClients\School.sql


:r C:\EvintoClients\FinalUpdates.sql
:r C:\EvintoClients\FinalUpdates_CodeTables.sql

:r C:\EvintoClients\ReLinkVolSupervisors.sql




update dbo.hearingvolunteerinput
set VolunteerPartyID = isnull(pv.PartyID,p.partyid) 
from dbo.HearingVolunteerInput hvi
join dbo.hearing h on h.HearingID = hvi.HearingID and h.programID = dbo.fn_programid() and hvi.programID = dbo.fn_programid()
 join [CasaManagerSource].[dbo].ConversionGroupToCase cg on h.CaseID = cg.caseid
 join CasaManagerSource.dbo.Casework cw on cw.[cFamily ID] = cg.[Case ID]
join CasaManagerSource.dbo.LI_CRT L on cw.[case ID] = l.[case ID] and h.HearingDate = convert(varchar(500),l.[Hearing Date])
  join [dbo].Party p on p.FirstName like 'Cynthia%' and p.LastName = 'Warren' and p.PartyType = 7 and  p.programID = dbo.fn_programid()
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)
	  join [dbo].casevolunteer cv on cv.programid = dbo.fn_programid() and cv.assigneddate <= h.HearingDate and (cv.ReleasedDate is null or cv.ReleasedDate >= h.HearingDate)
	  and cv.CaseID = h.CaseID
	  join dbo.party pv on pv.PartyID = cv.PartyID and pv.PartyType = 8 
where ((l.[Casa#Recs Accepted] is not null and l.[Casa#Recs Accepted]> 0) or 
(l.[CASA#RecsMade] is not null and l.[CASA#RecsMade] > 0) or
 (l.[CASA#RecsNeg] is not null and l.[CASA#RecsNeg] >0))

and hvi.volunteerpartyid is null
 



update dbo.hearingvolunteerinput
set VolunteerPartyID = isnull(pv.PartyID,p.partyid) 
from dbo.HearingVolunteerInput hvi
join dbo.hearing h on h.HearingID = hvi.HearingID and h.programID = dbo.fn_programid() and hvi.programID = dbo.fn_programid()
 join [CasaManagerSource].[dbo].ConversionGroupToCase cg on h.CaseID = cg.caseid
 join CasaManagerSource.dbo.Casework cw on cw.[cFamily ID] = cg.[Case ID]
join CasaManagerSource.dbo.LI_CRT L on cw.[case ID] = l.[case ID] and h.HearingDate = convert(varchar(500),l.[Hearing Date])
  join [dbo].Party p on p.FirstName like  'Cynthia%' and p.LastName = 'Warren'  and p.PartyType = 7 and  p.programID = dbo.fn_programid()
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)
	  join [dbo].casevolunteer cv on cv.programid = dbo.fn_programid() and cv.assigneddate <= h.HearingDate and (cv.ReleasedDate is null or cv.ReleasedDate >= h.HearingDate)
	  and cv.CaseID = h.CaseID
	  join dbo.party pv on pv.PartyID = cv.PartyID and pv.PartyType = 7 
where ((l.[Casa#Recs Accepted] is not null and l.[Casa#Recs Accepted]> 0) or 
(l.[CASA#RecsMade] is not null and l.[CASA#RecsMade] > 0) or
 (l.[CASA#RecsNeg] is not null and l.[CASA#RecsNeg] >0))

and hvi.volunteerpartyid is null



update dbo.hearingvolunteerinput
set VolunteerPartyID =  isnull(pv.PartyID,p.partyid) 
from dbo.HearingVolunteerInput hvi
join dbo.hearing h on h.HearingID = hvi.HearingID and h.programID = dbo.fn_programid() and hvi.programID = dbo.fn_programid()
 join [CasaManagerSource].[dbo].ConversionGroupToCase cg on h.CaseID = cg.caseid
 join CasaManagerSource.dbo.Casework cw on cw.[cFamily ID] = cg.[Case ID]
join CasaManagerSource.dbo.LI_CRT L on cw.[case ID] = l.[case ID] and h.HearingDate = convert(varchar(500),l.[Hearing Date])
  join [dbo].Party p on p.FirstName like 'Cynthia%' and p.LastName = 'Warren'  and p.PartyType = 7 and  p.programID = dbo.fn_programid()
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)

	  left join [dbo].casevolunteer cv on cv.programid = dbo.fn_programid() and cv.assigneddate <= h.HearingDate and (cv.ReleasedDate is null or cv.ReleasedDate >= h.HearingDate)
	  and cv.CaseID = h.CaseID
	  left join dbo.party pv on pv.PartyID = cv.PartyID and pv.PartyType in (7,8) 
where ((l.[Casa#Recs Accepted] is not null and l.[Casa#Recs Accepted]> 0) or 
(l.[CASA#RecsMade] is not null and l.[CASA#RecsMade] > 0) or
 (l.[CASA#RecsNeg] is not null and l.[CASA#RecsNeg] >0))

and hvi.volunteerpartyid is null


---------------------------------
insert into dbo.party
(firstname, LastName, programID, State, WorkPhone, partytype, active, AddDate, ModDate, AddUserID, ModUserID, Address, City, Zip, CellPhone, HomePhone, HomeEmail, Note)
select  isnull(Fname,'[Blank]'), isnull(Lname,'[Blank]'), dbo.fn_ProgramID(), left(State,2), WorkPhone, 5, 1, getdate(), getdate(), -1, -1 , left(address,50), left(city,50), Zip, CellPhone,HomePhone, email,
isnull(convert(varchar(max),comment) + char(10),'') + convert(varchar(max),isnull(relationship,''))
from casamanagersource.dbo.LI_PERS
where lname is not null


insert into [dbo].FamilyMember
        ( PartyID ,
          EduationLevelID ,
          EducationTypeID ,
          programID
        )
select distinct
p.PartyID
,null
,null
,dbo.fn_ProgramID()
from casamanagersource.dbo.LI_PERS l 
join dbo.party p on p.programID = dbo.fn_ProgramID() and p.FirstName = convert(varchar(max),isnull(l.Fname,'[Blank]')) and p.LastName = convert(varchar(max),isnull(l.Lname,'[Blank]')) 
and p.PartyType = 5
and isnull(p.Address,'') =convert(varchar(max),isnull(left(l.Address,50),''))  
where p.PartyID not in (Select partyid from dbo.familymember where programid = dbo.fn_ProgramID())


insert into [dbo].CaseFamilyMember
        ( FamilyPartyID ,
          CaseID ,
          RelationshipTypeID ,
          programID
        )
select distinct
p.PartyID
,cc.CaseID
,null
,dbo.fn_ProgramID()
from casamanagersource.dbo.LI_PERS l
join dbo.Party p  on p.programID = dbo.fn_ProgramID() and p.partytype = 5 and p.FirstName = convert(Varchar(max),isnull(l.Fname,'[Blank]')) and p.LastName = convert(varchar(max),isnull(l.Lname,'[Blank]'))
and isnull(p.Address,'') = convert(varchar(max),isnull(left(l.Address,50),'')) 
and isnull(p.city,'') = convert(varchar(max),isnull(left(l.city,50),'')) 
and isnull(p.state,'') = convert(varchar(max),isnull(l.state,'')) 
and isnull(p.zip,'') = convert(varchar(max),isnull(l.zip,'')) 
and isnull(p.HomeEmail,'') = convert(varchar(max),isnull(l.Email,'')) 
and isnull(p.CellPhone,'') = convert(varchar(max),isnull(l.CellPhone,'')) 
and isnull(p.WorkPhone,'') = convert(varchar(max),isnull(l.WorkPhone,'')) 
and isnull(p.HomePhone,'') = convert(varchar(max),isnull(l.HomePhone,'')) 
join CasaManagerSource.dbo.conversiongrouptocase cc on cc.[Case ID] = l.FamilyID

insert into [dbo].RelationshipType
        ( RelationshipTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct convert(varchar(max),Relationship)
,1 active
,getdate() AddDate
,-1 AddUserID
,getdate() ModDate
,-1 ModUserID
,dbo.fn_ProgramID() ProgramID
from CasaManagerSource.dbo.LI_PERS
where convert(varchar(max),Relationship) not in (select RelationshipTypename from dbo.RelationshipType where programID = dbo.fn_ProgramID())








insert into [dbo].CaseFamilyMemberChildRelationship
        ( CaseFamilyMemberID ,
          CaseChildID ,
          RelationshipTypeID ,
          programID
        )

select distinct 
  cfm.CaseFamilyMemberID
, cc.CaseChildID
, rt.RelationshipTypeID
,dbo.fn_ProgramID()
from [CasaManagerSource].[dbo].LI_PERS l
join dbo.Party p  on p.programID = dbo.fn_ProgramID() and p.partytype = 5 and p.FirstName = convert(Varchar(max),isnull(l.Fname,'[Blank]')) and p.LastName = convert(varchar(max),isnull(l.Lname,'[Blank]'))
and isnull(p.Address,'') = convert(varchar(max),isnull(l.Address,'')) 
and isnull(p.city,'') = convert(varchar(max),isnull(l.city,'')) 
and isnull(p.state,'') = convert(varchar(max),isnull(l.state,'')) 
and isnull(p.zip,'') = convert(varchar(max),isnull(l.zip,'')) 
and isnull(p.HomeEmail,'') = convert(varchar(max),isnull(l.Email,'')) 
and isnull(p.CellPhone,'') = convert(varchar(max),isnull(l.CellPhone,'')) 
and isnull(p.WorkPhone,'') = convert(varchar(max),isnull(l.WorkPhone,'')) 
and isnull(p.HomePhone,'') = convert(varchar(max),isnull(l.HomePhone,'')) 
join CasaManagerSource.dbo.conversiongrouptocase cg on cg.[Case ID] = l.FamilyID
join [dbo].CaseChild cc on cc.programID = dbo.fn_ProgramID() and cc.CaseID = cg.CaseID
join [dbo].RelationshipType rt on rt.RelationshipTypeName = convert(varchar(max),l.Relationship) and rt.programID = dbo.fn_ProgramID()
join [dbo].CaseFamilyMember cfm on cfm.CaseID = cc.CaseID and cfm.FamilyPartyID = p.PartyID and cfm.CaseID = cg.CaseID





declare @OtherVolContactType int = (select VolunteerContactTypeID from dbo.VolunteerContactType where programID = dbo.fn_ProgramID() and VolunteerContactTypeDesc = 'Other')
  declare @OtherVolActivityType int = (select VolunteerActivityTypeID from dbo.VolunteerActivityType where programID = dbo.fn_ProgramID() and VolunteerActivityTypeDesc = 'Other')

insert into [dbo].VolunteerActivity
              ( CaseDetailID ,
                ActivityDate ,
                VolunteerActivityTypeID ,
                OutOfCourt ,
                VolunteerContactTypeID ,
                [Hours] ,
                Mileage ,
                Expenses ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                VolunteerPartyID ,
                ActivityStatus ,
                programID,
				Subject
              )


	

select distinct 
       ci.CaseID CaseDetailID
      , isnull(n.[Date], convert(date,'01/01/1900')) ActivityDate
      , isnull(at.VolunteerActivityTypeID,@OtherVolActivityType) VolunteerActivityTypeID 
      ,0  OutOfCourt
     , @OtherVolContactType VolunteerContactTypeID
      ,.25 [Hours]
      ,0 Mileage
      , null Expenses
     ,convert(varchar(5000),n.notes)
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
      , cpp.PartyID VolunteerPartyID
	  , 'A' ActivityStatus
	  ,dbo.fn_ProgramID() ProgramID
	  ,null
from dbo.CaseInfo ci
join CasaManagerSource.dbo.ConversionGroupToCase cgc on cgc.CaseID = ci.CaseID and ci.programID = dbo.fn_ProgramID()
join CasaManagerSource.dbo.LI_NOT n on n.[Family ID] = cgc.[case ID]
and n.[Vol ID] is null
and n.Notes is not null
join dbo.party cpp on cpp.programID  = dbo.fn_ProgramID()
and cpp.FirstName like 'Cynthia%' and cpp.LastName = 'Warren'   and cpp.PartyType = 7
left join [dbo].VolunteerActivityType at on at.VolunteerActivityTypeDesc = left(n.[Discussion Topic],50) and at.programID = dbo.fn_programid()	
where convert(varchar(5000),n.Notes) not in
(select notes from dbo.VolunteerActivity va where va.programID = dbo.fn_ProgramID() and
va.CaseDetailID = ci.CaseID and va.ActivityDate = n.Date
and va.Notes is not null
)




union 




select distinct 
       ci.CaseID CaseDetailID
      , isnull(n.[Date], convert(date,'01/01/1900')) ActivityDate
      , isnull(at.VolunteerActivityTypeID,@OtherVolActivityType) VolunteerActivityTypeID 
      ,0  OutOfCourt
     , @OtherVolContactType VolunteerContactTypeID
      ,.25 [Hours]
      ,0 Mileage
      , null Expenses
,convert(varchar(5000),n.notes)
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
      , cpp.PartyID VolunteerPartyID
	  , 'A' ActivityStatus
	  ,dbo.fn_ProgramID() ProgramID
	  ,null



from dbo.CaseInfo ci
join CasaManagerSource.dbo.ConversionGroupToCase cgc on cgc.CaseID = ci.CaseID and ci.programID = dbo.fn_ProgramID()
join CasaManagerSource.dbo.child ch on ch.[Family ID] = cgc.[case ID]
join CasaManagerSource.dbo.LI_NOT n on n.[child ID] = ch.[child ID]
and n.[Vol ID] is null
and n.Notes is not null
join dbo.party cpp on cpp.programID  = dbo.fn_ProgramID()
and cpp.FirstName like 'Cynthia%' and cpp.LastName = 'Warren'   and cpp.PartyType = 7
left join [dbo].VolunteerActivityType at on at.VolunteerActivityTypeDesc = left(n.[Discussion Topic],50) and at.programID = dbo.fn_programid()	
where convert(varchar(5000),n.Notes) not in
(select notes from dbo.VolunteerActivity va where va.programID = dbo.fn_ProgramID() and
va.CaseDetailID = ci.CaseID and va.ActivityDate = n.Date
and va.Notes is not null
)




insert into [dbo].VolunteerActivity
              ( CaseDetailID ,
                ActivityDate ,
                VolunteerActivityTypeID ,
                OutOfCourt ,
                VolunteerContactTypeID ,
                [Hours] ,
                Mileage ,
                Expenses ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                VolunteerPartyID ,
                ActivityStatus ,
                programID,
				Subject
              )

 select 
 distinct 
       CV.CaseID CaseDetailID
      , isnull(pm.[Log Date], convert(date,'01/01/1900')) ActivityDate
      , 
@OtherVolActivityType
--) 
VolunteerActivityTypeID 
      ,1  OutOfCourt
     ,isnull(ct.VolunteerContactTypeID, @OtherVolContactType) VolunteerContactTypeID
      ,
	  --case
	  --when pm.[Log hrs.] is null then .25
	  --when pm.[Log hrs.] =0  then .25
	  --else 
	  case when isnull((pm.[Log hrs.]+
isnull((select sum(l1.[log hrs.]) 
  from [CasaManagerSource].[dbo].[LI_HRS] l1
  where l1.[type of contact] = pm.[type of contact] and l1.[log activity] = pm.[log activity]
  and l1.[log date] = pm.[log date]
  and l1.[log id] = pm.[log id] and 
   l1.[child id] <> pm.[child id]  
 group by l1.[log id], l1.[type of contact], l1.[log activity], l1.[log date], l1.[vol id], convert(varchar(500),l1.[cLabel.BothName])
),0)),0)> 999.99 then 999.99
else isnull((pm.[Log hrs.]+
isnull((select sum(l1.[log hrs.]) 
  from [CasaManagerSource].[dbo].[LI_HRS] l1
  where l1.[type of contact] = pm.[type of contact] and l1.[log activity] = pm.[log activity]
  and l1.[log date] = pm.[log date]
  and l1.[log id] = pm.[log id] and 
   l1.[child id] <> pm.[child id]  
 group by l1.[log id], l1.[type of contact], l1.[log activity], l1.[log date], l1.[vol id], convert(varchar(500),l1.[cLabel.BothName])
),0)),0)
end 


	 -- [Hours]
      ,case
	  when pm.[Mileage]> 999.99 then 999.99
	  else pm.[Mileage]
	  end Mileage
      , null Expenses
      , case when pm.[Mileage]> 999.99 then 'Actual Mileage: ' + convert(varchar(500),pm.Mileage)
	end Notes 
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
      , p.PartyID VolunteerPartyID
	  , 'A' ActivityStatus
	  ,dbo.fn_ProgramID() ProgramID
,left(pm.[Log Activity],100) 
    from [CasaManagerSource].[dbo].LI_HRS pm
      left join [dbo].VolunteerContactType ct on ct.VolunteerContactTypeDesc = left([Type of Contact],50) and ct.programID = dbo.fn_ProgramID()
      join CasaManagerSource.dbo.Child c on c.[Child ID] = pm.[Child ID]
	-- join [CasaManagerSource].[dbo].ConversionGroupToCase cgc on cgc.[Case ID] = c.[Family ID]
	
join CasaManagerSource.dbo.LI_ADV l on 
	  l.[cChild ID] = pm.[Child ID] 
	  and l.[Date Assigned] < pm.[log Date]
	  and (l.[Date Resigned] is null or l.[Date Resigned] > pm.[log Date])
	  join CasaManagerSource.dbo.conversionpersontoparty cp on cp.[vol id] = l.[Vol ID] 
	 and (cp.type = 'V')
      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype in (8)
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)
	 
	 join CasaManagerSource.dbo.ConversionPersonToParty cpc on cpc.[Vol ID] = pm.[child ID] and cpc.Type = 'C'
	  join dbo.CaseChild cc on cc.ChildPartyID = cpc.PartyID and cc.programID = dbo.fn_ProgramID()
	  join dbo.casevolunteer cv on cv.PartyID = p.partyid and cv.programID = dbo.fn_programid() and cv.CaseID = cc.CaseID
	  join dbo.casevolunteerchild cvc on cvc.programID = dbo.fn_ProgramID() and cvc.CaseChildID = cc.CaseChildID and cvc.CaseVolunteerID = cv.CaseVolunteerID
	  and cvc.AssignedDate < pm.[log Date]
	  and (cvc.ReleasedDate is null or cvc.ReleasedDate > pm.[log Date])







insert into [dbo].VolunteerActivityParty
        ( VolunteerActivityID ,
          PartyID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModeUserID ,
          ProgramID ,
          isSelected
        )


select distinct
   va.VolunteerActivityID
  ,cpc.PartyID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
  ,1
	  from [CasaManagerSource].[dbo].LI_HRS pm
      left join [dbo].VolunteerContactType ct on ct.VolunteerContactTypeDesc = left([Type of Contact],50) and ct.programID = dbo.fn_ProgramID()
      join CasaManagerSource.dbo.Child c on c.[Child ID] = pm.[Child ID]
	
join CasaManagerSource.dbo.LI_ADV l on 
	  l.[cChild ID] = pm.[Child ID] 
	  and l.[Date Assigned] < pm.[log Date]
	  and (l.[Date Resigned] is null or l.[Date Resigned] > pm.[log Date])
	  join CasaManagerSource.dbo.conversionpersontoparty cp on cp.[vol id] = l.[Vol ID] 
	 and (cp.type = 'V')
      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype in (8)
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)
	 
	 join CasaManagerSource.dbo.ConversionPersonToParty cpc on cpc.[Vol ID] = pm.[child ID] and cpc.Type = 'C'
	  join dbo.CaseChild cc on cc.ChildPartyID = cpc.PartyID and cc.programID = dbo.fn_ProgramID()
	  join dbo.casevolunteer cv on cv.PartyID = p.partyid and cv.programID = dbo.fn_programid() and cv.CaseID = cc.CaseID
	  join dbo.casevolunteerchild cvc on cvc.programID = dbo.fn_ProgramID() and cvc.CaseChildID = cc.CaseChildID and cvc.CaseVolunteerID = cv.CaseVolunteerID
	  and cvc.AssignedDate < pm.[log Date]
	  and (cvc.ReleasedDate is null or cvc.ReleasedDate > pm.[log Date])


      join dbo.VolunteerActivity va on va.CaseDetailID = cv.CaseID and va.VolunteerPartyID = p.PartyID
	 and va.ActivityDate = isnull(pm.[Log Date], convert(date,'01/01/1900'))
     and va.VolunteerActivityTypeID = 
@OtherVolActivityType
and va.Subject =  left(pm.[Log Activity],100) 
	and va.VolunteerContactTypeID = isnull(ct.VolunteerContactTypeID, @OtherVolContactType)
	   and va.OutOfCourt = 1
	






declare @OtherVolActivityType int = (select VolunteerActivityTypeID from dbo.VolunteerActivityType where programID = dbo.fn_programid() and VolunteerActivityTypeDesc = 'Other')
declare @OtherVolContactType int = (select VolunteerContactTypeID from dbo.VolunteerContactType where programID = dbo.fn_programid() and VolunteerContactTypeDesc = 'Other')
  

    insert into [dbo].VolunteerActivity
              ( CaseDetailID ,
                ActivityDate ,
                VolunteerActivityTypeID ,
                OutOfCourt ,
                VolunteerContactTypeID ,
                [Hours] ,
                Mileage ,
                Expenses ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                VolunteerPartyID ,
                ActivityStatus ,
                programID,
				Subject
              )



 select 
 distinct 
       cc.CaseID
	   --CV.CaseID CaseDetailID
      , isnull(pm.[Log Date], convert(date,'01/01/1900')) ActivityDate
      ,@OtherVolActivityType VolunteerActivityTypeID 
      ,1  OutOfCourt
     ,isnull(ct.VolunteerContactTypeID, @OtherVolContactType) VolunteerContactTypeID
      ,
	  --case
	  --when pm.[Log hrs.] is null then .25
	  --when pm.[Log hrs.] =0  then .25
	  --else 

	  case when isnull((pm.[Log hrs.]+
isnull((select sum(l1.[log hrs.]) 
  from [CasaManagerSource].[dbo].[LI_HRS] l1
  where l1.[type of contact] = pm.[type of contact] and l1.[log activity] = pm.[log activity]
  and l1.[log date] = pm.[log date]
  and l1.[log id] = pm.[log id] and 
   l1.[child id] <> pm.[child id]  
 group by l1.[log id], l1.[type of contact], l1.[log activity], l1.[log date], l1.[vol id], convert(varchar(500),l1.[cLabel.BothName])
),0)),0)> 999.99 then 999.99
else isnull((pm.[Log hrs.]+
isnull((select sum(l1.[log hrs.]) 
  from [CasaManagerSource].[dbo].[LI_HRS] l1
  where l1.[type of contact] = pm.[type of contact] and l1.[log activity] = pm.[log activity]
  and l1.[log date] = pm.[log date]
  and l1.[log id] = pm.[log id] and 
   l1.[child id] <> pm.[child id]  
 group by l1.[log id], l1.[type of contact], l1.[log activity], l1.[log date], l1.[vol id], convert(varchar(500),l1.[cLabel.BothName])
),0)),0)
end 


	 -- [Hours]


      ,case
	  when pm.[Mileage]> 999.99 then 999.99
	  else pm.[Mileage]
	  end Mileage
      , null Expenses
      , case when pm.[Mileage]> 999.99 then 'Actual Mileage: ' + convert(varchar(500),pm.Mileage)
	end Notes 
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
    ,p.partyid
	  , 'A' ActivityStatus
	  ,dbo.fn_programid() ProgramID
,left(pm.[Log Activity],100) 
from [CasaManagerSource].[dbo].LI_HRS pm

      left join [dbo].VolunteerContactType ct on ct.VolunteerContactTypeDesc = left([Type of Contact],50) and ct.programID = dbo.fn_programid()
      join CasaManagerSource.dbo.Child c on c.[Child ID] = pm.[Child ID]
	   join [dbo].Party p on p.FirstName like 'Cynthia%' and p.LastName = 'Warren' and p.PartyType = 7 and  p.programID = dbo.fn_programid()
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)
	 
   
	 join CasaManagerSource.dbo.ConversionPersonToParty cpc on cpc.[Vol ID] = pm.[child ID] and cpc.Type = 'C'
join dbo.party pc on pc.partyid = cpc.partyid
	  join dbo.CaseChild cc on cc.ChildPartyID = cpc.PartyID and cc.programID = dbo.fn_programid()
join dbo.caseinfo ci on ci.caseid = cc.caseid
	  
	  where 
	  pm.[vol id] is null and 
	  
	  not exists (select volunteeractivityid from dbo.volunteeractivity where programid =dbo.fn_programid()
and CaseDetailID= cc.CaseID and ActivityDate = isnull(pm.[Log Date], convert(date,'01/01/1900'))
and VolunteerContactTypeID = isnull(ct.VolunteerContactTypeID, @OtherVolContactType)
and hours = isnull((pm.[Log hrs.]+
isnull((select sum(l1.[log hrs.]) 
  from [CasaManagerSource].[dbo].[LI_HRS] l1
  where l1.[type of contact] = pm.[type of contact] and l1.[log activity] = pm.[log activity]
  and l1.[log date] = pm.[log date]
  and l1.[log id] = pm.[log id] and 
   l1.[child id] <> pm.[child id]  
 group by l1.[log id], l1.[type of contact], l1.[log activity], l1.[log date], l1.[vol id], convert(varchar(500),l1.[cLabel.BothName])
),0)),0)

)


and 
	  
	  not exists (select volunteeractivityid from dbo.volunteeractivity where programid =dbo.fn_programid()
and CaseDetailID= cc.CaseID and ActivityDate = isnull(pm.[Log Date], convert(date,'01/01/1900'))
and VolunteerContactTypeID = isnull(ct.VolunteerContactTypeID, @OtherVolContactType)
and hours = isnull(pm.[Log hrs.],0)

)





