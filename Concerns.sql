
--Concerns

insert into [dbo].Concerns
        ( ConcernsDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sFamilyConcernDesc,1,getdate(),-1,getdate(),-1,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblFamilyConcern]
where len(sFamilyConcernDesc)>0
and sFamilyConcernDesc not in (select ConcernsDesc  from dbo.Concerns where programid = dbo.fn_ProgramID())
order by sFamilyConcernDesc