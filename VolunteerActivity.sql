

--VolunteerActivityType


insert into [dbo].VolunteerActivityType
        ( VolunteerActivityTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sActivityTypeDesc vat, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblActivityType]
where len(sActivityTypeDesc) > 0
and sActivityTypeDesc  not in (select VolunteerActivityTypeDesc  from [dbo].VolunteerActivityType where programid = dbo.fn_programid())
--union
--select distinct sVolunteerActivityDesc vat, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
--from [CometSource].[dbo].[_tblVolunteerActivity]
--where len(sVolunteerActivityDesc) > 0
--order by vat

union 

select 'Other', 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
where 'Other' not in (select VolunteerActivityTypedesc from dbo.VolunteerActivityType where programid = dbo.fn_ProgramID())








--VolunteerContactType

with t as (
  select distinct sContactTypeDesc
  from [CometSource].[dbo].[_tblContactType]
  where len(sContactTypeDesc) > 0

  union

  select distinct sContactType sContactTypeDesc
  from [CometSource].[dbo].tblRelationshipShipVolunteerActivity
  where len(sContactType) > 0


)


insert into [dbo].VolunteerContactType
        ( VolunteerContactTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
  select distinct sContactTypeDesc, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
  from t
where sContactTypeDesc not in (select VolunteerContactTypeDesc from dbo.VolunteerContacTType where programid = dbo.fn_programid())
  order by sContactTypeDesc

union 

select distinct 'Other', 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
where 'Other' not in (select VolunteerContactTypedesc from dbo.VolunteerContactType where programid = dbo.fn_ProgramID())


---------------------------------------------------------------------------------
--VOLUNTEER ACTIVITY
--Dependencies: VolunteerActivityType, VolunteerContactType
---------------------------------------------------------------------------------

/****** Object:  Table [dbo].[ConversionPersonToParty]    Script Date: 03/12/2012 22:41:26 ******/
IF  EXISTS (SELECT * FROM CometSource.sys.objects where name like '%ConversionVolunteerActivity%')
DROP TABLE [CometSource].[dbo].ConversionVolunteerActivity
GO

/****** Object:  Table [dbo].[ConversionVolunteerActivity]    Script Date: 03/12/2012 22:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CometSource].[dbo].ConversionVolunteerActivity(
	[nVolunteerActivityID] [int] NOT NULL,
	[VolunteerActivityID] [int] NOT NULL,
	[Source] varchar(50) NULL
) ON [PRIMARY]

GO



--CREATE NONCLUSTERED INDEX [IDX_VolunteerActivityTypeDesc] ON [dbo].[VolunteerActivityType]
--(
--	[VolunteerActivityTypeDesc] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]





 
  declare @VolunteerActivityID int;
  declare @nVolunteerActivityID int;
  declare @CaseDetailID int;
  declare @ActivityDate date;
  declare @VolunteerActivityTypeID int;
  declare @OutOfCourt bit;
  declare @VolunteerContactTypeID int;
  declare @Hours decimal(5,2);
  declare @Mileage decimal(5,2);
  declare @Expenses smallmoney;
  declare @Notes varchar(5000);
  declare @AddDate smalldatetime;
  declare @AddUserID int;
  declare @ModDate smalldatetime;
  declare @ModUserID int;
  declare @VolunteerPartyID int;
  declare @ActivityStatus char(1);
  declare @FamilMemberID int; 
  declare @ProgramID int;

  set @ProgramID = dbo.fn_ProgramID()

  declare @OtherVolContactType int = (select VolunteerContactTypeID from dbo.VolunteerContactType where programID = dbo.fn_ProgramID() and VolunteerContactTypeDesc = 'Other')
  declare @OtherVolActivityType int = (select VolunteerActivityTypeID from dbo.VolunteerActivityType where programID = dbo.fn_ProgramID() and VolunteerActivityTypeDesc = 'Other')


   declare crs cursor for
      select
        va.nVolunteerActivityID
      , cgc.CaseID CaseDetailID
      --, ci.CaseID CaseDetailID
      , case when isdate(convert(datetime,va.dContact)) = 1 then va.dContact end ActivityDate
      , isnull(coalesce(at.VolunteerActivityTypeID,atOther.VolunteerActivityTypeID),@OtherVolActivityType) VolunteerActivityTypeID
      , va.fOOC OutOfCourt
      , isnull(coalesce(ct.VolunteerContactTypeID,ctOther.VolunteerContactTypeID),@OtherVolContactType) VolunteerContactTypeID
      , case when va.nHours < 0 then 0 when va.nHours > 999 then 999 else va.nHours end [Hours]
      , case when va.nMileage < 0 then 0 when va.nMileage > 999 then 999 else va.nMileage end Mileage
      , null Expenses
      , null Notes 
      --This was split out to VolunteerActivityParty
      --ltrim(rtrim(
      --    coalesce('Contacted ID: ' + convert(varchar,cp.PartyID) + ', Name: ' + p.FirstName + ' ' + p.LastName,'')
      --  + coalesce(' Hearing: ' + convert(varchar,month(h.HearingDate)) + '/' + convert(varchar,day(h.HearingDate)) + '/' + convert(varchar,year(h.HearingDate)) + ' ' + h.DocketNumber,'')
      --  ))
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
      , p.PartyID VolunteerPartyID
      , 'A' ActivityStatus
      ,@ProgramID ProgramID
      --,null--cp.PartyID
      from [CometSource].[dbo].tblRelationshipShipVolunteerActivity va
      left join [dbo].VolunteerActivityType at on at.VolunteerActivityTypeDesc = va.sActivityType and at.programID = dbo.fn_ProgramID()
      left join [dbo].VolunteerActivityType atOther on atOther.VolunteerActivityTypeDesc = 'Other' and atOther.programID = dbo.fn_ProgramID()
      left join [dbo].VolunteerContactType ct on ct.VolunteerContactTypeDesc = va.sContactType and ct.programID = dbo.fn_ProgramID()
      left join [dbo].VolunteerContactType ctOther on ctOther.VolunteerContactTypeDesc = 'Other' and ctOther.programID = dbo.fn_ProgramID()
      --join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = va.nHearingID
      --left join [dbo].Hearing h on h.HearingID = hh.HearingID and h.programID = dbo.fn_ProgramID()
      --left join [dbo].CaseInfo ci on ci.CaseID = h.CaseID and ci.programID = dbo.fn_ProgramID()
      join [CometSource].[dbo].tblPersonRelationship pr on pr.nPersonRelationshipID = va.nPersonRelationshipID
      join [CometSource].[dbo].tblPersonChild pc on pc.nPersonID = pr.nPersonID
      join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pc.nGroupID
      join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = pr.nRelationshipID
      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID)
      where isdate(convert(datetime,va.dContact)) = 1
      and va.nHours is not null
    ;

  open crs;
  fetch crs
   into
     @nVolunteerActivityID
    ,@CaseDetailID
    ,@ActivityDate
    ,@VolunteerActivityTypeID
    ,@OutOfCourt
    ,@VolunteerContactTypeID
    ,@Hours
    ,@Mileage
    ,@Expenses
    ,@Notes
    ,@AddDate
    ,@AddUserID
    ,@ModDate
    ,@ModUserID
    ,@VolunteerPartyID
    ,@ActivityStatus
    ,@ProgramID
   
   while @@FETCH_STATUS = 0
   begin
      insert into [dbo].VolunteerActivity
              ( CaseDetailID ,
                ActivityDate ,
                VolunteerActivityTypeID ,
                OutOfCourt ,
                VolunteerContactTypeID ,
                [Hours] ,
                Mileage ,
                Expenses ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                VolunteerPartyID ,
                ActivityStatus ,
                programID,
				Subject
                --FamilyMemberID 
              )
      values  ( 
           @CaseDetailID
          ,@ActivityDate
          ,@VolunteerActivityTypeID
          ,@OutOfCourt
          ,@VolunteerContactTypeID
          ,@Hours
          ,@Mileage
          ,@Expenses
          ,@Notes
          ,getdate()
          ,-1
          ,getdate()
          ,-1
          ,@VolunteerPartyID
          ,@ActivityStatus
          ,@ProgramID
		  ,null
      )
           
      set @VolunteerActivityID = SCOPE_IDENTITY();
      
      insert into [CometSource].[dbo].[ConversionVolunteerActivity]
              ( [nVolunteerActivityID], [VolunteerActivityID], [Source] )
      values  ( @nVolunteerActivityID ,@VolunteerActivityID, 'RelationshipVolActivity' );

      --print 'ID = ' + convert(varchar, @PersonID) + '  New ID = ' + convert(varchar, @PartyID);
   
      fetch crs
       into
         @nVolunteerActivityID
        ,@CaseDetailID
        ,@ActivityDate
        ,@VolunteerActivityTypeID
        ,@OutOfCourt
        ,@VolunteerContactTypeID
        ,@Hours
        ,@Mileage
        ,@Expenses
        ,@Notes
        ,@AddDate
        ,@AddUserID
        ,@ModDate
        ,@ModUserID
        ,@VolunteerPartyID
        ,@ActivityStatus
        ,@ProgramID
   end
   
   close crs;
   deallocate crs;

go



-------------------------------------------------------------------
--FROM Meeting Tables---
--------------------------------





--VolunteerActivityType


insert into [dbo].VolunteerActivityType
        ( VolunteerActivityTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
sMeetingType vat, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblMeetingType]
where len(sMeetingType) > 0
and sMeetingType not in 
(select VolunteerActivityTypeDesc from dbo.VolunteerActivityTYpe where programid = dbo.fn_programid()) 


insert into [dbo].VolunteerActivityType
        ( VolunteerActivityTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
'Meeting', 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
where 'Meeting' not in 
(select VolunteerActivityTypeDesc from dbo.VolunteerActivityTYpe where programid = dbo.fn_programid())


--Add any vales that appear in tblPersonMeeting but not in _tblMeetingType
insert into [dbo].VolunteerActivityType
        ( VolunteerActivityTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sMeetingType vat, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from [CometSource].[dbo].[tblPersonMeeting]
where len(sMeetingType) > 0
and sMeetingType not in 
(select VolunteerActivityTypeDesc from dbo.VolunteerActivityTYpe where programid = dbo.fn_programid())
;



insert into [dbo].VolunteerContactType
        ( VolunteerContactTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
  select 'Meeting', 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
where 'Meeting' not in (select VolunteerContactTypeDesc from dbo.VolunteerContactType where programid = dbo.fn_programid())
 




---------------------------------------------------------------------------------
--VOLUNTEER ACTIVITY
--Dependencies: VolunteerActivityType, VolunteerContactType
---------------------------------------------------------------------------------
 
 declare @ContactType int; set @ContactType= (select VolunteerContactTypeID from dbo.volunteercontacttype
 where VolunteerContactTypeDesc = 'Meeting' and programid = dbo.fn_programid());

  declare @DefaultVolActivityType int; set @DefaultVolActivityType= (select VolunteerActivityTypeID from dbo.volunteeractivitytype
 where VolunteerActivityTypeDesc = 'Meeting' and programid = dbo.fn_programid());


  declare @VolunteerActivityID int;
  declare @nVolunteerActivityID int;
  declare @CaseDetailID int;
  declare @ActivityDate date;
  declare @VolunteerActivityTypeID int;
  declare @OutOfCourt bit;
  declare @VolunteerContactTypeID int;
  declare @Hours decimal(5,2);
  declare @Mileage decimal(5,2);
  declare @Expenses smallmoney;
  declare @Notes varchar(5000);
  declare @AddDate smalldatetime;
  declare @AddUserID int;
  declare @ModDate smalldatetime;
  declare @ModUserID int;
  declare @VolunteerPartyID int;
  declare @ActivityStatus char(1);
  declare @FamilMemberID int; 
  declare @ProgramID int;

  set @ProgramID = dbo.fn_ProgramID()

   declare crs cursor for
      select
        pm.nPersonMeetingID
      , cgc.CaseID CaseDetailID
      , case when isdate(convert(datetime,pm.dMeetingDate)) = 1 then pm.dMeetingDate end ActivityDate
      , isnull(at.VolunteerActivityTypeID, @DefaultVolActivityType) VolunteerActivityTypeID
      ,1  OutOfCourt
      ,@ContactType VolunteerContactTypeID
      ,1 [Hours]
      ,null Mileage
      , null Expenses
      , convert(varchar(5000),pm.sNote) Notes 
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
      , p.PartyID VolunteerPartyID
	  , 'A' ActivityStatus
	  ,@ProgramID ProgramID
      from [CometSource].[dbo].tblPersonMeeting pm
      left join [dbo].VolunteerActivityType at on at.VolunteerActivityTypeDesc = pm.sMeetingType and at.programID = dbo.fn_ProgramID()
      join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pm.nGroupID
	  join [CometSource].[dbo].tblMeetingInvolvedPerson I on I.nPersonMeetingID = pm.nPersonMeetingID 
     join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = i.nPersonID
      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype in (7,8)
      and exists (select 1 from [dbo].Volunteer v where v.PartyID = p.PartyID);
	  
  open crs;
  fetch crs
   into
     @nVolunteerActivityID
    ,@CaseDetailID
    ,@ActivityDate
    ,@VolunteerActivityTypeID
    ,@OutOfCourt
    ,@VolunteerContactTypeID
    ,@Hours
    ,@Mileage
    ,@Expenses
    ,@Notes
    ,@AddDate
    ,@AddUserID
    ,@ModDate
    ,@ModUserID
    ,@VolunteerPartyID
    ,@ActivityStatus
    ,@ProgramID
   
   while @@FETCH_STATUS = 0
   begin
      insert into [dbo].VolunteerActivity
              ( CaseDetailID ,
                ActivityDate ,
                VolunteerActivityTypeID ,
                OutOfCourt ,
                VolunteerContactTypeID ,
                [Hours] ,
                Mileage ,
                Expenses ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                VolunteerPartyID ,
                ActivityStatus ,
                programID,
				Subject
              )
      values  ( 
           @CaseDetailID
          ,@ActivityDate
          ,@VolunteerActivityTypeID
          ,@OutOfCourt
          ,@VolunteerContactTypeID
          ,@Hours
          ,@Mileage
          ,@Expenses
          ,@Notes
          ,getdate()
          ,-1
          ,getdate()
          ,-1
          ,@VolunteerPartyID
          ,@ActivityStatus
          ,@ProgramID
		  ,null
      )
           
      set @VolunteerActivityID = SCOPE_IDENTITY();
      
      insert into [CometSource].[dbo].[ConversionVolunteerActivity]
              ( [nVolunteerActivityID], [VolunteerActivityID], [Source] )
      values  ( @nVolunteerActivityID ,@VolunteerActivityID, 'PersonMeeting' );

      fetch crs
       into
         @nVolunteerActivityID
        ,@CaseDetailID
        ,@ActivityDate
        ,@VolunteerActivityTypeID
        ,@OutOfCourt
        ,@VolunteerContactTypeID
        ,@Hours
        ,@Mileage
        ,@Expenses
        ,@Notes
        ,@AddDate
        ,@AddUserID
        ,@ModDate
        ,@ModUserID
        ,@VolunteerPartyID
        ,@ActivityStatus
        ,@ProgramID
   end
   
   close crs;
   deallocate crs;

go




---------------------



--tblMeeting Not Volunteer
---------------------------------------------------------------------------------
--VOLUNTEER ACTIVITY
--Dependencies: VolunteerActivityType, VolunteerContactType
---------------------------------------------------------------------------------
 
 declare @ContactType int; set @ContactType= (select VolunteerContactTypeID from dbo.volunteercontacttype
 where VolunteerContactTypeDesc = 'Meeting' and programid = dbo.fn_programid());

  declare @DefaultVolActivityType int; set @DefaultVolActivityType= (select VolunteerActivityTypeID from dbo.volunteeractivitytype
 where VolunteerActivityTypeDesc = 'Meeting' and programid = dbo.fn_programid());


  declare @VolunteerActivityID int;
  declare @nVolunteerActivityID int;
  declare @CaseDetailID int;
  declare @ActivityDate date;
  declare @VolunteerActivityTypeID int;
  declare @OutOfCourt bit;
  declare @VolunteerContactTypeID int;
  declare @Hours decimal(5,2);
  declare @Mileage decimal(5,2);
  declare @Expenses smallmoney;
  declare @Notes varchar(5000);
  declare @AddDate smalldatetime;
  declare @AddUserID int;
  declare @ModDate smalldatetime;
  declare @ModUserID int;
  declare @VolunteerPartyID int;
  declare @ActivityStatus char(1);
  declare @FamilMemberID int; 
  declare @ProgramID int;

  set @ProgramID = dbo.fn_ProgramID()
  
  declare crs cursor for


  -- select distinct 
  --      pm.nPersonMeetingID
  --    , cgc.CaseID CaseDetailID
  --    , case when isdate(convert(datetime,pm.dMeetingDate)) = 1 then pm.dMeetingDate end ActivityDate
  --    , isnull(at.VolunteerActivityTypeID, @DefaultVolActivityType) VolunteerActivityTypeID
  --    ,1  OutOfCourt
  --    ,@ContactType VolunteerContactTypeID
  --    ,1 [Hours]
  --    ,null Mileage
  --    , null Expenses
  --    , convert(varchar(5000),pm.sNote) Notes 
  --    , getdate() AddDate
  --    , -1 AddUserID
  --    , getdate() ModDate
  --    , -1 ModUserID
  --    , (select top 1 VolunteerPartyID from dbo.VolunteerActivity where CaseDetailID = cgc.CaseID
	 -- and programID = dbo.fn_ProgramID()
	 -- and VolunteerPartyID <> p.PartyID and ActivityDate = pm.dMeetingDate
	 -- )
	 --  VolunteerPartyID
	 -- , 'A' ActivityStatus
	 -- ,@ProgramID ProgramID 
  --    from [CometSource].[dbo].tblPersonMeeting pm
  --    left join [dbo].VolunteerActivityType at on at.VolunteerActivityTypeDesc = pm.sMeetingType and at.programID = dbo.fn_ProgramID()
  --    join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pm.nGroupID
	 --join [CometSource].[dbo].tblMeetingInvolvedPerson I on I.nPersonMeetingID = pm.nPersonMeetingID 
  --   join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = i.nPersonID
  --    join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	 -- and p.partytype not in (7,8)
	 -- where exists (select VolunteerActivityID from dbo.VolunteerActivity where CaseDetailID = cgc.CaseID
	 -- and programID = dbo.fn_ProgramID()
	 -- and VolunteerPartyID <> p.PartyID and ActivityDate = pm.dMeetingDate
	 -- ) 
	 -- union 

      select distinct 
        pm.nPersonMeetingID
      , cgc.CaseID CaseDetailID
      , case when isdate(convert(datetime,pm.dMeetingDate)) = 1 then pm.dMeetingDate end ActivityDate
      , isnull(at.VolunteerActivityTypeID, @DefaultVolActivityType) VolunteerActivityTypeID
      ,1  OutOfCourt
      ,@ContactType VolunteerContactTypeID
      ,1 [Hours]
      ,null Mileage
      , null Expenses
      , convert(varchar(5000),pm.sNote) Notes 
      , getdate() AddDate
      , -1 AddUserID
      , getdate() ModDate
      , -1 ModUserID
      , (select top 1 PartyID from dbo.CaseVolunteer where CaseID = cgc.CaseID and programID = dbo.fn_ProgramID()
	  order by CaseSupervisor desc, AssignedDate desc) VolunteerPartyID
	  , 'A' ActivityStatus
	  ,@ProgramID ProgramID 
      from [CometSource].[dbo].tblPersonMeeting pm
      left join [dbo].VolunteerActivityType at on at.VolunteerActivityTypeDesc = pm.sMeetingType and at.programID = dbo.fn_ProgramID()
      join [CometSource].[dbo].ConversionGroupToCase cgc on cgc.nGroupID = pm.nGroupID
	 join [CometSource].[dbo].tblMeetingInvolvedPerson I on I.nPersonMeetingID = pm.nPersonMeetingID 
     join [CometSource].[dbo].ConversionPersonToParty cp on cp.PersonID = i.nPersonID
      join [dbo].Party p on p.PartyID = cp.PartyID and p.programID = dbo.fn_ProgramID() 
	  and p.partytype not in (7,8)
	  where not exists (select VolunteerActivityID from dbo.VolunteerActivity where CaseDetailID = cgc.CaseID
	  and programID = dbo.fn_ProgramID() and ActivityDate = pm.dMeetingDate)
	
  

  open crs;
  fetch crs
   into
     @nVolunteerActivityID
    ,@CaseDetailID
    ,@ActivityDate
    ,@VolunteerActivityTypeID
    ,@OutOfCourt
    ,@VolunteerContactTypeID
    ,@Hours
    ,@Mileage
    ,@Expenses
    ,@Notes
    ,@AddDate
    ,@AddUserID
    ,@ModDate
    ,@ModUserID
    ,@VolunteerPartyID
    ,@ActivityStatus
    ,@ProgramID
   
   while @@FETCH_STATUS = 0
   begin
      insert into [dbo].VolunteerActivity
              ( CaseDetailID ,
                ActivityDate ,
                VolunteerActivityTypeID ,
                OutOfCourt ,
                VolunteerContactTypeID ,
                [Hours] ,
                Mileage ,
                Expenses ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                VolunteerPartyID ,
                ActivityStatus ,
                programID,
				Subject
              )
      values  ( 
           @CaseDetailID
          ,@ActivityDate
          ,@VolunteerActivityTypeID
          ,@OutOfCourt
          ,@VolunteerContactTypeID
          ,@Hours
          ,@Mileage
          ,@Expenses
          ,@Notes
          ,getdate()
          ,-1
          ,getdate()
          ,-1
          ,@VolunteerPartyID
          ,@ActivityStatus
          ,@ProgramID
		  ,null
      )
           
      set @VolunteerActivityID = SCOPE_IDENTITY();
      
      insert into [CometSource].[dbo].[ConversionVolunteerActivity]
              ( [nVolunteerActivityID], [VolunteerActivityID], [Source] )
      values  ( @nVolunteerActivityID ,@VolunteerActivityID, 'PersonMeeting_NoVolActivity' );

      fetch crs
       into
         @nVolunteerActivityID
        ,@CaseDetailID
        ,@ActivityDate
        ,@VolunteerActivityTypeID
        ,@OutOfCourt
        ,@VolunteerContactTypeID
        ,@Hours
        ,@Mileage
        ,@Expenses
        ,@Notes
        ,@AddDate
        ,@AddUserID
        ,@ModDate
        ,@ModUserID
        ,@VolunteerPartyID
        ,@ActivityStatus
        ,@ProgramID
   end
   
   close crs;
   deallocate crs;

go



--DROP INDEX [IDX_VolunteerActivityTypeDesc] ON [dbo].[VolunteerActivityType]



