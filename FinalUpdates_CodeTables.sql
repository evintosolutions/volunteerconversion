



----------------------------------------------------------------------------
--Issue 1495

insert into dbo.DocumentType
([DocTypeDesc]
,AreaID
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [DOCTYPEDESC]
	  ,AreaID
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[DOCUMENTTYPE] D
  where programID = 110 and Active= 1 
  and 
  --DocTypeDesc not in 
  (select top 1 DocTypeDesc from [192-168-0-5].volunteer.dbo.DOCUMENTTYPE where programID  = dbo.fn_ProgramID()
  ) is null

 


insert into dbo.EligibleStatus
([EligibleStatusDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [ELIGIBLESTATUSDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[ELIGIBLESTATUS]
  where programID = 110 and Active= 1 and 
  --ELIGIBLESTATUSDesc not in 
  (select top 1 ELIGIBLESTATUSDesc from [192-168-0-5].volunteer.dbo.ELIGIBLESTATUS where programID  = dbo.fn_ProgramID())
  is null


  



insert into dbo.DEGREE
(DegreeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      DEGREEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].DEGREE
  where programID = 110 and Active= 1 and 
  --DegreeDesc not in 
  (select top 1 DegreeDesc from[192-168-0-5].volunteer.dbo.DEGREE where programID  = dbo.fn_ProgramID())
  is null 
  




insert into dbo.MARITALSTATUS
(MARITALSTATUSDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      MARITALSTATUSDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].MARITALSTATUS
  where programID = 110 and Active= 1 and 
  --MARITALSTATUSDesc not in 
  (select top 1 MARITALSTATUSDesc from[192-168-0-5].volunteer.dbo.MARITALSTATUS where programID  = dbo.fn_ProgramID())
  is null



  

insert into dbo.TODOTASKCATEGORY
(TODOTASKCATEGORYDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKCATEGORYDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKCATEGORY
  where programID = 110 and Active= 1 and 
  --TODOTASKCATEGORYDesc not in 
  (select top 1 TODOTASKCATEGORYDesc from [192-168-0-5].volunteer.dbo.TODOTASKCATEGORY where programID  = dbo.fn_ProgramID())
  is null




insert into dbo.ToDoTaskServiceType
(  ToDoTaskServiceTypeDesc
	  ,ToDoTaskCategoryID
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      ToDoTaskServiceTypeDesc
	  ,TC2.ToDoTaskCategoryID
      ,TS.[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKSERVICETYPE TS
  JOIN [192.168.0.2].volunteer.[dbo].TODOTASKCATEGORY TC ON TC.ToDoTaskCategoryID = TS.ToDoTaskCategoryID AND TC.ProgramID = 110
  JOIN[192-168-0-5].volunteer.[dbo].TODOTASKCATEGORY TC2 ON TC2.ToDoTaskCategoryDesc = TC.ToDoTaskCategoryDesc AND TC2.ProgramID = DBO.fn_ProgramID()
  where TS.ProgramID = 110 and TS.Active= 1 
  and 
  --TODOTASKSERVICETYPEDesc not in 
  (select top 1 TODOTASKSERVICETYPEDesc from[192-168-0-5].volunteer.dbo.TODOTASKSERVICETYPE where programID  = dbo.fn_ProgramID())
  is null




insert into dbo.CAREERTYPE
(CAREERTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      CAREERTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CAREERTYPE
  where programID = 110 and Active= 1 and 
  --CAREERTYPEDesc not in 
  (select top 1 CAREERTYPEDesc from[192-168-0-5].volunteer.dbo.CAREERTYPE where programID  = dbo.fn_ProgramID())
  is null


INSERT INTO [dbo].[ApplicationProcessConfig]
           ([AppIntro]
           ,[DisplayFamily]
           ,[DisplayPriorEmployment]
           ,[VehicleIntro]
           ,[DisplayVehicleQuestions]
           ,[BackgroundIntro]
           ,[CriminalIntro]
           ,[ReferenceIntro]
           ,[DisplayReferenceCt]
           ,[AgreementIntro]
           ,[ReleaseIntro]
           
           ,[ReferenceSurveySTatusId]
           ,[CertifiedStatusID]
           ,[Displaybirthplace]
           ,[Displayheightandweight]
           ,[DisplaySSN]
           ,[DisplaySpouse]
           ,[DisplayChildren]
           ,[FamilyInto]
           ,[ExperienceInto]
           ,[VolunteerExperienceInto]
           ,[Thankyoutext]
           ,[DisplayEmploymentDetail]
           ,[RequireEmailAddress]
           ,[RequireAddress]
           ,[EmploymentIntro]
		   ,[AddDate]
           ,[AddUserID]
           ,[ModDate]
           ,[ModUserID]
		   ,programID)
SELECT
      [AppIntro]
           ,[DisplayFamily]
           ,[DisplayPriorEmployment]
           ,[VehicleIntro]
           ,[DisplayVehicleQuestions]
           ,[BackgroundIntro]
           ,[CriminalIntro]
           ,[ReferenceIntro]
           ,[DisplayReferenceCt]
           ,[AgreementIntro]
           ,[ReleaseIntro]
           ,[ReferenceSurveySTatusId]
           ,[CertifiedStatusID]
           ,[Displaybirthplace]
           ,[Displayheightandweight]
           ,[DisplaySSN]
           ,[DisplaySpouse]
           ,[DisplayChildren]
           ,[FamilyInto]
           ,[ExperienceInto]
           ,[VolunteerExperienceInto]
           ,[Thankyoutext]
           ,[DisplayEmploymentDetail]
           ,[RequireEmailAddress]
           ,[RequireAddress]
           ,[EmploymentIntro]
      ,GETDATE()
      ,-1
      ,GETDATE()
	  ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].applicationprocessconfig
  where programID = 110 and 
  --appintro not in 
  (select top 1 appintro from[192-168-0-5].volunteer.dbo.applicationprocessconfig where programID  = dbo.fn_ProgramID())
  is null


--Issue 1486, adding more code tables to import---

insert into dbo.AllegationOutcome
([Description]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [Description]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[AllegationOutcome]
  where programID = 110 and Active= 1 and 
  (select top 1 Description from [192-168-0-5].volunteer.dbo.AllegationOutcome where programID  = dbo.fn_ProgramID()
  ) is null

  

insert into dbo.ChildReferralREASON
([ChildReferralDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CHILDREFERRALDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CHILDREFERRALREASON]
  where programID = 110 and Active= 1 and  
  (select top 1 ChildReferralDesc from [192-168-0-5].volunteer.dbo.CHILDREFERRALREASON where programID  = dbo.fn_ProgramID())
  is null


  


insert into dbo.DISABILITYTYPE
(DISABILITYTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      DISABILITYTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].DISABILITYTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 DISABILITYTYPEDesc from [192-168-0-5].volunteer.dbo.DISABILITYTYPE where programID  = dbo.fn_ProgramID())
  is null


  

insert into dbo.ethnicity
(ethnicityDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      ethnicityDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].ethnicity
  where programID = 110 and Active= 1 and 
  (select top 1 ethnicityDesc from[192-168-0-5].volunteer.dbo.ethnicity where programID  = dbo.fn_ProgramID())
  is null




  
insert into dbo.FinalPlacementType
(FinalPlacementTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      FinalPlacementTypeName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].FinalPlacementType
  where programID = 110 and Active= 1 and
  (select top 1 FinalPlacementTypeName from [192-168-0-5].volunteer.dbo.FinalPlacementType where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.GEOLOCATION
(GEOLOCATIONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      GEOLOCATIONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].GEOLOCATION
  where programID = 110 and Active= 1 and
  (select top 1 GEOLOCATIONDesc from [192-168-0-5].volunteer.dbo.GEOLOCATION where programID  = dbo.fn_ProgramID())
  is null




insert into dbo.INTERVIEWOUTCOMETYPE
(OutcomeTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      OutcomeTypeName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].INTERVIEWOUTCOMETYPE
  where programID = 110 and Active= 1 and  
  (select top 1 OutcomeTypeName from [192-168-0-5].volunteer.dbo.INTERVIEWOUTCOMETYPE where programID  = dbo.fn_ProgramID())
  is null




insert into dbo.legalstatustype
(legalstatustypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      legalstatustypeDesc
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].legalstatustype
  where programID = 110 and Active= 1 and 
  (select top 1 legalstatustypeDesc from [192-168-0-5].volunteer.dbo.legalstatustype where programID  = dbo.fn_ProgramID())
  is null



insert into dbo.ORDERSERVICETYPE
(ORDERSERVICETYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      ORDERSERVICETYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].ORDERSERVICETYPE
  where programID = 110 and Active= 1 and 
  (select top 1 ORDERSERVICETYPEDesc from [192-168-0-5].volunteer.dbo.ORDERSERVICETYPE where programID  = dbo.fn_ProgramID())
  is null




insert into dbo.PermanentPlanType
(PermanentPlanTypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
	  ,ModUserID
      ,[programID]
)
SELECT
      PermanentPlanTypeDesc
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
	  ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PermanentPlanType
  where programID = 110 and Active= 1 and 
  (select top 1 PermanentPlanTypeDesc from[192-168-0-5].volunteer.dbo.PermanentPlanType where programID  = dbo.fn_ProgramID())
  is null



insert into dbo.REFERENCESTATUS
([REFERENCESTATUSDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [REFERENCESTATUSDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[REFERENCESTATUS]
  where programID = 110 and Active= 1 and 
  (select top 1 REFERENCESTATUSDesc from [192-168-0-5].volunteer.dbo.REFERENCESTATUS where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.SCHOOLTYPE
(SchoolTypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      SCHOOLTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].SCHOOLTYPE
  where programID = 110 and Active= 1 and  
  (select top 1 SchoolTypeDesc from [192-168-0-5].volunteer.dbo.SCHOOLTYPE where programID  = dbo.fn_ProgramID())
  is null 

  

insert into dbo.TODOTASKPriorityType
(TODOTASKPriorityTypeDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKPriorityTypeDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKPriorityType
  where programID = 110 and Active= 1 and
  (select top 1 TODOTASKPriorityTypeDesc from [192-168-0-5].volunteer.dbo.TODOTASKPriorityType where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.TODOTASKSTATUSTYPE
(TODOTASKSTATUSTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKSTATUSTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKSTATUSTYPE
  where programID = 110 and Active= 1 and  
  (select top 1 TODOTASKSTATUSTYPEDesc from [192-168-0-5].volunteer.dbo.TODOTASKSTATUSTYPE where programID  = dbo.fn_ProgramID())
  is null




insert into dbo.TRAININGFORMAT
(TRAININGFORMATDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TRAININGFORMATDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TRAININGFORMAT
  where programID = 110 and Active= 1 and 
  (select top 1 TRAININGFORMATDesc from [192-168-0-5].volunteer.dbo.TRAININGFORMAT where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.VISITATIONTYPE
(VISITATIONTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      VISITATIONTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VISITATIONTYPE
  where programID = 110 and Active= 1 and  
  (select top 1 VISITATIONTYPEDesc from [192-168-0-5].volunteer.dbo.VISITATIONTYPE where programID  = dbo.fn_ProgramID())
  is null




  
insert into dbo.ApplicantTrainingCourse
([CourseName]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      coursename
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[ApplicantTrainingCourse]
  where programID = 110 and 
  (select top 1 CourseName from [192-168-0-5].volunteer.dbo.applicanttrainingcourse where programID  = dbo.fn_ProgramID()
  ) is null



  

insert into dbo.AbuseType
([AbuseTypeDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [AbuseTypeDesc]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[AbuseType]
  where programID = 110  and 
  (select top 1 AbuseTypeDesc from[192-168-0-5].volunteer.dbo.AbuseType where programID  = dbo.fn_ProgramID()
  )
  is null


  


insert into dbo.CaseReleaseReason
([CaseReleaseReasonDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CaseReleaseReasonDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CASERELEASEREASON]
  where programID = 110 and Active= 1 and 
  (select top 1 CASERELEASEREASONDesc from[192-168-0-5].volunteer.dbo.CASERELEASEREASON where programID  = dbo.fn_ProgramID())
  is null


  
insert into dbo.PETITIONCASETYPE
([PETITIONCASETYPEDesc]
      ,[Active]
     ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [PETITIONCASETYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PETITIONCASETYPE
  where programID = 110 and Active= 1 and 
  (select top 1 PETITIONCASETYPEDesc from[192-168-0-5].volunteer.dbo.PETITIONCASETYPE where programID  = dbo.fn_ProgramID())
  is null


  

insert into dbo.CaseChildStatus
(Statusname
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [Statusname]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CaseChildStatus
  where programID = 110 and Active= 1 and 
  (select top 1 StatusName from[192-168-0-5].volunteer.dbo.CaseChildStatus where programID  = dbo.fn_ProgramID())
  is null



  
insert into dbo.CaseClosureReason
(CaseClosureReasonDEsc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      CaseClosureReasonDesc
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CaseClosureReason
  where programID = 110 and Active= 1 and 
  (select top 1 CaseClosureReasonDesc from[192-168-0-5].volunteer.dbo.CaseClosureReason where programID  = dbo.fn_ProgramID())
  is null


  

insert into dbo.NonCaseActivityTypes
(NonCaseActivityTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[programID]
)
SELECT
      NonCaseActivityTypeName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].NonCaseActivityTypes
  where programID = 110 and Active= 1 and 
  (select top 1 NonCaseActivityTypeName from[192-168-0-5].volunteer.dbo.NonCaseActivityTypes where programID  = dbo.fn_ProgramID())
  is null



  insert into dbo.ChildReferral
([ChildReferralDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CHILDREFERRALDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CHILDREFERRAL]
  where programID = 110 and Active= 1 and 
  (select top 1 ChildReferralDesc from[192-168-0-5].volunteer.dbo.CHILDREFERRAL where programID  = dbo.fn_ProgramID())
  is null


  
insert into dbo.Concerns
([ConcernsDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [CONCERNSDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[CONCERNS]
  where programID = 110 and Active= 1 and 
  (select top 1 ConcernsDesc from[192-168-0-5].volunteer.dbo.CONCERNS where programID  = dbo.fn_ProgramID())
  is null





  insert into dbo.INTERESTEDPARTYTYPE
([INTERESTEDPARTYTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [INTERESTEDPARTYTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].INTERESTEDPARTYTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 INTERESTEDPARTYTYPEDesc from[192-168-0-5].volunteer.dbo.INTERESTEDPARTYTYPE where programID  = dbo.fn_ProgramID())
  is null



  

insert into dbo.VOLUNTEERACTIVITYTYPE
([VOLUNTEERACTIVITYTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERACTIVITYTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERACTIVITYTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 VOLUNTEERACTIVITYTYPEDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERACTIVITYTYPE where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.VOLUNTEERCONTACTTYPE
([VOLUNTEERCONTACTTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERCONTACTTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERCONTACTTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 VOLUNTEERCONTACTTYPEDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERCONTACTTYPE where programID  = dbo.fn_ProgramID())
  is null



  INSERT INTO [dbo].[ReferenceSurveyConfiguration]
           ([EmailTextBody]
           ,[EmailTextFooter]
           ,[EmailFromAddress]
           ,[programID])
SELECT
      [EmailTextBody]
           ,[EmailTextFooter]
           ,[EmailFromAddress]
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[ReferenceSurveyConfiguration] A
  where programID = 110 and 
  (select top 1 EmailFromAddress from [192-168-0-5].volunteer.dbo.[ReferenceSurveyConfiguration] where programID  = dbo.fn_ProgramID()
   and EmailTextBody = A.EmailTextBody
  ) is null


  

insert into dbo.EDUCATIONTYPE
(EDUCATIONTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      EDUCATIONTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].EDUCATIONTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 EDUCATIONTYPEDesc from[192-168-0-5].volunteer.dbo.EDUCATIONTYPE where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.EMPLOYMENTSTATUS
(EMPLOYMENTSTATUSDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      EMPLOYMENTSTATUSDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].EMPLOYMENTSTATUS
  where programID = 110 and Active= 1 and 
  (select top 1 EMPLOYMENTSTATUSDesc from[192-168-0-5].volunteer.dbo.EMPLOYMENTSTATUS where programID  = dbo.fn_ProgramID())
  is null


  

insert into dbo.LANGUAGETYPE
(LANGUAGETYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      LANGUAGETYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].LANGUAGETYPE
  where programID = 110 and Active= 1 and 
  (select top 1 LANGUAGETYPEDesc from[192-168-0-5].volunteer.dbo.LANGUAGETYPE where programID  = dbo.fn_ProgramID())
  is null


  insert into dbo.RELATIONSHIPTYPE
(RelationshipTypeName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      RELATIONSHIPTYPENAME
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].RELATIONSHIPTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 RelationshipTypeName from[192-168-0-5].volunteer.dbo.RELATIONSHIPTYPE where programID  = dbo.fn_ProgramID())
  is null 






  
insert into dbo.HEARINGOUTCOMETYPE
(HEARINGOUTCOMETYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HEARINGOUTCOMETYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HEARINGOUTCOMETYPE
  where programID = 110 and Active= 1 and 
  (select top 1 HEARINGOUTCOMETYPEDesc from[192-168-0-5].volunteer.dbo.HEARINGOUTCOMETYPE where programID  = dbo.fn_ProgramID())
  is null
  



insert into dbo.HEARINGSTATUS
(HEARINGSTATUSDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HEARINGSTATUSDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HEARINGSTATUS
  where programID = 110 and Active= 1 and 
  (select top 1 HEARINGSTATUSDesc from[192-168-0-5].volunteer.dbo.HEARINGSTATUS where programID  = dbo.fn_ProgramID())
  is null

insert into dbo.HEARINGTYPE
(HEARINGTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HEARINGTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HEARINGTYPE
  where programID = 110 and Active= 1 and
  (select top 1 HEARINGTYPEDesc from[192-168-0-5].volunteer.dbo.HEARINGTYPE where programID  = dbo.fn_ProgramID())
  is null



  

insert into dbo.PLACEMENTREASON
(PLACEMENTREASONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      PLACEMENTREASONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PLACEMENTREASON
  where programID = 110 and Active= 1 and
  (select top 1 PLACEMENTREASONDesc from[192-168-0-5].volunteer.dbo.PLACEMENTREASON where programID  = dbo.fn_ProgramID())
  is null



insert into dbo.PLACEMENTTYPE
(PLACEMENTTYPEDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      PLACEMENTTYPEDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].PLACEMENTTYPE
  where programID = 110 and Active= 1 and 
  (select top 1 PLACEMENTTYPEDesc from[192-168-0-5].volunteer.dbo.PLACEMENTTYPE where programID  = dbo.fn_ProgramID())
  is null

  





insert into dbo.TODOTASKSTATUSREASON
(TODOTASKSTATUSREASONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      TODOTASKSTATUSREASONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].TODOTASKSTATUSREASON
  where programID = 110 and Active= 1 and 
  (select TODOTASKSTATUSREASONDesc from[192-168-0-5].volunteer.dbo.TODOTASKSTATUSREASON where programID  = dbo.fn_ProgramID())
  is null 
  

insert into dbo.CERTIFICATION
(CERTIFICATIONName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      CERTIFICATIONNAME
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].CERTIFICATION
  where programID = 110 and Active= 1 and 
  (select top 1 CERTIFICATIONName from[192-168-0-5].volunteer.dbo.CERTIFICATION where programID  = dbo.fn_ProgramID())
  is null


insert into dbo.VOLUNTEERDISCHARGEREASON
(DISCHARGEREASONDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      DISCHARGEREASONDESC
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERDISCHARGEREASON
  where programID = 110 and Active= 1 and 
  (select top 1 DISCHARGEREASONDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERDISCHARGEREASON where programID  = dbo.fn_ProgramID())
  is null



  
insert into dbo.NOTESUBJECT
([NOTESUBJECTDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [NOTESUBJECTDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[NOTESUBJECT]
  where programID = 110 and Active= 1 and 
  (select top 1 NOTESUBJECTDesc from[192-168-0-5].volunteer.dbo.NOTESUBJECT where programID  = dbo.fn_ProgramID())
  is null 


insert into dbo.volunteerreferral
([volunteerreferralDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [volunteerreferralDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[volunteerreferral]
  where programID = 110 and Active= 1 and 
  (select top 1 volunteerreferralDesc from[192-168-0-5].volunteer.dbo.volunteerreferral where programID  = dbo.fn_ProgramID())
  is null


  

insert into dbo.VOLUNTEERSTATUS
(VOLUNTEERSTATUSName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      VOLUNTEERSTATUSNAME
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERSTATUS
  where programID = 110 and Active= 1 and 
  (select top 1 VOLUNTEERSTATUSName from[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUS where programID  = dbo.fn_ProgramID())
  is null 


insert into dbo.VOLUNTEERSTATUSREASON
([VOLUNTEERSTATUSREASONDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERSTATUSREASONDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[VOLUNTEERSTATUSREASON]
  where programID = 110 and Active= 1 and 
  (select top 1 VOLUNTEERSTATUSREASONDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERSTATUSREASON where programID  = dbo.fn_ProgramID())
  is null



  
insert into dbo.VOLUNTEERTYPE
([VOLUNTEERTYPEDesc]
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [VOLUNTEERTYPEDESC]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].[VOLUNTEERTYPE]
  where programID = 110 and Active= 1 and 
  (select top 1 VOLUNTEERTYPEDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERTYPE where programID  = dbo.fn_ProgramID())
  is null


  

insert into dbo.VOLUNTEERREJECTEDREASON
(RejectedReasonDesc
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      [RejectedReasonDesc]
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].VOLUNTEERREJECTEDREASON
  where programID = 110 and Active= 1 and 
  (select top 1 RejectedReasonDesc from[192-168-0-5].volunteer.dbo.VOLUNTEERREJECTEDREASON where programID  = dbo.fn_ProgramID())
  is null



insert into dbo.HearingLocation
(HearingLocationName
      ,[Active]
      ,[AddDate]
      ,[AddUserID]
      ,[ModDate]
      ,[ModUserID]
      ,[programID]
)
SELECT
      HearingLocationName
      ,[Active]
      ,GETDATE()
      ,-1
      ,GETDATE()
      ,-1
      ,dbo.fn_ProgramID()
  FROM [192.168.0.2].volunteer.[dbo].HearingLocation
  where programID = 110 and Active= 1 and 
  (select top 1 HearingLocationName from[192-168-0-5].volunteer.dbo.HearingLocation where programID  = dbo.fn_ProgramID())
  is null 




