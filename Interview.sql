--InterviewOutcomeType
/*

insert into [dbo].InterviewOutcomeType
        ( OutcomeTypeName, programID )
select distinct 

,dbo.fn_ProgramID()
from [CometSource].[dbo].

order by

*/



insert into [dbo].Interview
        ( PartyID ,
          InterviewDate ,
          InterviewerPartyID ,
          Notes ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          OutcomeTypeID ,
          InterviewerName ,
          programID
        )
select distinct
  c.PartyID
  ,case when isdate(convert(datetime,i.dDateInterviewed)) = 1 then i.dDateInterviewed end
  --Issue 722: Changing InterviewerPartyID to null
  --,ci.PartyID
  ,null as InterviewerPartyID 
  --Issue 722: Changing Notes to null
  --,i.sInterviewer
  ,null as Notes
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,null
  ,left(sInterviewer,100)
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteerInterview i
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = i.nPersonID
join [dbo].Volunteer v on v.PartyID = c.PartyID
--Issue 722, no longer need join
--left join [CometSource].[dbo].ConversionPersonToParty ci on ci.PersonID = i.nPersonID

