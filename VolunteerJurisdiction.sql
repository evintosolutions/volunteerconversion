
declare @Jurisdiction int = (select Jurisdictionid from dbo.Jurisdiction where JurisdictionName = dbo.fn_Jurisdiction()
and programid = dbo.fn_ProgramID()
)

insert into dbo.VolunteerJurisdiction
(PartyID,
JurisdictionID,
programID
)
select
p.PartyID
,@Jurisdiction
,dbo.fn_ProgramID()
from dbo.Volunteer v
join dbo.Party p on p.PartyID = v.PartyID and p.programID = dbo.fn_ProgramID()
join CometSource.dbo.ConversionPersonToParty cpp on cpp.PartyID =  p.PartyID