
------------------------------------------------------------
--HEARING
--Dependencies: HearingLocation, HearingStatus, Judge
---------------------------------------------------------------------------------


/****** Object:  Table [dbo].[ConversionPersonToParty]    Script Date: 03/12/2012 22:41:26 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[CometSource].[dbo].[ConversionHearingToHearing]') AND type in (N'U'))
DROP TABLE [CometSource].[dbo].ConversionHearingToHearing
GO

/****** Object:  Table [dbo].[ConversionHearingToHearing]    Script Date: 03/12/2012 22:41:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [CometSource].[dbo].ConversionHearingToHearing(
	[nHearingID] [int] NOT NULL,
	[HearingID] [int] NOT NULL
) ON [PRIMARY]

GO



 
  declare @HearingID int;
  declare @nHearingID int;
  declare @CaseID int;
  declare @HearingDate date;
  declare @HearingTime datetime;
  declare @DocketNumber varchar(50);
  declare @HearingLocationID int;
  declare @HearingStatusID int;
  declare @JudgeID int;
  declare @OrderFiledDate date;
  declare @OrderReceivedDate date;
  declare @Notes varchar(5000);
  declare @ProgramID int

  set @ProgramID =  dbo.fn_ProgramID();

   declare crs cursor for
    select
      h.nHearingID
    , cg.CaseID
    , case when isdate(convert(datetime,h.dHearing))=1 then h.dHearing end
    , case when isdate(convert(datetime,h.tHearing))=1 then h.tHearing end
    , h.sDocketNum
    , hl.HearingLocationID
    , hs.HearingStatusID
    , j.JudgeID
    , case when isdate(convert(datetime,h.dOrderFiled))=1 then h.dOrderFiled end
    , case when isdate(convert(datetime,h.dOrderReceived))=1 then h.dOrderReceived end
    , h.sNote
    ,@ProgramID ProgramID
    from [CometSource].[dbo].tblHearing h
    join [CometSource].[dbo].ConversionGroupToCase cg on h.nGroupID = cg.nGroupID
    left join [dbo].HearingLocation hl on hl.HearingLocationName = h.sHearingLocation and hl.programID = dbo.fn_ProgramID()
    left join [dbo].HearingStatus hs on hs.HearingStatusDesc = h.sStatus and hs.programID = dbo.fn_ProgramID()
    left join [CometSource].[dbo].tblPerson p on p.nPersonID = h.nJudgeID
    left join [dbo].Judge j on j.LastName = p.sLName and j.FirstName = p.sFName and (j.MiddleName = p.sMName or ((j.MiddleName = '' or j.MiddleName is null) and p.sMName is null)) and j.programID = dbo.fn_ProgramID()
    where isdate(convert(datetime,h.dHearing)) = 1 
    ;
  
  open crs;

  fetch crs
   into
     @nHearingID       
    ,@CaseID
    ,@HearingDate
    ,@HearingTime
    ,@DocketNumber
    ,@HearingLocationID
    ,@HearingStatusID
    ,@JudgeID
    ,@OrderFiledDate
    ,@OrderReceivedDate
    ,@Notes
    ,@ProgramID
   
   while @@FETCH_STATUS = 0
   begin
      insert into [dbo].Hearing
              ( CaseID ,
                HearingDate ,
                HearingTime ,
                DocketNumber ,
                HearingLocationID ,
                HearingStatusID ,
                JudgeID ,
                OrderFiledDate ,
                OrderReceivedDate ,
                Notes ,
                AddDate ,
                AddUserID ,
                ModDate ,
                ModUserID ,
                programID
              )
      values  ( 
           @CaseID
          ,@HearingDate
          ,@HearingTime
          ,@DocketNumber
          ,@HearingLocationID
          ,@HearingStatusID
          ,@JudgeID
          ,@OrderFiledDate
          ,@OrderReceivedDate
          ,@Notes
          ,getdate()
          ,-1
          ,getdate()
          ,-1
          ,@ProgramID
      )
           
      set @HearingID = SCOPE_IDENTITY();
      
      insert into [CometSource].[dbo].[ConversionHearingToHearing]
              ( [nHearingID], [HearingID] )
      values  ( @nHearingID ,@HearingID );

      --print 'ID = ' + convert(varchar, @PersonID) + '  New ID = ' + convert(varchar, @PartyID);
   
      fetch crs
       into
         @nHearingID       
        ,@CaseID
        ,@HearingDate
        ,@HearingTime
        ,@DocketNumber
        ,@HearingLocationID
        ,@HearingStatusID
        ,@JudgeID
        ,@OrderFiledDate
        ,@OrderReceivedDate
        ,@Notes
        ,@ProgramID
   end
   
   close crs;
   deallocate crs;

go
