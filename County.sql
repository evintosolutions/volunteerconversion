--County


with t as (
  --select distinct sCounty CountyName
  --from [CometSource].[dbo].[_tblCounty]
  --where len(sCounty) > 0
  
  --union
  
  select distinct sHomeCounty CountyName
  from [CometSource].[dbo].tblPerson
  where len(sHomeCounty) > 0
)

insert into [dbo].County
        ( CountyName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct CountyName, 1, getdate(), -1, getdate(), -1,dbo.fn_ProgramID()
from t
where CountyName not in (select CountyName from dbo.County where programID = dbo.fn_ProgramID())
order by CountyName