
---------------------------------------------------------------------------------
--HEARING LOCATION
--Dependencies: 
---------------------------------------------------------------------------------


insert into [dbo].HearingLocation
        ( HearingLocationName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
ltrim(rtrim(hl.sHearingLocationDesc))
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblHearingLocation] hl
where len(ltrim(rtrim(hl.sHearingLocationDesc))) > 0
and ltrim(rtrim(hl.sHearingLocationDesc)) not in (select HearingLocationName from dbo.HearingLocation where programid = dbo.fn_ProgramID())
