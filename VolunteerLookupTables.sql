

--VolunteerReferral


insert into [dbo].VolunteerReferral
        ( VolunteerReferralDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct sVolunteerReferralDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblVolunteerReferral]
where len(sVolunteerReferralDesc)>0
and sVolunteerReferralDesc not in (select VolunteerReferralDesc from dbo.VolunteerReferral where programId = dbo.fn_ProgramID())
order by sVolunteerReferralDesc;




with t as (
--select distinct sDischargeReasonDesc sDischargeReason
--from [CometSource].[dbo]._tblDischargeReason
--where len(sDischargeReasonDesc) > 0

--union

select distinct sDischargeReason sDischargeReason
from [CometSource].[dbo].tblPersonVolunteer
where len(sDischargeReason) > 0
and sDischargeReason not in (select DischargeReasonDesc from dbo.VolunteerDischargeReason where programID = dbo.fn_ProgramID())
)

insert into [dbo].VolunteerDischargeReason
        ( DischargeReasonDesc ,
          Active ,
          programID ,
          [AddDate] ,
          [AddUserID] ,
          [ModDate] ,
          [ModUserID]
        )
select distinct
sDischargeReason
,1
,dbo.fn_ProgramID()
,getdate()
,-1
,getdate()
,-1
from t
order by sDischargeReason
        

--------------------------






--VolunteerType

insert into [dbo].VolunteerType
        ( VolunteerTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programId
        )
select distinct sVolunteerCatDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblVolunteerCat]
where len(sVolunteerCatDesc)>0
and sVolunteerCatDesc not in (select Volunteertypedesc from dbo.volunteertype where programid = dbo.fn_programid())
order by sVolunteerCatDesc



--VolunteerStatus

insert into [dbo].VolunteerStatus
        ( VolunteerStatusName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct sVolunteerStatusDesc, 1, getdate(), -1, getdate(), -1, dbo.fn_ProgramID()
from [CometSource].[dbo].[_tblVolunteerStatus]
where len(sVolunteerStatusDesc)>0
and sVolunteerStatusDesc not in (select VolunteerStatusName from dbo.VolunteerStatus where programID = dbo.fn_ProgramID())
order by sVolunteerStatusDesc

