

insert into [dbo].EmergencyContact
        ( PartyID ,
          LastName ,
          FirstName ,
          Phone ,
          Relationship ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          Email ,
          Phone2 ,
          programID
        )
select distinct
  c.PartyID
  ,case when charindex(' ',pv.sEmerContact) > 0 then ltrim(rtrim(substring(pv.sEmerContact,charindex(' ',pv.sEmerContact)+1,len(pv.sEmerContact)-charindex(' ',pv.sEmerContact)))) else pv.sEmerContact end
  ,case when charindex(' ',pv.sEmerContact) > 0 then ltrim(rtrim(substring(pv.sEmerContact,1,charindex(' ',pv.sEmerContact)-1))) else pv.sEmerContact end
  ,pv.sEmerPhone
  ,'N/A'
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,null
  ,null
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer pv
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pv.nPersonID
where len(pv.sEmerContact) > 0
and len(pv.sEmerPhone) > 0
