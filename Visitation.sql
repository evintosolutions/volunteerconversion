

--------------------------------------------------------------------------------
--VisitationType
--Dependencies: 
---------------------------------------------------------------------------------

 
with t as (
  select distinct 
  sVisitationTypeDesc sVisitationType
  from [CometSource].[dbo].[_tblVisistationType]
  where len(sVisitationTypeDesc) > 0
)

insert into [dbo].VisitationType
        ( VisitationTypeDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sVisitationType
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from t
where sVisitationType not in (select VisitationTypeDesc from dbo.VisitationType where programid = dbo.fn_programid())
order by sVisitationType




--------------------------------------------------------------------------------
--Visitation
--Dependencies: Hearing, CaseAffiliatedParty, VisitationType, CaseFamilyMember, CaseChild
---------------------------------------------------------------------------------
 
insert into [dbo].Visitation
        ( HearingID ,
          CaseAffiliatedPartyID ,
          VisitationTypeID ,
          SpecifiedFrequency ,
          ActualFrequency ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          CaseFamilyMemberID ,
          CaseChildID ,
          programID
        )
select
hh.HearingID
,null
,vt.VisitationTypeID
,v.sFrequency
,v.sActualFreqency
,getdate()
,-1
,getdate()
,-1
,cfm.CaseFamilyMemberID
,cc.CaseChildID
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblCOVisitation v
join [CometSource].[dbo].ConversionHearingToHearing hh on hh.nHearingID = v.HearingID
join [dbo].VisitationType vt on vt.VisitationTypeDesc = v.sVisitationType and vt.programID = dbo.fn_ProgramID()
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = v.nFamilyID
join [CometSource].[dbo].ConversionPersonToParty ch on ch.PersonID = v.nChildID
join [dbo].CaseChild cc on cc.ChildPartyID = ch.PartyID and cc.programID = dbo.fn_ProgramID()
join [dbo].CaseFamilyMember cfm on cfm.FamilyPartyID = c.PartyID and cfm.programID = dbo.fn_ProgramID() and cfm.CaseID = cc.CaseID


