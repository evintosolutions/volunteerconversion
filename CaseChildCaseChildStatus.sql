 

insert into dbo.CaseChildCaseChildStatus
        ( CaseChildID ,
          CaseChildStatusID ,
          CaseChildStatusDate ,
CaseChildStatusEndDate,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          ProgramID
        )
select
    cc.CaseChildID
  , ccs.CaseChildStatusID
  , case when isdate(pc.dAssignCASA) = 1 then pc.dAssignCASA end
,null as CaseChildStatusEndDate
  ,getdate() AddDate
  ,-1 AddUserID
  ,getdate() ModDate
  ,-1 ModUserID
  ,dbo.fn_ProgramID() ProgramID
from [CometSource].[dbo].tblPersonChild pc
join [CometSource].[dbo].ConversionPersonToParty cpp on cpp.PersonID = pc.nPersonID
join dbo.CaseChild cc on cc.ChildPartyID = cpp.PartyID and cc.programID = dbo.fn_ProgramID()
join dbo.CaseChildStatus ccs on ccs.StatusName = pc.sIntitialStatus and ccs.programID = dbo.fn_ProgramID()
where isdate(pc.dAssignCASA) = 1