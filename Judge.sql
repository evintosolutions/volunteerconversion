--Judge

insert into [dbo].Judge
        ( LastName ,
          FirstName ,
          MiddleName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct
sLName
, sFName
, coalesce(sMName,'')
, 1--case bInactive when 0 then 1 else 0 end
, getdate()
, -1
, getdate()
, -1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPerson p
where p.sCategory like '%judge%'
and sLName not in (select LastName from dbo.Judge where programid = dbo.fn_Programid() and 
FirstName = sFName and MiddleName = coalesce(sMName,''))
