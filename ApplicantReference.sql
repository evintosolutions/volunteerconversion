------------------------------


insert into [dbo].[ApplicantReference]
        ( [PartyID] ,
          [LastName] ,
          [FirstName] ,
          --[Relationship] ,
          --[Address] ,
          --[Address2] ,
          --[City] ,
          --[State] ,
          --[Zip] ,
          [Phone] ,
          --[Email] ,
          [CheckDate] ,
          --[CheckPartyID] ,
          [Notes] ,
          [AddDate] ,
          [AddUserID] ,
          [ModDate] ,
          [ModUserID],
          --[SurveySent] ,
          --[SurveyReceived]
          [ProgramID]
        )
select distinct 
  c.PartyID
  ,pvr.sLName
  ,isnull(pvr.sFName,'<Blank>')
  ,pvr.sPhone
  ,case when isdate(convert(datetime,pvr.dDateCalled)) = 1 then pvr.dDateCalled end
  ,case when len(pvr.sResult) > 0 then pvr.sResult + '. ' else '' end + coalesce(case when isdate(convert(datetime,pvr.dDateCalled)) = 1 then 'Called: ' + convert(varchar,pvr.dDateCalled) end,'')
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteerReference pvr
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = pvr.nPersonID
join dbo.Volunteer v on v.PartyID = c.partyid
where len(pvr.sLName) > 0