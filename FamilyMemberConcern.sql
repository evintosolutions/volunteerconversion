
---------------------------------------------------------------------------------
--FAMILY MEMBER CONCERN
--Dependencies: CaseFamilyMember, Concerns
---------------------------------------------------------------------------------

 
insert into [dbo].FamilyMemberConcern
        ( CaseFamilyMemberID ,
          ConcernsID ,
          programID
        )
select distinct cfm.CaseFamilyMemberID,con.ConcernsID,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonFamilyConcern fc
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = fc.nPersonID
join [dbo].CaseFamilyMember cfm on cfm.FamilyPartyID = c.PartyID and cfm.programID = dbo.fn_ProgramID()
join [dbo].Concerns con on con.ConcernsDesc = fc.sFamilyConcern and con.programID = dbo.fn_ProgramID()
