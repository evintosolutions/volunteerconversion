

-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--BEGINNING OF INIT 
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--use [Volunteer];
--go

CREATE 
--ALTER 
FUNCTION dbo.fn_ProgramName()
RETURNS Varchar(50)
AS
BEGIN
    RETURN 'PUT PROGRAM NAME HERE' 

END


CREATE 
--ALTER 
FUNCTION dbo.fn_ProgramID()
RETURNS int
AS
BEGIN
    RETURN ##PUT PROGRAM_ID_HERE##

END


CREATE 
--ALTER 
FUNCTION dbo.fn_Jurisdiction()
RETURNS varchar(50)
AS
BEGIN
    RETURN ##PUT Jurisdiction_HERE##

END


--Set ProgramID to this client ProgramID so conversion programmer can access front-end for this client
update dbo.SystemUser
set programID = dbo.fn_ProgramID()
where programID <> dbo.fn_ProgramID()
and UserName = 'cory.cresiski'
and email = 'cory5@evintosolutions.com'
--


declare @ProgramName varchar(50);set @ProgramName = dbo.fn_ProgramName();
declare @ProgramID int;select @ProgramID = programID from [VolunteerMaster].dbo.VolunteerProgram where ProgramName like '%' + @ProgramName + '%';




declare @tablename varchar(255);
set @tablename = 'SystemUser_backup_'+replace(replace((replace(@ProgramName,' ','_')),'.','_'),'-','')  + convert(char(4),year(getdate())) + case when len(month(getdate())) < 2 then '0' + convert(char(1),month(getdate())) else convert(char(2),month(getdate())) end + case when len(day(getdate())) < 2 then '0' + convert(char(1),day(getdate())) else convert(char(2),day(getdate())) end
declare @cmd varchar(max);
select @cmd = 
'if (select object_id(''' + @tablename + ''')) is not null drop table ' + @tablename + ';' 
+ 'select su.* into ' + @tablename + ' from dbo.SystemUser su where programID = ' + convert(varchar,@ProgramID) + ';'

exec(@cmd);


delete  [dbo].[ChildClosingLog] where programID = @ProgramID; dbcc checkident([ChildClosingLog],reseed);
delete  [dbo].[Event] where programID = @ProgramID; dbcc checkident([Event],reseed);
delete  [dbo].CaseClosingLog where programID = @ProgramID; dbcc checkident(CaseClosingLog,reseed);
delete  [dbo].PlacementHistory where ProgramID = @ProgramID;dbcc checkident(PlacementHistory,reseed);
delete  [dbo].PartyCaseAssociatedParty where ProgramID = @ProgramID;dbcc checkident(PartyCaseAssociatedParty,reseed);
delete  [dbo].CaseAssociatedParty where ProgramID = @ProgramID;dbcc checkident(CaseAssociatedParty,reseed);
delete  [dbo].Allegation where programid = @ProgramID;dbcc checkident(Allegation,reseed);
delete  [dbo].CaseChildRemovedFrom where programid = @ProgramID;dbcc checkident(CaseChildRemovedFrom,reseed);
delete  [dbo].CaseFamilyMemberChildRelationship where programid = @ProgramID;dbcc checkident(CaseFamilyMemberChildRelationship,reseed);

--11/26/13 CC: Deleting per email from Linda, that this is a configuration
--delete  [dbo].CourtReportDetail where programid = @ProgramID;dbcc checkident(CourtReportDetail,reseed);
--delete  [dbo].CourtReport where programid = @ProgramID;dbcc checkident(CourtReport,reseed);
--delete  [dbo].CourtReportApproval where programid = @ProgramID;dbcc checkident(CourtReportApproval,reseed);


delete  [dbo].Document where programid = @ProgramID;dbcc checkident(Document,reseed);
delete  [dbo].DocumentType where programid = @ProgramID;dbcc checkident(DocumentType,reseed);

delete  [dbo].CaseVolunteer where programid = @ProgramID;dbcc checkident(CaseVolunteer,reseed);
delete  [dbo].CaseVolunteerChild where programid = @ProgramID;dbcc checkident(CaseVolunteerChild,reseed);
delete  [dbo].FamilyMemberConcern where programid = @ProgramID;dbcc checkident(FamilyMemberConcern,reseed);
delete  [dbo].HearingDetail where programid = @ProgramID;dbcc checkident(HearingDetail,reseed);
delete  [dbo].HearingChild where programid = @ProgramID;dbcc checkident(HearingChild,reseed);
delete  [dbo].HearingFamily where programid = @ProgramID;dbcc checkident(HearingFamily,reseed);
delete  [dbo].OrderService where programid = @ProgramID;dbcc checkident(OrderService,reseed);
delete  [dbo].Visitation where programid = @ProgramID;dbcc checkident(Visitation,reseed);
delete  [dbo].CaseFamilyMember where programid = @ProgramID;dbcc checkident(CaseFamilyMember,reseed);
delete fm from FamilyMember fm join Party p on p.PartyID = fm.PartyID where p.programID = @ProgramID;
delete  [dbo].HearingOutcome where programid = @ProgramID;dbcc checkident(HearingOutcome,reseed);
delete  [dbo].HearingPetition where programid = @ProgramID;dbcc checkident(HearingPetition,reseed);
delete  [dbo].HearingVolunteerActivity where programid = @ProgramID;dbcc checkident(HearingVolunteerActivity,reseed);
delete  [dbo].HearingVolunteerInput where programid = @ProgramID;dbcc checkident(HearingVolunteerInput,reseed);
delete  [dbo].PermanentPlan where programid = @ProgramID;dbcc checkident(PermanentPlan,reseed);

delete  [dbo].Hearing where programid = @ProgramID;dbcc checkident(Hearing,reseed);
delete  [dbo].Petition where programid = @ProgramID;dbcc checkident(Petition,reseed);
delete  [dbo].PetitionChild where programid = @ProgramID;dbcc checkident(PetitionChild,reseed);
delete  [dbo].PlacementHistory where programid = @ProgramID;dbcc checkident(PlacementHistory,reseed);
delete  [dbo].VolunteerActivity where programid = @ProgramID;dbcc checkident(VolunteerActivity,reseed);
delete  [dbo].CourtReportCaseChild where programid = @ProgramID;dbcc checkident(CourtReportCaseChild,reseed);
delete  [dbo].CaseChildCaseChildStatus where programID = @ProgramID; dbcc checkident(CaseChildCaseChildStatus,reseed);
delete  [dbo].CaseChildStatus where programID = @ProgramID; dbcc checkident(CaseChildStatus,reseed);
delete  [dbo].CaseChild where programid = @ProgramID;dbcc checkident(CaseChild,reseed);
delete c from  [dbo].Child c where exists(select  1 from  [dbo].Party p where p.PartyID = c.PartyID and p.ProgramID = @ProgramID);
delete  [dbo].CaseInfo where programid = @ProgramID;dbcc checkident(CaseInfo,reseed);
delete  [dbo].EmergencyContact where programid = @ProgramID;dbcc checkident(EmergencyContact,reseed);
delete  [dbo].AssociatedParty where programid = @ProgramID;dbcc checkident(AssociatedParty,reseed);
delete  [dbo].TrainingActivity where programid = @ProgramID;dbcc checkident(TrainingActivity,reseed);
delete  [dbo].SchoolHistory where programid = @ProgramID;dbcc checkident(SchoolHistory,reseed);
delete  [dbo].PrefAbuse where programid = @ProgramID;dbcc checkident(PrefAbuse,reseed);
delete  [dbo].PrefDisability where programid = @ProgramID;dbcc checkident(PrefDisability,reseed);
delete  [dbo].PrefGeoLocation where programid = @ProgramID;dbcc checkident(PrefGeoLocation,reseed);
delete  [dbo].Language where programid = @ProgramID;dbcc checkident(Language,reseed);
delete  [dbo].Judge where programid = @ProgramID;dbcc checkident(Judge,reseed);
delete  [dbo].[Note] where programid = @ProgramID;dbcc checkident(Note,reseed);
delete  [dbo].[NoteSubject] where programid = @ProgramID;dbcc checkident(NoteSubject,reseed);
delete  [dbo].[Education] where programid = @ProgramID;dbcc checkident(Education,reseed);
delete  [dbo].[EducationType] where programid = @ProgramID;dbcc checkident(EducationType,reseed);
delete  [dbo].Interview where programid = @ProgramID;dbcc checkident(Interview,reseed);
delete  [dbo].Employment where programid = @ProgramID;dbcc checkident(Employment,reseed);
delete  [dbo].Disability where programid = @ProgramID;dbcc checkident(Disability,reseed);
delete  [dbo].BackgroundCheck where programid = @ProgramID;dbcc checkident(BackgroundCheck,reseed);
delete  [dbo].BackgroundCheckType where programid = @ProgramID;dbcc checkident(BackgroundCheckType,reseed);
delete  [dbo].VolunteerCertifyCheckLog where programid = @ProgramID;dbcc checkident(VolunteerCertifyCheckLog,reseed);
delete  [dbo].VolunteerJurisdiction where programid = @ProgramID;dbcc checkident(VolunteerJurisdiction,reseed);
delete  dbo.VolunteerReferenceSurvey where programId = @ProgramID;dbcc checkident(VolunteerReferenceSurvey,reseed);
delete  [dbo].[ApplicantReference] where programid = @ProgramID;dbcc checkident(ApplicantReference,reseed);
delete  [dbo].[ApplicantTraining] where programid = @ProgramID;dbcc checkident(ApplicantTraining,reseed);
delete  [dbo].[ApplicantTrainingCourse] where programid = @ProgramID;dbcc checkident(ApplicantTrainingCourse,reseed);
delete  [dbo].AddressBook where programid = @ProgramID;dbcc checkident(AddressBook,reseed);
delete  [dbo].VisitationType where programid = @ProgramID;dbcc checkident(VisitationType,reseed);
delete  [dbo].TrainingTopic where programid = @ProgramID;dbcc checkident(TrainingTopic,reseed);
delete  [dbo].TrainingFormat where programid = @ProgramID;dbcc checkident(TrainingFormat,reseed);
delete  [dbo].Trainer where programid = @ProgramID;dbcc checkident(Trainer,reseed);
delete  [dbo].School where programid = @ProgramID;dbcc checkident(School,reseed);
delete  [dbo].SchoolType where programid = @ProgramID;dbcc checkident(SchoolType,reseed);
delete  [dbo].SchoolTransferReason where programid = @ProgramID;dbcc checkident(Schooltransferreason,reseed);


delete  [dbo].RelationshipType where programid = @ProgramID;dbcc checkident(RelationshipType,reseed);
delete  [dbo].PermanentPlanType where programid = @ProgramID;dbcc checkident(PermanentPlanType,reseed);
delete  [dbo].OrderServiceType where programid = @ProgramID;dbcc checkident(OrderServiceType,reseed);
delete  [dbo].InterviewOutcomeType where programID = @ProgramID; dbcc checkident(InterviewOutcomeType,reseed);
delete  [dbo].InterestedPartyType where programID = @ProgramID; dbcc checkident(InterestedPartyType,reseed);
delete  [dbo].HearingVolunteerActivityType where programID = @ProgramID; dbcc checkident(HearingVolunteerActivityType,reseed);
delete  [dbo].HearingType where programID = @ProgramID; dbcc checkident(HearingType,reseed);
delete  [dbo].HearingStatus where programID = @ProgramID; dbcc checkident(HearingStatus,reseed);
delete  [dbo].HearingOutcomeType where programID = @ProgramID; dbcc checkident(HearingOutcomeType,reseed);
delete  [dbo].HearingLocation where programID = @ProgramID; dbcc checkident(HearingLocation,reseed);
delete  [dbo].AllegationOutcome where programID = @ProgramID; dbcc checkident(AllegationOutcome,reseed);
delete  [dbo].PlacementReason where programID = @ProgramID; dbcc checkident(PlacementReason,reseed);
delete  [dbo].PlacementFacility where programID = @ProgramID; dbcc checkident(PlacementFacility,reseed);
delete  [dbo].LicensingAgency where programID = @ProgramID; dbcc checkident(LicensingAgency,reseed);
delete  [dbo].PlacementType where programID = @ProgramID; dbcc checkident(PlacementType,reseed);
delete i from  [dbo].InquiryDetail i where programID = @ProgramID or exists (select 1 from VolunteerReferral r where i.VolunteerReferralID = r.VolunteerReferralID and r.ProgramID = @ProgramID); dbcc checkident(InquiryDetail,reseed);
delete from dbo.VolunteerCertification where programID = @ProgramID; dbcc checkident(VolunteerCertification,reseed);
delete from dbo.Certification where programID = @ProgramID; dbcc checkident(Certification,reseed);
delete from dbo.Inquiry where programID = @ProgramID; dbcc checkident(Inquiry,reseed);
delete from dbo.VolunteerJurisdiction where programID = @ProgramID; dbcc checkident(VolunteerJurisdiction,reseed);
delete from dbo.VolunteerExperience where programId = @ProgramID;  dbcc checkident(VolunteerExperience,reseed);
delete from dbo.VolunteerResponse where programID = @ProgramID; dbcc checkident(VolunteerResponse,reseed);
delete from dbo.VolunteerExplain where programID = @ProgramID; dbcc checkident(VolunteerExplain,reseed);

delete  [dbo].VolunteerSpouse where programid = @ProgramID;dbcc checkident(VolunteerSpouse,reseed);
delete  [dbo].VolunteerChild where programid = @ProgramID;dbcc checkident(VolunteerChild,reseed);

delete  [dbo].VolunteerCertification where programID = @ProgramID; dbcc checkident(VolunteerCertification,reseed);

delete  [dbo].NonCaseActivity where programid = @ProgramID;dbcc checkident(NonCaseActivity,reseed);

delete v from  [dbo].Volunteer v where exists(select  1 from  [dbo].Party p where p.PartyID = v.PartyID and p.ProgramID = @ProgramID);
delete a from  [dbo].Attorney a where exists(select  1 from  [dbo].Party p where p.PartyID = a.PartyID and p.ProgramID = @ProgramID);

delete  [dbo].Party where programid = @ProgramID;dbcc checkident(Party,reseed);
delete  [dbo].VolunteerReferral where programID = @ProgramID; dbcc checkident(VolunteerReferral,reseed);
delete  [dbo].PetitionCaseType where programID = @ProgramID; dbcc checkident(PetitionCaseType,reseed);
delete  [dbo].CaseReleaseReason where programID = @ProgramID; dbcc checkident(CaseReleaseReason,reseed);
delete  [dbo].CaseClosureReason where programID = @ProgramID; dbcc checkident(CaseClosureReason,reseed);
delete  [dbo].AbuseType where programID = @ProgramID; dbcc checkident(AbuseType,reseed);
delete  [dbo].DisabilityType where programID = @ProgramID; dbcc checkident(DisabilityType,reseed);
delete  [dbo].LanguageType where programID = @ProgramID; dbcc checkident(LanguageType,reseed);
delete  [dbo].EmploymentStatus where programID = @ProgramID; dbcc checkident(EmploymentStatus,reseed);
--delete  [dbo].PartyType where programID = @ProgramID;dbcc checkident(PartyType,reseed);
delete  [dbo].Eligible where programID = @ProgramID; dbcc checkident(EligibleStatus,reseed);
delete  [dbo].EligibleStatus where programID = @ProgramID; dbcc checkident(EligibleStatus,reseed);
delete  [dbo].County where programid = @ProgramID; dbcc checkident(County,reseed);
delete  [dbo].Concerns where programid = @ProgramID; dbcc checkident(Concerns,reseed);
delete  [dbo].ChildReferral where programid = @ProgramID; dbcc checkident(ChildReferral,reseed);
delete  [dbo].ChildReferralReason where programid = @ProgramID; dbcc checkident(ChildReferralReason,reseed);
delete  [dbo].GeoLocation where programid = @ProgramID; dbcc checkident(GeoLocation,reseed);
delete  [dbo].FinalPlacementType where programid = @ProgramID; dbcc checkident(FinalPlacementType,reseed);
delete  [dbo].Agency where programID = @ProgramID; dbcc checkident(Agency,reseed);
delete  [dbo].VolunteerDischargeReason where programid = @ProgramID;dbcc checkident(VolunteerDischargeReason,reseed);
delete  [dbo].VolunteerStatus where programID = @ProgramID; dbcc checkident(VolunteerStatus,reseed);
delete  [dbo].VolunteerType where programID = @ProgramID; dbcc checkident(VolunteerType,reseed);
delete  [dbo].VolunteerContactType where programID = @ProgramID; dbcc checkident(VolunteerContactType,reseed);
delete  [dbo].VolunteerActivityType where programID = @ProgramID; dbcc checkident(VolunteerActivityType,reseed);
delete  [dbo].Ethnicity where programID = @ProgramID; dbcc checkident(Ethnicity,reseed);
delete  [dbo].Jurisdiction where programID = @ProgramID; dbcc checkident(Jurisdiction,reseed);
delete  [dbo].CareerType where programid = @ProgramID; dbcc checkident(CareerType,reseed);
delete  [dbo].SystemUser where programid = @ProgramID and username not like '%cresiski%';dbcc checkident(SystemUser,reseed);

delete from dbo.Question where programID = @ProgramID; dbcc checkident(Question,reseed);


delete  [dbo].AssessmentChildResult where programid = @ProgramID;dbcc checkident(AssessmentChildResult,reseed);
delete  [dbo].AssessmentQuestion where programid = @ProgramID;dbcc checkident(AssessmentQuestion,reseed);
delete  [dbo].AssessmentChild where programid = @ProgramID;dbcc checkident(AssessmentChild,reseed);
delete  [dbo].AssessmentCategory where programid = @ProgramID;dbcc checkident(AssessmentCategory,reseed);
delete  [dbo].Assessment where programid = @ProgramID;dbcc checkident(Assessment,reseed);

delete  [dbo].ToDoTaskDetail where programid = @ProgramID;dbcc checkident(ToDoTaskDetail ,reseed);
delete  [dbo].ToDoTask where programid = @ProgramID;dbcc checkident(ToDoTask ,reseed);
delete  [dbo].ToDoTaskServiceType where programid = @ProgramID;dbcc checkident(ToDoTaskServiceType ,reseed);
delete  [dbo].ToDoTaskCategory where programid = @ProgramID;dbcc checkident(ToDoTaskCategory ,reseed);
delete  [dbo].ToDoTaskPriorityType where programid = @ProgramID;dbcc checkident(ToDoTaskPriorityType,reseed);
delete  [dbo].ToDoTaskStatusReason where programid = @ProgramID;dbcc checkident(ToDoTaskStatusReason,reseed);
delete  [dbo].ToDoTaskStatusType where programid = @ProgramID;dbcc checkident(ToDoTaskStatusType,reseed);

delete  [dbo].LegalStatusType where programid = @ProgramID;dbcc checkident(LegalStatusType,reseed);
delete  [dbo].NonCaseActivityTypes where programid = @ProgramID;dbcc checkident(NonCaseActivityTypes,reseed);

delete  [dbo].VolunteerStatusReason where programid = dbo.fn_ProgramID();dbcc checkident(Volunteerstatusreason,reseed);
delete  [dbo].MaritalStatus where programid = @ProgramID; dbcc checkident(MaritalStatus,reseed);

delete  [dbo].[Degree] where programID = @ProgramID; dbcc checkident([Degree],reseed);
delete  [dbo].[ReferenceStatus] where programID = @ProgramID; dbcc checkident([ReferenceStatus],reseed);
delete  [dbo].[VolunteerRejectedReason] where programID = @ProgramID; dbcc checkident([VolunteerRejectedReason],reseed);
--Taking out per issue 1262
--delete  [dbo].Configuration where programid = @ProgramID;dbcc checkident(Configuration,reseed);
--delete  [dbo].ApplicationProcessConfig where programid = @ProgramID;dbcc checkident(ApplicationProcessConfig,reseed);

delete from dbo.InquiryEvent where programID = @ProgramID; dbcc checkident(InquiryEvent,reseed);

--Taking out per issue 1262
--delete from dbo.AppQuestion where programID = @ProgramID; dbcc checkident(AppQuestion,reseed);
--delete from dbo.AppQuestionType where programID = @ProgramID;

--Taking out per issue 1262
--delete from dbo.ReferenceSurveyQuestion where programID = @ProgramID; dbcc checkident(ReferenceSurveyQuestion,reseed);
--delete from dbo.InquiryConfiguration where programID = @ProgramID; dbcc checkident(InquiryConfiguration,reseed);


-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--END OF INIT
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
