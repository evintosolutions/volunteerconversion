
---------------------------------------------------------------------------------
--RelationshipType
--Dependencies:  
---------------------------------------------------------------------------------


 

with t as (
select distinct 
sRelationID  RelationshipTypeName
from [CometSource].[dbo].tblRelationshipChildFamily
where len(sRelationID) > 0

union

select distinct 
sRemovedRelation RelationshipTypeName
from [CometSource].[dbo].tblPersonChild
where len(sRemovedRelation) > 0

union

select distinct 
sRelationDesc RelationshipTypeName
from [CometSource].[dbo].[_tblRelation]
where len(sRelationDesc) > 0
)

insert into [dbo].RelationshipType
        ( RelationshipTypeName ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
RelationshipTypeName
,1 active
,getdate() AddDate
,-1 AddUserID
,getdate() ModDate
,-1 ModUserID
,dbo.fn_ProgramID() ProgramID
from t
where RelationshipTypeName not in (select RelationshipTypeName from dbo.RelationshipType where programid = dbo.fn_programID())
order by RelationshipTypeName