
insert into dbo.NoteSubject
(NoteSubjectDesc, Active, AddDate, AddUserID, ModDate, ModUserID, ProgramID)
select
'General',
1,
getdate(),
-1,
getdate(),
-1,
dbo.fn_ProgramID()
where 'General' not in 
(select NoteSubjectDesc  from dbo.NoteSubject where ProgramID = dbo.fn_ProgramID())




--Notes longer than 5000 chars

--declare @UserID int;
--set @UserID = (select userid from dbo.SystemUser where programID = dbo.fn_ProgramID()
--and UserName = 'System')

declare @NoteSubjectID int;
set @NoteSubjectID = (select NoteSubjectID from dbo.NoteSubject where 
NoteSubjectDesc = 'General' and ProgramID = dbo.fn_ProgramID());


insert into dbo.Note
(
--NoteID,
PartyID, 
--InquiryID, 
NoteSubjectID, 
NoteText, 
AddDate, 
AddUserID, 
ModDate, 
ModUserID,
NoteDate,
ProgramID)
select 
--n.NoteID,
pp.PartyID,
@NoteSubjectID,
substring(convert(varchar(max),pv.sNote),5001,5000),
GETDATE(),
--@UserID,
-1,
getdate(),
--@UserID,
-1,
GETDATE(),
dbo.fn_ProgramID()
from CometSource.dbo.tblPersonVolunteer pv
join CometSource.dbo.ConversionPersonToParty pp on pp.PersonID = pv.nPersonID 
--join dbo.Note N on n.PartyID = pp.PartyID
--and left(convert(varchar(max),pv.sNote),5000) = left(n.NoteText,5000)
where pv.sNote is not null
and len(convert(varchar(max),pv.snote))>5000

--------------------------------


--declare @UserID2 int;
--set @UserID2 = (select userid from dbo.SystemUser where programID = dbo.fn_ProgramID()
--and UserName = 'System')

declare @NoteSubjectID2 int;
set @NoteSubjectID2 = (select NoteSubjectID from dbo.NoteSubject where 
NoteSubjectDesc = 'General' and ProgramID = dbo.fn_ProgramID());




insert into dbo.Note
(PartyID, 
--InquiryID, 
NoteSubjectID, 
NoteText, 
AddDate, 
AddUserID, 
ModDate, 
ModUserID,
NoteDate,
ProgramID)
select 
pp.PartyID,
@NoteSubjectID2,
left(convert(varchar(max),pv.sNote),5000),
GETDATE(),
--@UserID2,
-1,
getdate(),
--@UserID2,
-1,
GETDATE(),
dbo.fn_ProgramID()
from CometSource.dbo.tblPersonVolunteer pv
join CometSource.dbo.ConversionPersonToParty pp on pp.PersonID = pv.nPersonID 
where pv.sNote is not null

