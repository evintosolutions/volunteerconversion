
--declare @volunteerinfohelper table (
  --    nPersonID int
  --  , PolicyHolder varchar(max)
  --  , Skills varchar(max)
  --  , Interests varchar(max)
  --);
  --insert into @volunteerinfohelper
  --SELECT 
  --     pc1.nPersonID
  --   , max(aut.sPolicyHolder)
  --   , replace(ltrim(rtrim(STUFF(
  --   (SELECT + ', ' + sSkill
  --    FROM [CometSource].[dbo].tblPerson pc2
  --    join [CometSource].[dbo].tblPersonVolunteerAuto aut on pc2.nPersonID = aut.nPersonID
  --    join [CometSource].[dbo].tblPersonVolunteerSkill sk on pc2.nPersonID = sk.nPersonID
  --    join [CometSource].[dbo].tblPersonVolunteerInterest interest on sk.nPersonID = interest.nPersonID
  --    WHERE pc2.nPersonID = pc1.nPersonID
  --    FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
  --   , replace(ltrim(rtrim(STUFF(
  --   (SELECT + ', ' + sInterest
  --    FROM [CometSource].[dbo].tblPerson pc2
  --    join [CometSource].[dbo].tblPersonVolunteerAuto aut on pc2.nPersonID = aut.nPersonID
  --    join [CometSource].[dbo].tblPersonVolunteerSkill sk on pc2.nPersonID = sk.nPersonID
  --    join [CometSource].[dbo].tblPersonVolunteerInterest interest on sk.nPersonID = interest.nPersonID
  --    WHERE pc2.nPersonID = pc1.nPersonID
  --    FOR XML PATH('') ),1,1,''))),'&#x0D;','') 
  --  FROM [CometSource].[dbo].tblPerson pc1
  --  join [CometSource].[dbo].tblPersonVolunteerAuto aut on aut.nPersonID = pc1.nPersonID
  --GROUP BY pc1.nPersonID ;
  
--First insert supervisors
insert into [dbo].Volunteer
        ( PartyID ,
          Available ,
          ShareInfoPermission ,
          IsVolunteer ,
          IsSupervisor ,
          Assigned ,
          EligibleForRehire ,
          programID ,
          SupervisorPartyID
        )
select distinct
  c.PartyID
  ,0
  ,0
  ,0
  ,1
  ,0
  ,0
  ,null--dbo.fn_ProgramID()
  ,c.PartyID
from [CometSource].[dbo].tblPerson p 
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
where p.sCategory = 'Sprvsr'

go


------------------------------


update v
set Available = 1
from [dbo].Volunteer v
join [dbo].Party p on v.PartyID = p.PartyID
where v.IsSupervisor = 1 and p.Active = 1
and p.programid = dbo.fn_programid()
------------------------------

