
 
insert into [dbo].GeoLocation
        ( GeoLocationDesc ,
          Active ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select distinct 
ltrim(rtrim(sLocationPref))
,1
,getdate()
,-1
,getdate()
,-1
,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer
where ltrim(rtrim(sLocationPref)) is not null
and ltrim(rtrim(sLocationPref)) not in (select GeoLocationdesc from dbo.GeoLocation where programID = dbo.fn_ProgramID())
order by ltrim(rtrim(sLocationPref))

------------------------------

 
insert into [dbo].PrefGeoLocation
        ( PartyID ,
          GeoLocationID ,
          AddDate ,
          AddUserID ,
          ModDate ,
          ModUserID ,
          programID
        )
select
  c.PartyID
  ,t.GeoLocationID
  ,getdate()
  ,-1
  ,getdate()
  ,-1
  ,dbo.fn_ProgramID()
from [CometSource].[dbo].tblPersonVolunteer p
join [CometSource].[dbo].ConversionPersonToParty c on c.PersonID = p.nPersonID
join [dbo].GeoLocation t on t.GeoLocationDesc = p.sLocationPref and t.programID = dbo.fn_ProgramID()

------------------------------

